//
//  ConfirmationVC.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 8/26/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import UIKit
//import AcceptSDK

@available(iOS 13.0, *)
class ConfirmationVC: UIViewController {

    @IBOutlet weak var ordersLbl: UILabel!
    @IBOutlet weak var tbl: UITableView!
    @IBOutlet weak var valuesLbl: UILabel!
    @IBOutlet weak var valuesCountLbl: UILabel!
    @IBOutlet weak var deliverTV: UITextViewX!
    @IBOutlet weak var totalLbl: UILabel!
    @IBOutlet weak var deliverLbl: UILabel!
    @IBOutlet weak var deliveryLabel: UILabel!
    @IBOutlet weak var deliverycost: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var totalCost: UILabel!
    @IBOutlet weak var paymentLbl: UILabel!
    @IBOutlet weak var cashBtn: UIButton!
    @IBOutlet weak var creditBtn: UIButton!
    @IBOutlet weak var confirmBtn: UIButtonX!
    @IBOutlet weak var couponLbl: UILabel!
    @IBOutlet weak var couponTF: UITextField!
//    @IBOutlet weak var couponvalueLbl: UILabel!
    @IBOutlet weak var activateBtn: UIButtonX!
    @IBOutlet weak var currency1Lbl: UILabel!
    @IBOutlet weak var curr2Lbl: UILabel!
    @IBOutlet weak var soonlbl: UILabel!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var copounresultLbl: UILabel!
    
    var CartArr = MyCartData.getCartData().0 as! [CartData]
    var totalPrice: Double?
    var addressid = 0
    var addresstitle1 = ""
    var addresstitle2 = ""
    var shippingcost  = ""
    let token = def.string(forKey: "logToken")
    var allprice = 0
    var currentdate = ""
    var cart = [[String: Int]]()
    var couponsource: Coupon_Base?
    var datasource: _OrderDetails_Base?
    var newcartarr = def.array(forKey: "cartarr")
    var checkpayment = 1
//    let accept = AcceptSDK()
    let btn2 = UIButton(type: .custom)
    let date = Date()
    let formatter = DateFormatter()
    var shippingfees = Double()
    
    // place your payment key here
    let KEY: String = "ZXlKMGVYQWlPaUpLVjFRaUxDSmhiR2NpT2lKSVV6VXhNaUo5LmV5SndjbTltYVd4bFgzQnJJam95TkRRNU15d2libUZ0WlNJNkltbHVhWFJwWVd3aUxDSmpiR0Z6Y3lJNklrMWxjbU5vWVc1MEluMC5PeXItRlNjb3hBYlVKeW0ybDFJbXByZEVxenY1ZWxCVEdpYUZoblFCSDVsU2ZkUDl4TTFkeGFQdzNwV0pUWWZXZm90WFdyY1lfNzV1dzhXODFiS3FlQQ=="

    // Place your billing data here
    // billing data can not be empty
    // if empty, type in NA instead
    let bData = [  "apartment": "1",
                   "email": "ios.ezzat@gmail.com",
                   "floor": "2",
                   "first_name": "mohamed",
                   "street": "zaki",
                   "building": "2",
                   "phone_number": "01112983774",
                   "shipping_method": "e",
                   "postal_code": "3223",
                   "city": "cairo",
                   "country": "egypt",
                   "last_name": "mohamed",
                   "state": "w"
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getShippingFees()
        setDefault()
        
        self.spinner.stopAnimating()
//        self.accept.delegate = self
        self.tbl.delegate = self
        self.tbl.dataSource = self
        
        collectCartData()
        self.confirmBtn.addTarget(self, action: #selector(confirmOrder), for: .touchUpInside)
//        self.couponTF.addTarget(self, action: #selector(getCoupon), for: .editingDidEnd)
        self.activateBtn.addTarget(self, action: #selector(getCoupon), for: .touchUpInside)
//        self.cashBtn.addTarget(self, action: #selector(cashpay), for: .touchUpInside)
//        self.creditBtn.addTarget(self, action: #selector(creditpay), for: .touchUpInside)
        
    }
//    
//    func userDidCancel() {
//        self.checkpayment = 1
//        self.cashBtn.setImage(UIImage(named: "check"), for: .normal)
//        self.creditBtn.setImage(UIImage(named: "checkmark-circle"), for: .normal)
//    }
//    
//    func paymentAttemptFailed(_ error: AcceptSDKError, detailedDescription: String) {
//        print("Failed")
//    }
//    
//    func transactionRejected(_ payData: PayResponse) {
//            print("rejected")
//    }
//    
//    func transactionAccepted(_ payData: PayResponse) {
//        print("accepted")
//    }
//    
//    func transactionAccepted(_ payData: PayResponse, savedCardData: SaveCardResponse) {
//        print("accepted savecard")
//    }
//    
//    func userDidCancel3dSecurePayment(_ pendingPayData: PayResponse) {
//        print("canceled3")
//    }
    
    
    func pageSize(){
        self.tbl.frame.size = CGSize(width: 0.93 * self.view.frame.width, height: 180.0 * CGFloat(self.CartArr.count))
        
    }
    
    func setDefault(){
        self.setSemantic(view: self.view, languageKey: LanguageHelper.getCurrentLanguage())
        self.navigationItem.title = LanguageHelper.getString(forKey: "confirmorder")
        self.ordersLbl.setText(forKey: "myorders")
        self.cashBtn.setText(forKey: "cash")
        self.creditBtn.setText(forKey: "credit")
        self.confirmBtn.setText(forKey: "confirmorder")
        self.deliverLbl.setText(forKey: "deliverto")
        self.deliveryLabel.setText(forKey: "delivery")
        self.paymentLbl.setText(forKey: "paymentmethod")
        self.valuesLbl.setText(forKey: "ordervalues")
        self.totalLabel.setText(forKey: "ordertotal")
        self.totalLbl.setText(forKey: "total")
        self.couponLbl.setText(forKey: "coupon")
        self.curr2Lbl.setText(forKey: "le")
        self.currency1Lbl.setText(forKey: "le")
        self.activateBtn.setText(forKey: "activate")
        self.soonlbl.setText(forKey: "soon")
        pageSize()
        if LanguageHelper.getCurrentLanguage() == "ar"{
            self.cashBtn.titleEdgeInsets.right = 8
            self.cashBtn.titleEdgeInsets.left = 0
            self.cashBtn.contentHorizontalAlignment = .right
            self.creditBtn.titleEdgeInsets.right = 8
            self.creditBtn.titleEdgeInsets.left = 0
            self.creditBtn.contentHorizontalAlignment = .right
        }
        setData()
        getCurrentDate()
        
        btn2.setTitle(LanguageHelper.getString(forKey: "cancel"), for: .normal)
        btn2.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn2.setTitleColor(UIColor.black, for: .normal)
        btn2.tintColor = UIColor.black
        btn2.addTarget(self, action: #selector(backtoviewcontroller), for: .touchUpInside)
        let item2 = UIBarButtonItem(customView: btn2)
        self.navigationItem.setLeftBarButtonItems([item2], animated: true)
    }

}
