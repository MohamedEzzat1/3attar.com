//
//  ConfirmationEx.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 8/26/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

@available(iOS 13.0, *)
extension ConfirmationVC{
    
    func calculatePrice(value:Int, maxdiscount: Int, minbasket: Int, type: String, sdate: String, edate: String){
        
        self.copounresultLbl.textColor = UIColor(red: 40/255, green: 199/255, blue: 111/255, alpha: 1.0)
        
        let currenttimestamp = Double(self.date.timeIntervalSince1970)
        let startdate = formatter.date(from: sdate)
        let enddate   = formatter.date(from: edate)
        
        let starttimestamp = Double(startdate?.timeIntervalSince1970 ?? 0.0)
        let endtimestamp = Double(enddate?.timeIntervalSince1970 ?? 0.0)
        
        if type == ""{
            confirmationcart(value: value, maxdiscount: maxdiscount, minbasket: minbasket, type: type, sdate: sdate, edate: edate)
        }else{
            if currenttimestamp < endtimestamp && currenttimestamp > starttimestamp{
                confirmationcart(value: value, maxdiscount: maxdiscount, minbasket: minbasket, type: type, sdate: sdate, edate: edate)
            }else{
                print(currenttimestamp, endtimestamp, starttimestamp, "popwoWE22222x")
//                SharedHandler.alertDialog2lang(self, msgEg: "Copune is expired", msgAr: "الكوبون منتهي")
                self.copounresultLbl.textColor = UIColor(red: 203/255, green: 38/255, blue: 47/255, alpha: 1.0)
                self.copounresultLbl.text = LanguageHelper.getString(forKey: "copounexpired")
            }
        }
    }
    
    func confirmationcart(value:Int, maxdiscount: Int, minbasket: Int, type: String, sdate: String, edate: String){
        totalPrice = 0.0
        //        var maxdiscount = 0
        //        var minbasket   = 0
                    
        for i in CartArr{
                        
            let qtty  = i.qty
            let price = i.price
            let intPrice = Double(price!)
            let intqtty = Double(qtty!)
            let doubleprice = Double(intPrice ?? 0)
            let doubleqty   = Double(intqtty ?? 0)
            totalPrice = (totalPrice ?? 0.0) + (doubleprice * doubleqty)
            print(totalPrice, "opipoup'", doubleqty, doubleprice)
        }
                    
        if type == "percentage"{
//            self.activateBtn.setTitle(String(describing:value) + "%", for: .normal)
            
            //            self.couponvalueLbl.text = String(describing:value) + "%"

            if value < maxdiscount{
                if LanguageHelper.getCurrentLanguage() == "ar"{
                    self.copounresultLbl.text = "تم خصم" + "\(value)%"
                }else{
                    self.copounresultLbl.text = "\(value)%" + "Off"
                }
                //                totalPrice = 0.0
                totalPrice = totalPrice ?? 0.0 - Double(totalPrice ?? 0.0 * Double((value / 100)))
                            
            }else{
                self.copounresultLbl.textColor = UIColor(red: 203/255, green: 38/255, blue: 47/255, alpha: 1.0)
                self.copounresultLbl.text = LanguageHelper.getString(forKey: "minimumcost") + " \(maxdiscount)" + LanguageHelper.getString(forKey: "le")
            }
        }else if type == "numeric"{
//            self.activateBtn.setTitle(String(describing:value), for: .normal)
            //            self.couponvalueLbl.text = String(describing:value)
            if Int(self.totalPrice ?? 0) > minbasket{
                if LanguageHelper.getCurrentLanguage() == "ar"{
                    self.copounresultLbl.text = "تم خصم" + "\(value) جنيه"
                }else{
                    self.copounresultLbl.text = "\(value) LE" + "Off"
                }

            //                self.totalPrice = 0.0
                totalPrice =   abs (Double(value) - totalPrice! ?? 0.0)
                self.totalCost.text = "\(String(describing: totalPrice))"
            }else{
                self.copounresultLbl.textColor = UIColor(red: 203/255, green: 38/255, blue: 47/255, alpha: 1.0)
                self.copounresultLbl.text = LanguageHelper.getString(forKey: "minimumcost") + " \(minbasket)" + LanguageHelper.getString(forKey: "le")
            }
        }
                    
        if value == 0{
            self.activateBtn.setText(forKey: "activate")
        }
        print(self.totalPrice , Double(self.shippingfees))
        if self.totalPrice ?? 0.0 >= Double(self.shippingfees){
                        
            self.valuesCountLbl.text = "\(totalPrice ?? 0.0)"
            self.totalCost.text =  "\(totalPrice ?? 0.0)"
            self.deliverycost.setText(forKey: "freeshiping")
            self.currency1Lbl.text = ""
            allprice = Int(totalPrice ?? 0.0)
        }else{
                        print("qeqweqwerqwR3333")
            if Double(shippingcost) != nil{
                print("qeqweqwerqwR1111")
                let totalcost = Double(shippingcost)! + self.totalPrice!
                self.valuesCountLbl.text = "\(totalPrice ?? 0.0)"
                self.totalCost.text    = "\(totalcost)"
                allprice = Int(totalcost)
            }else{
                print("qeqweqwerqwR666")
                let totalcost = Double(shippingcost) ?? 0.0 + self.totalPrice!
                self.valuesCountLbl.text = "\(totalPrice ?? 0.0)"
                self.totalCost.text    = "\(totalcost)"
                allprice = Int(totalcost)
            }
        }
    }
    
    @objc func getCoupon(){
        SharedHandler.startSpinner(spinner, v, self.view)
        switch couponTF.text {
        case "":
//            self.couponTF.emptyField(msg: LanguageHelper.getString(forKey: "coponempty"))
            print("")
        default:
            let header: HTTPHeaders = [
                "Authorization": "bearer \(self.token ?? "")"
            ]
            
            APIManager.sharedInstance.getRequest(URLs.coupon + couponTF.text!, header: header) { (res) in
                SharedHandler.stopSpinner(self.spinner, v, self.view)
                let decoder = JSONDecoder()
                do{
                    if let data = res.data{
                        let model = try decoder.decode(Coupon_Base.self, from: data)
                        self.couponsource = model
                        print("model ==> \(self.couponsource!)")
                        if model.success! == true{
                            if model.orders?.active == 0{
                                self.couponTF.emptyField(msg: "not active")
                            }else{
                                self.calculatePrice(value: model.orders?.value ?? 0, maxdiscount: model.orders?.max_discount ?? 0, minbasket: model.orders?.min_basket ?? 0, type: model.orders?.type ?? "", sdate: model.orders?.start_date ?? "", edate: model.orders?.end_date ?? "")
                            }
                            
                        }else{
                            self.couponTF.emptyField(msg: model.message!)
                            SharedHandler.alertDialog2lang(self, msgEg: "\(model.message!)", msgAr: "\(model.message!)")
                        }
                    }
                }catch{
                    SharedHandler.alertDialog2lang(self, msgEg: "invalid data Parsing.", msgAr: "خطأ في تحويل البيانات.")
                    print("model ==> \(res.error)")
                }
            }
        }
    }
    
    @objc func backtoviewcontroller(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func setData(){
        self.deliverTV.text = self.addresstitle1 + " ( " + self.addresstitle2 + " )"
        self.deliverycost.text = "\(shippingcost)"
    }
    
    func getCurrentDate(){
        formatter.dateFormat = "yyyy-MM-dd"
        let result = formatter.string(from: date)
        let x = date.timeIntervalSince1970
        self.currentdate = result
    }
    
    func collectCartData(){
        for i in CartArr{
            let price = i.price
            let qty   = i.qty
            let id    = i.product_id
            
            let intid = Int(id ?? "0")!
            let doubleprice = Double(price ?? "0.0")!
            let intprice = Int(doubleprice)
            let intqty = Int(qty ?? "1")!
                        
            let dict = ["product_id": intid, "quantity": intqty, "item_cost": intprice] as [String : Int]
            self.cart.append(dict)
        }
        
        print(cart, "hfjffjhfj")
    }
    
    @objc func confirmOrder(){
        SharedHandler.startSpinner(spinner, v, self.view)
        let header: HTTPHeaders = [
            "Authorization": "bearer \(self.token ?? "")"
        ]
        
        let body = [
            "token" : self.token!,
            "payment_method": 1,
            "courier_id": 1,
            "address_id": self.addressid,
            "promo_code": couponTF.text!,
            "total": self.allprice,
            "shipping_date": self.currentdate,
            "order_items": self.cart
            ] as [String : Any]
        
        APIManager.sharedInstance.postRequest(URLs.placeOrder, header: header, Parameters: body) { (res) in
            SharedHandler.stopSpinner(self.spinner, v, self.view)
            let decoder = JSONDecoder()
            do{
                let model = try decoder.decode(_OrderDetails_Base.self, from: res.data!)
                self.datasource = model
                if model.success! == false{
                    SharedHandler.alertDialog2lang(self, msgEg: "\(model.message!)", msgAr: "\(model.message!)")
                }else{
                    let json = JSON(res.value!)
                    
                    if let orderno = json["orders"]["order_no"].int{
                        print(orderno, "ewrretr")
                        def.set(orderno, forKey: "orderNo")
                    }
                    if let paymentmethod = json["orders"]["payment_method"].int{
                        def.set(paymentmethod, forKey: "paymentmethod")
                    }
                    
//                    def.set(self.datasource?.order_details?.order_no ?? "", forKey: "orderNo")
                    def.set(self.datasource?.order_details?.payment_method ?? 0, forKey: "paymentmethod")
                    def.set(self.deliverycost.text, forKey: "cartshippingcost")
                    def.set(self.totalCost.text, forKey: "carttotalcost")
                    MyCartData.deleteCartData()
                    print("model ==> \(self.datasource!)", "dsdwererrE", res.value)
                                    
                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Order", bundle: nil)
                    let viewController = mainStoryboard.instantiateViewController(withIdentifier: "SuccessVC") as! SuccessVC
                    
                    UIApplication.shared.keyWindow?.rootViewController = viewController
                                    
                }
            }catch{
                SharedHandler.alertDialog2lang(self, msgEg: "invalid data Parsing.", msgAr: "خطأ في تحويل البيانات.")
                print("model ==> \(res.response?.statusCode)")
            }
        }
        
    }
    
    
    @objc func cashpay(){
        self.checkpayment = 1
        self.cashBtn.setImage(UIImage(named: "check"), for: .normal)
        self.creditBtn.setImage(UIImage(named: "checkmark-circle"), for: .normal)
    }
    
    @objc func creditpay(){
        self.checkpayment = 2
        self.cashBtn.setImage(UIImage(named: "checkmark-circle"), for: .normal)
        self.creditBtn.setImage(UIImage(named: "check"), for: .normal)
//        do {
//            try accept.presentPayVC(vC: self, billingData: bData, paymentKey: KEY, saveCardDefault:
//            true, showSaveCard: true, showAlerts: true)
//        } catch {
//            print("errorMessage")
//        }  catch let error {
//            print(error.localizedDescription)
//        }
    }
    
    func getShippingFees(){
        SharedHandler.startSpinner(self.spinner, v, self.view)
        
        let header: HTTPHeaders = [
            "Authorization": "bearer \(self.token ?? "")"
        ]
        
        APIManager.sharedInstance.getRequest("https://app.3attar.com/api/v2/shipping_fees", header: header) { (res) in
            if let fees = res.value as? Double{
                self.shippingfees = fees
                print(Double(self.shippingfees), "oapsoipdoas")
                self.calculatePrice(value: 0, maxdiscount: 1, minbasket: 1, type: "", sdate: "", edate: "")
            }
            SharedHandler.stopSpinner(self.spinner, v, self.view)
            
        }
    }
    
    
}
