//
//  SuccessVC.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 8/26/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import UIKit

class SuccessVC: UIViewController {

    @IBOutlet weak var pageTitle: UILabel!
    @IBOutlet weak var ordernumberLbl: UILabel!
    @IBOutlet weak var addresslbl: UILabel!
    @IBOutlet weak var addressTV: UITextViewX!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var phonenuber: UILabel!
    @IBOutlet weak var paymentmethod: UILabel!
    @IBOutlet weak var deliveryLabel: UILabel!
    @IBOutlet weak var deliveryCost: UILabel!
    @IBOutlet weak var totalLbl: UILabel!
    @IBOutlet weak var totalCost: UILabel!
    @IBOutlet weak var backhomeBtn: UIButtonX!
    override func viewDidLoad() {
        super.viewDidLoad()
        setDefault()
        getData()
        self.backhomeBtn.addTarget(self, action: #selector(backhome), for: .touchUpInside)
    }

    func setDefault(){
        self.setSemantic(view: self.view, languageKey: LanguageHelper.getCurrentLanguage())
        self.pageTitle.setText(forKey: "ordersuccess")
        self.addresslbl.setText(forKey: "myaddress")
        self.phoneLabel.setText(forKey: "phone")
        self.deliveryLabel.setText(forKey: "delivery")
        self.totalLbl.setText(forKey: "total")
        self.backhomeBtn.setText(forKey: "backtohome")
        
    }
}
