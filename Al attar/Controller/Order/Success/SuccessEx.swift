//
//  SuccessEx.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 8/26/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation

extension SuccessVC{
    
    func getData(){
        self.phonenuber.text = def.string(forKey: "logPhone")
        self.ordernumberLbl.text = String(describing:def.integer(forKey: "orderNo"))
        print(def.integer(forKey: "orderNo"), "[qoe[qwrqwR")
        let payment = def.integer(forKey: "paymentmethod")
        if payment == 1{
            self.paymentmethod.setText(forKey: "cashpay")
        }else if payment == 2{
            self.paymentmethod.setText(forKey: "creditpay")
        }
        self.totalCost.text = (def.string(forKey: "carttotalcost"))
        self.deliveryCost.text = (def.string(forKey: "cartshippingcost"))
    }
    
    @objc func backhome(){
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }
    
}
