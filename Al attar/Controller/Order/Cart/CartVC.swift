//
//  CartVC.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 8/23/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import UIKit

var glbcart: CartData?
var tblid = 0

protocol PriceDelegate{
    func setNewPrice(qty:String)
}
@available(iOS 13.0, *)

class CartVC: UIViewController, CartDelegate, PriceDelegate {

    @IBOutlet weak var titalLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var orderBtn: UIButtonX!
    @IBOutlet weak var tbl: UITableView!
    @IBOutlet weak var currencyLbl: UILabel!
    @IBOutlet weak var totalview: UIViewX!
    @IBOutlet weak var startBtn: UIButtonX!
    @IBOutlet weak var emptycartLbl: UILabel!
    @IBOutlet weak var image: UIImageView!
    let btn1 = UIButton(type: .custom)
    let btn2 = UIButton(type: .custom)

    
    var CartArr = MyCartData.getCartData().0 as! [CartData]
    var newcartarr = def.array(forKey: "cartarr")
    var totalPrice = 0.0
    var cartdelegate: CartDelegate?
    var alert = UIAlertController(title: LanguageHelper.getString(forKey: "loginfirst"), message: nil, preferredStyle: .actionSheet)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        image.isHidden = false
        emptycartLbl.isHidden = false
        startBtn.isHidden  = false
        totalview.isHidden = true
        orderBtn.isHidden  = true
        tbl.isHidden = true
        setDefault()
        calculatePrice()
        self.orderBtn.addTarget(self, action: #selector(ordernow), for: .touchUpInside)
        self.startBtn.addTarget(self, action: #selector(openhome), for: .touchUpInside)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.backBarButtonItem?.tintColor = UIColor.black
    }
    
    @objc func openhome(){
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }
    

    func setDefault(){
        self.setSemantic(view: self.view, languageKey: LanguageHelper.getCurrentLanguage())
        btn1.setTitle(LanguageHelper.getString(forKey: "deletecart"), for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn1.setTitleColor(UIColor(red: 203/255, green: 38/255, blue: 47/255, alpha: 1), for: .normal)
        btn1.tintColor = UIColor(red: 203/255, green: 38/255, blue: 47/255, alpha: 1)
        btn1.addTarget(self, action: #selector(deleteallitems), for: .touchUpInside)
        
        btn2.setTitle(LanguageHelper.getString(forKey: "cancel"), for: .normal)
        btn2.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn2.setTitleColor(UIColor.black, for: .normal)
        btn2.tintColor = UIColor.black
        btn2.addTarget(self, action: #selector(backtoviewcontroller), for: .touchUpInside)
        
        let item1 = UIBarButtonItem(customView: btn1)
        let item2 = UIBarButtonItem(customView: btn2)
        if MyCartData.getCartData().0.count != 0{
            self.navigationItem.setRightBarButtonItems([item1], animated: true)
            self.navigationItem.setLeftBarButtonItems([item2], animated: true)
            image.isHidden = true
            emptycartLbl.isHidden = true
            startBtn.isHidden  = true
            totalview.isHidden = false
            orderBtn.isHidden  = false
            tbl.isHidden = false
        }
        self.navigationItem.title = LanguageHelper.getString(forKey: "cart")
        self.titalLbl.setText(forKey: "myorders")
        self.priceLbl.setText(forKey: "total")
        self.orderBtn.setText(forKey: "ordernow")
        self.currencyLbl.setText(forKey: "le")
        emptycartLbl.setText(forKey: "wishdesc")
        startBtn.setText(forKey: "startshopping")
    }
}
