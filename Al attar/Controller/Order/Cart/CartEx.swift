//
//  CartEx.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 8/23/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation

@available(iOS 13.0, *)
extension CartVC{
    
    func calculatePrice(){
        for i in CartArr{
                
            let qtty  = i.qty
            let price = i.price
            let intPrice = Double(price!)
            let intqtty = Double(qtty!)
            let doubleprice = Double(intPrice ?? 0)
            let doubleqty   = Double(intqtty ?? 0)
            let total = totalPrice + (doubleprice * doubleqty)
            totalPrice = total
        }
            
        self.priceLbl.text = "\(totalPrice)"
    }
   
    @objc func deleteallitems(){
        MyCartData.deleteCartData()
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }
    
    @objc func backtoviewcontroller(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func setCart(cartcount: String) {
        totalPrice = 0.0
        CartArr = MyCartData.getCartData().0 as! [CartData]
        self.tbl.reloadData()
        self.calculatePrice()
        if CartArr.count == 0{
            image.isHidden = false
            emptycartLbl.isHidden = false
            startBtn.isHidden  = false
            totalview.isHidden = true
            orderBtn.isHidden  = true
            tbl.isHidden = true
        }
    }
    
    func setNewPrice(qty: String) {
        totalPrice = 0.0
        calculatePrice()
    }
    
    @objc func openLogin(){
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
//        UIApplication.shared.keyWindow?.rootViewController = viewController
            self.pushVC(storyboard: "Main", viewController: "LoginVC")
    }
        
    @objc func openSignup(){
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "SignupVC") as! SignupVC
//        UIApplication.shared.keyWindow?.rootViewController = viewController
        self.pushVC(storyboard: "Main", viewController: "SignupVC")
    }
    
    @objc func ordernow(){
        if def.bool(forKey: _LoggedIn) == true{
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "AddressListVC") as! AddressListVC
            vc.navID = 2
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            openLogin()
//            let loginAction = UIAlertAction(title: LanguageHelper.getString(forKey: "login"), style: .default){
//                UIAlertAction in
//                self.openLogin()
//            }
////            let signupAction = UIAlertAction(title: LanguageHelper.getString(forKey: "join"), style: .default){
////                UIAlertAction in
////                self.openSignup()
////            }
//            let cancelAction = UIAlertAction(title: LanguageHelper.getString(forKey: "cancel"), style: .cancel){
//                UIAlertAction in
//                return
//            }
//
//            // Add the actions
//            alert.addAction(loginAction)
////            alert.addAction(signupAction)
//            alert.addAction(cancelAction)
//
//            alert.popoverPresentationController?.sourceView = self.view
//            self.present(alert, animated: true, completion: nil)
//            SharedHandler.alertDialog2lang(self, msgEg: "Please Login first to complete your order.", msgAr: "من فضلك سجل دخول لاستكمال طلبك.")
        }
    }
    
}
