//
//  FilterVC.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 9/4/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import UIKit

protocol FilterDelegate{
    func setProducts(brandid:Int, brandtitle:String, cattitle:String)
}
@available(iOS 13.0, *)
class FilterVC: UIViewController, UIGestureRecognizerDelegate{

    @IBOutlet weak var minPriceLbl: UILabel!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var pagetitle: UILabel!
    @IBOutlet weak var catLbl: UILabel!
    @IBOutlet weak var catCollection: UICollectionView!
    @IBOutlet weak var brandLbl: UILabel!
    @IBOutlet weak var brandCollection: UICollectionView!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var priceCounter: UILabel!
    @IBOutlet weak var priceSlider: UISlider!
    @IBOutlet weak var filterBtn: UIButtonX!
    
    var catsource: Cat_Base?
    var brandsource: Brands_Base?
    var filterdelegate: FilterDelegate?
    var brandID: Int?
    var catID: Int?
    var catTitle: String?
    var brandTitle: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getData()
        setdefault()
        self.priceCounter.text = String(Int(self.priceSlider.value))
        self.priceSlider.addTarget(self, action: #selector(changevaluecounter), for: .valueChanged)
        self.filterBtn.addTarget(self, action: #selector(filterData), for: .touchUpInside)
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }

    func setdefault(){
        pagetitle.setText(forKey: "calssifyproducts")
        catLbl.setText(forKey: "categories")
        brandLbl.setText(forKey: "brand")
        priceLbl.setText(forKey: "price")
        filterBtn.setText(forKey: "filterproducts")
        self.navigationItem.title = LanguageHelper.getString(forKey: "filter")
    }
}
