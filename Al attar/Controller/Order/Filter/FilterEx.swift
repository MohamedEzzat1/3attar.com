//
//  FilterEx.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 9/4/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation
import SwiftyJSON

@available(iOS 13.0, *)
extension FilterVC{
    
    func getData(){
        SharedHandler.startSpinner(spinner, v, self.view)
        let url = URL(string: URLs.brands)
        APIManager.sharedInstance.getRequest(url!) { (res) in
            let decoder = JSONDecoder()
            SharedHandler.stopSpinner(self.spinner, v, self.view)
            do {
                if let products = res.data{
                    let model = try decoder.decode(Brands_Base.self, from: res.data!)
                    self.brandsource = model
                    print("modelproducts ==> \(self.brandsource!)")
                    self.brandCollection.dataSource = self
                    self.brandCollection.delegate   = self
                    self.brandCollection.reloadData()
                }
            }catch{
                print(JSON(res.value))
                SharedHandler.alertDialog2lang(self, msgEg: "invalid data Parsing.", msgAr: "خطأ في تحويل البيانات.")
            }
        }
    }
    
    @objc func filterData(){
        guard let id = brandID, id != nil else {
            return
        }
        guard let brandtitle = brandTitle, brandtitle != "" else {
            return
        }
        guard let cat = catTitle, cat != "" else {
            return
        }
        setDataFilter()
    }
    
    func setDataFilter(){
        SharedHandler.startSpinner(spinner, v, self.view)
        let body = [
            "brand_id": brandID ?? 0,
            "category_id": catID ?? 0,
            "price_from": priceSlider.minimumValue,
            "price_to": priceSlider.value
            ] as [String : Any]
        APIManager.sharedInstance.postRequest(URLs.filter, Parameters: body) { (res) in
            let decoder = JSONDecoder()
            SharedHandler.stopSpinner(self.spinner, v, self.view)
            do {
                if let products = res.data{
                    let model = try decoder.decode(Search_Base.self, from: res.data!)
                    self.filterdelegate?.setProducts(brandid: self.brandID ?? 0, brandtitle:self.brandTitle ?? "", cattitle: self.catTitle ?? "")
                    print(model)
                    if model.products?.count != 0{
                        let nextStoryboard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
                        let vc = nextStoryboard.instantiateViewController(withIdentifier: "SearchVC") as! SearchVC
                        vc.brandid = self.brandID!
                        vc.searchsource = model
                        vc.checksearch = 1
                        vc.searchWord = self.catTitle! + "-" + self.brandTitle!
                        self.navigationController?.pushViewController(vc, animated: true)
                    }else{
                        SharedHandler.alertDialog2lang(self, msgEg: "No products found!", msgAr: "لا يوجد منتجات")
                    }
                }
            }catch{
                print(JSON(res.value))
                SharedHandler.alertDialog2lang(self, msgEg: "invalid data Parsing.", msgAr: "خطأ في تحويل البيانات.")
            }
        }
    }
    
    @objc func changevaluecounter(){
        self.minPriceLbl.text  = String(Int(self.priceSlider.minimumValue))
        self.priceCounter.text = String(Int(self.priceSlider.value))
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
}
