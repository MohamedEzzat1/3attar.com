//
//  FiltercollectionEx.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 9/4/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation

@available(iOS 13.0, *)
extension FilterVC: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == brandCollection{
            return brandsource?.brands?.count ?? 0
        }else{
            return catsource?.categories?.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch collectionView {
        case brandCollection:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BrandCell", for: indexPath) as! BrandCell
            cell.id = brandsource?.brands?[indexPath.row].id
            if LanguageHelper.getCurrentLanguage() == "ar"{
                cell.title.text = brandsource?.brands?[indexPath.row].title_ar
            }else{
                cell.title.text = brandsource?.brands?[indexPath.row].title_en
            }
            return cell
            
            case catCollection:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FilterCell", for: indexPath) as! FilterCell
                cell.id = catsource?.categories?[indexPath.row].id
            if LanguageHelper.getCurrentLanguage() == "ar"{
                cell.title.text = catsource?.categories?[indexPath.row].title_ar
            }else{
                cell.title.text = catsource?.categories?[indexPath.row].title_en
            }
            return cell
        default:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BrandCell", for: indexPath) as! BrandCell
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch collectionView {
        case brandCollection:
            for i in 0..<collectionView.numberOfItems(inSection: indexPath.section) {
                 let cell = collectionView.cellForItem(at: [indexPath.section, i]) as? BrandCell
                if i == indexPath.row{
                    cell?.selectCell()
                    if LanguageHelper.getCurrentLanguage() == "ar"{
                        self.brandTitle = brandsource?.brands?[i].title_ar
                    }else{
                        self.brandTitle = brandsource?.brands?[i].title_en
                    }
                    self.brandID = brandsource?.brands?[i].id
                }else{
                    cell?.unselectCell()
                }
            }
            print(brandID, brandTitle)
        case catCollection:
            for i in 0..<collectionView.numberOfItems(inSection: indexPath.section) {
                 let cell = collectionView.cellForItem(at: [indexPath.section, i]) as? FilterCell
                if i == indexPath.row{
                    cell?.selectCell()
                    if LanguageHelper.getCurrentLanguage() == "ar"{
                        self.catTitle = catsource?.categories?[i].title_ar
                    }else{
                        self.catTitle = catsource?.categories?[i].title_en
                    }
                    self.catID = catsource?.categories?[i].id
                }else{
                    cell?.unselectCell()
                }
            }
            print(catTitle)
        default:
            print("nothing")
        }
    }
    
}
