//
//  OrderDetailsTableEx.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 8/22/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation

extension OrderDetailsVC: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.datasource?.order_details?.order_items?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderDetailsCell", for: indexPath) as! OrderDetailsCell
        if LanguageHelper.getCurrentLanguage() == "ar"{
            cell.qty.text = "الكمية: " + String(self.datasource?.order_details?.order_items?[indexPath.row].quantity ?? 0)
        }else{
            cell.qty.text = "Quantity: " + String(self.datasource?.order_details?.order_items?[indexPath.row].quantity ?? 0)
        }
        cell.priceLbl.text = (self.datasource?.order_details?.order_items?[indexPath.row].item_cost) ?? ""
        cell.currencyLbl.setText(forKey: "le")
        cell.titleLbl.text = self.datasource?.order_details?.order_items?[indexPath.row].title ?? ""
        let url = URL(string: self.datasource?.order_details?.order_items?[indexPath.row].image ?? "")
        cell.img.kf.setImage(with: url)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 123
    }
    
}
