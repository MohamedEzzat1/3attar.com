//
//  OrderDetailsEx.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 8/22/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation
import Alamofire

extension OrderDetailsVC {
    
    func getData(){
        
        let token = def.string(forKey: "logToken")
        let headers : HTTPHeaders = [
            "Authorization": "bearer \(token ?? "")"]
        SharedHandler.startSpinner(spinner, v, self.view)
        APIManager.sharedInstance.getRequest(URLs.order_details + "\(orderid!)", header: headers) { (res) in
            SharedHandler.stopSpinner(self.spinner, v, self.view)
            let decoder = JSONDecoder()
            do {
                if let dta = res.data{
                    let model = try decoder.decode(_OrderDetails_Base.self, from: dta)
                    self.datasource = model
                    
                    self.costDigit.text = (self.datasource?.order_details?.total)!
                    self.ordernumberdigit.text = String(describing:self.datasource?.order_details?.order_no ?? "0")
                    self.ordersDigit.text = String(describing:self.datasource?.order_details?.order_no ?? "0")
                    
                    switch model.order_details?.status {
                    case 1:
                        self.statusdigit.setText(forKey: "notworking")
                    case 2:
                        self.statusdigit.setText(forKey: "ongoing")
                    case 3:
                        self.statusdigit.setText(forKey: "delivered")
                    case 4:
                        self.statusdigit.setText(forKey: "recived")
                    case 5:
                        self.statusdigit.setText(forKey: "canceled")
                    default:
                        print("")
                    }
                    
                    if let items = self.datasource?.order_details?.order_items{
                        var totalsum = 0.0
                        for i in items{
                            if i.item_cost != nil{
                                let x = Double(i.item_cost!)!
                                totalsum += x * Double(i.quantity!)
                            }
                        }
                        let total = (self.datasource?.order_details?.total)!
                        let shippingcost = Double(total)! - totalsum
                        self.shippingdigit.text = String(describing: shippingcost)
                        self.ordersDigit.text = String(describing: totalsum)
                    }
                    
                    self.table.dataSource = self
                    self.table.delegate   = self
                    self.table.reloadData()
                }else{
                    self.getData()
                }
            }catch{
                SharedHandler.alertDialog2lang(self, msgEg: "invalid data Parsing.", msgAr: "خطأ في تحويل البيانات.")
            }
        }
        
    }
    
}
