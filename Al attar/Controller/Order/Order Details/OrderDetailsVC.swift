//
//  OrderDetailsVC.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 8/22/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import UIKit

class OrderDetailsVC: UIViewController {

    @IBOutlet weak var ordernoLbl: UILabel!
    @IBOutlet weak var ordernumberdigit: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var statusdigit: UILabel!
    @IBOutlet weak var productLbl: UILabel!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var costLbl: UILabel!
    @IBOutlet weak var costDigit: UILabel!
    @IBOutlet weak var currencyLbl: UILabel!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var ordersLabel: UILabel!
    @IBOutlet weak var shippingLbl: UILabel!
    @IBOutlet weak var le1: UILabel!
    @IBOutlet weak var le2: UILabel!
    @IBOutlet weak var ordersDigit: UILabel!
    @IBOutlet weak var shippingdigit: UILabel!
    
    var orderid : String?
    var datasource: _OrderDetails_Base?
    override func viewDidLoad() {
        super.viewDidLoad()
        setDefault()
        getData()
    }

    func setDefault(){
        self.setSemantic(view: self.view, languageKey: LanguageHelper.getCurrentLanguage())
        self.navigationItem.title = LanguageHelper.getString(forKey: "orderdetails")
        self.ordernoLbl.setText(forKey: "ordernumber")
        self.statusLbl.setText(forKey: "status")
        self.productLbl.setText(forKey: "products")
        self.costLbl.setText(forKey: "total")
        self.ordersLabel.setText(forKey: "myorders")
        self.shippingLbl.setText(forKey: "delivery")
        self.currencyLbl.setText(forKey: "le")
        self.le1.setText(forKey: "le")
        self.le2.setText(forKey: "le")
    }
}
