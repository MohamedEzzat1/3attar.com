//
//  ProductDetailsTableEx.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 10/23/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation

@available(iOS 13.0, *)
extension ProductDetailsVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row{
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! ProductDetailsCell
            let base_url = self.datasource?.imgs_url
            let imgurl   = self.datasource?.article?.image
            let url = base_url! + "/" + imgurl!
            let u = URL(string: url)
            cell.img.kf.setImage(with: u)
            if cell.img.image == nil{
                cell.img.image = UIImage(named: "logo_splash_screen")
            }
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as! ProductDetailsCell
            if LanguageHelper.getCurrentLanguage() == "ar"{
                cell.descLbl.textAlignment = .right
                cell.descLbl.attributedText = try? NSAttributedString(htmlString: (self.datasource?.article?.description_ar ?? ""))
            }else{
                cell.descLbl.attributedText = try? NSAttributedString(htmlString: (self.datasource?.article?.description_en ?? ""))
            }
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell3", for: indexPath) as! ProductDetailsCell
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell4", for: indexPath) as! ProductDetailsCell
            cell.addtoCart.setText(forKey: "addtocart")
            cell.addtoCart.addTarget(self, action: #selector(addtoCart), for: .touchUpInside)
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! ProductDetailsCell
            return cell
            print("error")
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row{
        case 0:
            return 310
        case 1:
            return UITableView.automaticDimension
        case 2:
            return 107
        case 3:
            return 70
        default:
            return 50
        }
    }
    
    
}
