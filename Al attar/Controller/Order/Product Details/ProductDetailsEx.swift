//
//  ProductDetailsEx.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 8/21/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation
import Alamofire

@available(iOS 13.0, *)
extension ProductDetailsVC{
    
    func getData(){
        print(productid,"SSSSSS")
        SharedHandler.startSpinner(spinner, v, self.view)
        APIManager.sharedInstance.getRequest(URLs.product_details + "\(productid ?? 0)", header: [:]) { (res) in
            SharedHandler.stopSpinner(self.spinner, v, self.view)
            let decoder = JSONDecoder()
            do {
                if let products = res.data{
                    let model = try decoder.decode(product_details.self, from: products)
                    self.datasource = model
                    self.tbl.delegate = self
                    self.tbl.dataSource = self
                    self.tbl.reloadData()
//                    if LanguageHelper.getCurrentLanguage() == "ar"{
//                        self.adjustUITextViewHeight(arg: self.productDesc, desc: self.datasource?.article?.description_ar ?? "")
//                        self.productNameLbl.text = self.datasource?.article?.title_ar
//                    }else{
//                        self.adjustUITextViewHeight(arg: self.productDesc, desc: self.datasource?.article?.description_en ?? "")
//                        self.productNameLbl.text = self.datasource?.article?.title_en
//                    }
//                    let base_url = self.datasource?.imgs_url
//                    let imgurl   = self.datasource?.article?.image
//                    let url = base_url! + "/" + imgurl!
//                    let u = URL(string: url)
//                    self.productImg.kf.setImage(with: u)
//                    print("modelproducts ==> \(self.datasource!)")
//                    print((self.datasource?.imgs_url!)! + "/" + (self.datasource?.article?.image!)!, "DSfwegfEW")
                }
            }catch{
                
                SharedHandler.alertDialog2lang(self, msgEg: "invalid data Parsing.", msgAr: "خطأ في تحويل البيانات.")
            }

        }
    }
    
    func getDataDL(){
        SharedHandler.startSpinner(spinner, v, self.view)
        APIManager.sharedInstance.getRequest(URLs.productDL + producturi , header: [:]) { (res) in
            SharedHandler.stopSpinner(self.spinner, v, self.view)
            let decoder = JSONDecoder()
            do {
                if let products = res.data{
                    let model = try decoder.decode(product_details.self, from: products)
                    self.datasource = model
                    self.tbl.delegate = self
                    self.tbl.dataSource = self
                    self.tbl.reloadData()
//                    if LanguageHelper.getCurrentLanguage() == "ar"{
//                        self.adjustUITextViewHeight(arg: self.productDesc, desc: self.datasource?.article?.description_ar ?? "")
//                        self.productNameLbl.text = self.datasource?.article?.title_ar
//                    }else{
//                        self.adjustUITextViewHeight(arg: self.productDesc, desc: self.datasource?.article?.description_en ?? "")
//                        self.productNameLbl.text = self.datasource?.article?.title_en
//                    }
//                    let base_url = self.datasource?.imgs_url
//                    let imgurl   = self.datasource?.article?.image
//                    let url = base_url! + "/" + imgurl!
//                    let u = URL(string: url)
//                    self.productImg.kf.setImage(with: u)
//                    print("modelproducts ==> \(self.datasource!)")
//                    print((self.datasource?.imgs_url!)! + "/" + (self.datasource?.article?.image!)!, "DSfwegfEW")
                }
            }catch{
                
                SharedHandler.alertDialog2lang(self, msgEg: "invalid data Parsing.", msgAr: "خطأ في تحويل البيانات.")
            }

        }
    }
    
    @objc func addtofavorites(){
        SharedHandler.startSpinner(spinner, v, self.view)
        guard let token = def.string(forKey: "logToken") else{
            return
        }
        let headers : HTTPHeaders = [
            "Authorization": "bearer \(token ?? "")"]
        
        let body = [
            "product_id": self.datasource?.article?.id
        ]
        APIManager.sharedInstance.postRequest(URLs.add_to_wishlist, header: headers, Parameters: body) { (res) in
            SharedHandler.stopSpinner(self.spinner, v, self.view)
            print(res.value)
            self.btn1.setImage(UIImage(named: "Add_to_favorite_button_selected"), for: .normal)
            self.isfaved = true
        }
    }
    
    @objc func removefrmfavorites(){
        btn1.setImage(UIImage(named: "Add_to_favorite_button"), for: .normal)

        isfaved = false
    }
    
    
    @objc func dismissVC(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func addcart(){
        var post: CartBase?
        if newcartarr.count != 0 {
            for i in newcartarr{
                if newcartarr.contains(where: {$0.product_id == self.datasource?.article?.id!}) {
                    let qty = i.quantity
                    newcartarr.filter {$0.product_id == self.datasource?.article?.id!}.first?.quantity = qty! + 1
                    print(true, newcartarr[0].quantity)
                    
                }else{
                    post = CartBase(name_ar: (self.datasource?.article?.title_ar!)!, product_id: (self.datasource?.article?.id!)!, name_en: (self.datasource?.article?.title_en!)!, quantity: 1, item_cost: (self.datasource?.article?.regular_price!)!, image: (self.datasource?.imgs_url!)! + "/" + (self.datasource?.article?.image!)!)
                    createtocart(cart: post!)
                    
                }
            }
        }else{
             post = CartBase(name_ar: (self.datasource?.article?.title_ar!)!, product_id: (self.datasource?.article?.id!)!, name_en: (self.datasource?.article?.title_en!)!, quantity: 1, item_cost: (self.datasource?.article?.regular_price!)!, image: (self.datasource?.imgs_url!)! + "/" + (self.datasource?.article?.image!)!)
            createtocart(cart: post!)
        }
        
    }
    
    @objc func addtoCart(){
        var checkID = [String]()
        if let CartArr = MyCartData.getCartData().0 as? [CartData]{
            print(CartArr)
            switch CartArr.count {
            case 1:
                MyCartData.addCartData(qty: "1", productID: String(describing:(self.datasource?.article?.id!)!), Image: (self.datasource?.imgs_url!)! + "/" + (self.datasource?.article?.image!)!, price: String(describing: (self.datasource?.article?.regular_price!)!), name_ar: (self.datasource?.article?.title_ar!)!, name_en: (self.datasource?.article?.title_en!)!)
                print(String(describing: (self.datasource?.article?.regular_price!)!), "EEE")
                self.badgeView.isHidden = false
                self.badgeView.setTitle(String(CartArr.count), for: .normal)
                cartdelegate?.setCart(cartcount: String(describing:CartArr.count + 1))
//                SharedHandler.DisAppearMessage(viewController: self, message: LanguageHelper.getString(forKey: "itemadded"))
                print("تمت إضافة منتج جديد لعربة التسوق")
                
            case CartArr.count:
                for i in CartArr{
                    let idCheck = i.product_id!
                    checkID.append(idCheck)
                }
                if checkID.contains(String(describing:self.datasource?.article?.id!)){
                    for i in CartArr{
                        let count = Int(i.qty!)
                        if i.product_id == String(describing:self.datasource?.article?.id!){
                            MyCartData.updateQTYCart(cart: i, qty: String(count! + 1))
                            self.badgeView.isHidden = false
                            self.badgeView.setTitle(String(CartArr.count), for: .normal)
                            print("تم إضافة هذا الإعلان من قبل")
                        }
                    }
                    
                }else{
                    MyCartData.addCartData(qty: "1", productID: String(describing:(self.datasource?.article?.id!)!), Image:  (self.datasource?.imgs_url!)! + "/" +  (self.datasource?.article?.image!)!, price: String(describing: (self.datasource?.article?.regular_price!)!), name_ar: (self.datasource?.article?.title_ar!)!, name_en: (self.datasource?.article?.title_en!)!)
                    print(String(describing: (self.datasource?.article?.regular_price!)!), "EEE")
                    cartdelegate?.setCart(cartcount: String(describing:CartArr.count + 1))
//                    SharedHandler.DisAppearMessage(viewController: self, message: LanguageHelper.getString(forKey: "itemadded"))
                    self.badgeView.isHidden = false
                    self.badgeView.setTitle(String(CartArr.count), for: .normal)
                }
                
            default:
                return
            }
            print(CartArr, "DSfreger",MyCartData.getCartData().2)
            
            cartdelegate?.setCart(cartcount: String(describing:MyCartData.getCartData().0.count))
            self.badgeView.setTitle(String(MyCartData.getCartData().0.count), for: .normal)
        }else{
            print("يوجد خطأ ما.")
        }
    }
    
    func hideOldUX(){
        self.rate1.isHidden = true
        self.rate2.isHidden = true
        self.rate3.isHidden = true
        self.rate4.isHidden = true
        self.rate5.isHidden = true
        self.rateBtn.isHidden = true
        self.ratetitle.isHidden = true
        self.myrateLbl.isHidden = true
        self.productRateBtn.isHidden = true
        self.providerImg.isHidden = true
        self.userLbl.isHidden = true
        self.userImg.isHidden = true
        self.ratingCountLbl.isHidden = true
        self.rateDescTV.isHidden = true
    }
    
    func createtocart(cart: CartBase){
        self.badgeView.isHidden = false
        self.badgeView.setTitle(String(newcartarr.count), for: .normal)
        newcartarr.append(cart)
        print(cartarr, "cgseck", newcartarr[0].product_id)
//        let x = newcartarr as [AnyObject]
        let data = NSKeyedArchiver.archivedData(withRootObject: newcartarr)
        let decoder = JSONDecoder()
        do{
            let model = try decoder.decode(CartBase.self, from: data)
            print(model)
        }catch{
            
        }
        print(newcartarr, "igigygi")
//        def.set(newcartarr[0] , forKey: "cartarr")
//        SharedHandler.DisAppearMessage(viewController: self, message: LanguageHelper.getString(forKey: "itemadded"))
    }
    
    func adjustUITextViewHeight(arg : UITextView, desc: String)
    {
        arg.text = desc.html2String
        arg.translatesAutoresizingMaskIntoConstraints = true
        arg.sizeToFit()
        self.view.layoutIfNeeded()
        arg.frame.size.height = arg.contentSize.height
        arg.isScrollEnabled = false
    }
}

extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String { html2AttributedString?.string ?? "" }
}

extension StringProtocol {
    var html2AttributedString: NSAttributedString? {
        Data(utf8).html2AttributedString
    }
    var html2String: String {
        html2AttributedString?.string ?? ""
    }
}

extension NSAttributedString {

    convenience init(htmlString html: String) throws {
        try self.init(data: Data(html.utf8), options: [
            .documentType: NSAttributedString.DocumentType.html,
            .characterEncoding: String.Encoding.utf8.rawValue
        ], documentAttributes: nil)
    }

}

//                        x["product_id"] = self.datasource?.article?.id!
//                        x["quantity"] = i["quantity"] as! Int + 1
//                        x["item_cost"] = self.datasource?.article?.regular_price!
//                        x["image"] = (self.datasource?.imgs_url!)! + "/" + (self.datasource?.article?.image!)!
//                        x["name_ar"] = self.datasource?.article?.title_ar!
//                        x["name_en"] = self.datasource?.article?.title_en!
////                        cartarr[counter]["quantity"] = i["quantity"] as! Int + 1
//                        if let row = cartarr.firstIndex(where: {$0["quantity"] as! Int == id}) {
//                               cartarr[row] = x
//                        }
                        
                        
                        
//                        cartarr = cartarr.map{
//                            ["product_id": $0["product_id"],"item_cost": $0["item_cost"],"image": $0["image"],"name_ar": $0["name_ar"], "name_en": $0["name_en"], "quantity": i["quantity"] as! Int + 1]
//
//                        }
//                        cartarr.remove(at: cartarr.lastIndex(where: { (i) -> Bool in
//                            return true
//                        })!)
