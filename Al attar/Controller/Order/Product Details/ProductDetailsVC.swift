//
//  ProductDetailsVC.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 8/21/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import UIKit

protocol CartDelegate{
    func setCart(cartcount:String)
}

@available(iOS 13.0, *)
class ProductDetailsVC: UIViewController,CartDelegate {

    @IBOutlet weak var productImg: UIImageView!
    @IBOutlet weak var productRateBtn: UIButtonX!
    @IBOutlet weak var providerImg: UIButtonX!
    @IBOutlet weak var productNameLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var tbl: UITableView!
    @IBOutlet weak var productDesc: UITextView!
    @IBOutlet weak var ratetitle: UILabel!
    @IBOutlet weak var userImg: UIButtonX!
    @IBOutlet weak var userLbl: UILabel!
    @IBOutlet weak var ratingCountLbl: UILabel!
    @IBOutlet weak var rateBtn: UIButtonX!
    @IBOutlet weak var rateDescTV: UITextView!
    @IBOutlet weak var myrateLbl: UILabel!
    @IBOutlet weak var badgeView: UIButtonX!
    
    @IBOutlet weak var rate5: UIButtonX!
    @IBOutlet weak var rate4: UIButtonX!
    @IBOutlet weak var rate3: UIButtonX!
    @IBOutlet weak var rate2: UIButtonX!
    @IBOutlet weak var rate1: UIButtonX!
    @IBOutlet weak var descRateTV: UITextViewX!
    @IBOutlet weak var addFavBtn: UIButtonX!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    var cartdelegate: CartDelegate?
    let CartArrr = MyCartData.getCartData().0 as? [CartData]
    var newcartarr = [CartBase]()
    var producturi = ""
    var checkuri = false
        
    let btn1 = UIButton(type: .custom)
    let btn2 = UIButton(type: .custom)
    let btn3 = UIButton(type: .custom)
    
    var datasource: product_details?
    var productid: Int?
    var cartarr = [[String:Any]]()
    var updatedcartarr = [[String:Any]]()
    var isfaved = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.badgeView.isHidden = true
        if let arr = def.array(forKey: "cartarr") as? [[String:Any]]{
         cartarr = arr
        }
//        hideOldUX()
        setDefault()
        if checkuri == true{
            getDataDL()
        }else{
            getData()
        }
        tbl.estimatedRowHeight = 50
        tbl.rowHeight = UITableView.automaticDimension
//        addFavBtn.addTarget(self, action: #selector(addtoCart), for: .touchUpInside)
    }
    
    func setCart(cartcount: String) {
        self.badgeView.isHidden = false
        self.badgeView.setTitle(cartcount, for: .normal)
       }
    
    
    @IBAction func submitBtn(_ sender: Any) {
        addtoCart()
//        addcart()
    }
    
    @IBAction func cartBtn(_ sender: Any) {
        let nextStoryboard: UIStoryboard = UIStoryboard(name: "Order", bundle: nil)
        let vc = nextStoryboard.instantiateViewController(withIdentifier: "CartVC") as! CartVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func setDefault(){
        
        
        setSemantic(view: self.view, languageKey: LanguageHelper.getCurrentLanguage())
        
//        myrateLbl.setText(forKey: "yourrate")
//        ratetitle.setText(forKey: "rate")
//        addFavBtn.setText(forKey: "addtocart")
        
//        if LanguageHelper.getCurrentLanguage() == "ar"{
//            changerate(button1: rate1, button2: rate2, button3: rate3, button4: rate4, button5: rate5)
//        }
     
        btn1.setImage(UIImage(named: "Add_to_favorite_button"), for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn1.addTarget(self, action: #selector(addtofavorites), for: .touchUpInside)
        btn2.setImage(UIImage(named: "Share_button"), for: .normal)
        btn2.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn3.setTitle(LanguageHelper.getString(forKey: "cancel"), for: .normal)
        btn3.setTitleColor(UIColor.black, for: .normal)
        btn1.tintColor = UIColor.black
        btn2.tintColor = UIColor.black
        btn3.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn3.addTarget(self, action: #selector(dismissVC), for: .touchUpInside)
//        cartdelegate?.setCart(cartcount: String(describing:CartArrr?.count))
        if MyCartData.getCartData().0.count != 0 {
            self.badgeView.isHidden = false
            self.badgeView.setTitle(String(describing: MyCartData.getCartData().0.count), for: .normal)
        }
        let item3 = UIBarButtonItem(customView: btn3)
        let item2 = UIBarButtonItem(customView: btn2)
        let item1 = UIBarButtonItem(customView: btn1)
        self.navigationItem.setRightBarButtonItems([item1,item2], animated: true)
        self.navigationItem.setLeftBarButtonItems([item3], animated: true)
    }
}
