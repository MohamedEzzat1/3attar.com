//
//  CategoryPageCollectionEx.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 7/26/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation

@available(iOS 13.0, *)
extension CategoryPageVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if checktype == 1{
//            return (self.productsource?.categories![0].products?.count) ?? 0
            return self.productsarr?.count ?? 0
        }else if checktype == 2{
            return self.productsarr?.count ?? 0
        }else {
            return self.productsArray?.article?.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductsCell", for: indexPath) as! ProductsCell
        cell.cartdelegete = self
        cell.greenview.isHidden = true
        switch checktype {
        case 3:
            if let brands = self.brandsource?.brands{
                for i in brands{
                    
                    if self.productsArray?.article?[indexPath.row].brand_id == i.id{
                        let base_url = self.brandsource?.imgs_url
                        let imgurl   = brands[brands.index(where: { $0.id == i.id }) ?? 0 ].image
                        
                        let url = base_url! + "/" + imgurl!
                        cell.brandimage = url
                        let u = URL(string: url)
                        cell.brandImg.cornerRadius = 22.5
                        cell.brandImg.kf.setImage(with: u)
                        if cell.brandImg.image == nil{
                            cell.brandImg.image = UIImage(named: "logo_splash_screen")
                        }
                    }
                }
            }
            cell.productid = String(describing:(self.productsArray?.article?[indexPath.row].id!)!)
            cell.cellPrice = self.productsArray?.article?[indexPath.row].regular_price as! String
            cell.celltitle_ar = self.productsArray?.article?[indexPath.row].title_ar as! String
            cell.celltitle_en = self.productsArray?.article?[indexPath.row].title_en as! String

            if LanguageHelper.getCurrentLanguage() == "ar"{
                cell.addnewItem.roundCorners(corners: [.topRight, .bottomLeft], radius: 10)
                cell.prdouctName.text = self.productsArray?.article?[indexPath.row].title_ar
                cell.desc.text = self.productsArray?.article?[indexPath.row].description_ar
                cell.price.text = (self.productsArray?.article?[indexPath.row].regular_price)! + " جنيه"
            }else{
                cell.addnewItem.roundCorners(corners: [.topLeft, .bottomRight], radius: 10)
                cell.prdouctName.text = self.productsArray?.article?[indexPath.row].title_en
                cell.desc.text = self.productsArray?.article?[indexPath.row].description_en
                cell.price.text = (self.productsArray?.article?[indexPath.row].regular_price)! + " LE"
            }
    ////        cell.price.text = self.productsource?.categories?[indexPath.row].regular_price
            var  base_url =  self.productsArray?.imgs_url
           
            let imgurl   = self.productsArray?.article?[indexPath.row].image
            let url = base_url! + "/" + imgurl!
            
    //        cell.brandimage = url
            cell.cellimg = url
            print(cell.cellimg, "SAdqwQ", imgurl!)
            let u = URL(string: url)
            cell.img.kf.setImage(with: u)
            if cell.img.image == nil{
                cell.img.image = UIImage(named: "logo_splash_screen")
            }
        default:
            if let brands = self.brandsource?.brands{
                for i in brands{
                    
                    if self.productsarr?[indexPath.row].brand_id == i.id{
                        let base_url = self.brandsource?.imgs_url
                        let imgurl   = brands[brands.index(where: { $0.id == i.id }) ?? 0 ].image
                        
                        let url = base_url! + "/" + imgurl!
                        cell.brandimage = url
                        let u = URL(string: url)
                        cell.brandImg.cornerRadius = 22.5
                        cell.brandImg.kf.setImage(with: u)
                        if cell.brandImg.image == nil{
                            cell.brandImg.image = UIImage(named: "logo_splash_screen")
                        }
                    }
                }
            }
            cell.productid = String(describing:(self.productsarr?[indexPath.row].id!)!)
            cell.cellPrice = self.productsarr?[indexPath.row].regular_price as! String
            cell.celltitle_ar = self.productsarr?[indexPath.row].title_ar as! String
            cell.celltitle_en = self.productsarr?[indexPath.row].title_en as! String

            if LanguageHelper.getCurrentLanguage() == "ar"{
                cell.addnewItem.roundCorners(corners: [.topRight, .bottomLeft], radius: 10)
                cell.prdouctName.text = self.productsarr?[indexPath.row].title_ar
                cell.desc.text = self.productsarr?[indexPath.row].description_ar
                cell.price.text = (self.productsarr?[indexPath.row].regular_price)! + " جنيه"
            }else{
                cell.addnewItem.roundCorners(corners: [.topLeft, .bottomRight], radius: 10)
                cell.prdouctName.text = self.productsarr?[indexPath.row].title_en
                cell.desc.text = self.productsarr?[indexPath.row].description_en
                cell.price.text = (self.productsarr?[indexPath.row].regular_price)! + " LE"
            }
    ////        cell.price.text = self.productsource?.categories?[indexPath.row].regular_price
            if let  base_url =  self.productsourcev2?.imgs_url{
                let imgurl   = self.productsarr?[indexPath.row].image
                let url = base_url + "/" + imgurl!
        //        cell.brandimage = url
                cell.cellimg = url
                let u = URL(string: url)
                cell.img.kf.setImage(with: u)
                
            }else if let base = self.productsourcedatav2?.imgs_url{
                let imgurl   = self.productsarr?[indexPath.row].image
                let url = base + "/" + imgurl!
        //        cell.brandimage = url
                cell.cellimg = url
                let u = URL(string: url)
                cell.img.kf.setImage(with: u)
            }
            if cell.img.image == nil{
                cell.img.image = UIImage(named: "logo_splash_screen")
            }
        }
        
        switch checkuri {
        case true:
//            checkuri = false
            if checktype == 1{
                cell.cellBaseimg = self.productsourcev2?.imgs_url as! String
            }else if checktype == 2{
                print("??????????")
                cell.cellBaseimg = self.productsourcedataDL?.imgs_url as! String
            }
        case false:
            if checktype == 1 || checktypeglb == 1{
                cell.cellbrandid = self.productsarr?[indexPath.row].brand_id ?? 0
                cell.brandBtn.addTarget(self, action: #selector(openBrandProducts), for: .touchUpInside)
                cell.cellBaseimg = self.productsourcev2?.imgs_url as! String
            }else if checktype == 2{
                cell.cellBaseimg = "https://app.3attar.com/uploads/images/products"//self.productsourcedatav2?.imgs_url as! String
            }
        }
        
//        cell.addnewItem.addTarget(self, action: #selector(addcounter), for: .touchUpInside)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductsCell", for: indexPath) as! ProductsCell
        
        let nextStoryboard: UIStoryboard = UIStoryboard(name: "Order", bundle: nil)
        let vc = nextStoryboard.instantiateViewController(withIdentifier: "ProductDetailsVC") as! ProductDetailsVC
        vc.productid = self.productsarr?[indexPath.row].id//self.productsource?.categories![0].products?[indexPath.row].id
        vc.cartdelegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView
    {
        if kind == UICollectionView.elementKindSectionHeader
        {
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "catheader", for: indexPath) as! CatReusableView
//            if checktype == 1{
//                let url = URL(string:self.productsourcev2?.imgs_url ?? "" + (self.productsourcev2?.category?.image)! ?? "" )
//                headerView.img.kf.setImage(with: url)
//            }else if checktype == 2{
//                let url = URL(string:self.productsourcedataDL?. ?? "" + (self.productsourcedataDL?.category?.image)! ?? "" )
//                headerView.img.kf.setImage(with: url)
//            }
            let url = URL(string:self.imageURL)
            headerView.img.kf.setImage(with: url)
            headerView.catTitle.text = self.cattitle
            headerView.filterBtn.setText(forKey: "chooseby")
            headerView.filterBtn.addTarget(self, action: #selector(openFilter), for: .touchUpInside)
            return headerView
        }
        return UICollectionReusableView()
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplaySupplementaryView view: UICollectionReusableView, forElementKind elementKind: String, at indexPath: IndexPath) {
        self.navigationItem.title = ""
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplayingSupplementaryView view: UICollectionReusableView, forElementOfKind elementKind: String, at indexPath: IndexPath) {
        self.navigationItem.title = self.cattitle
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
//        return CGSize(width: 180, height: 228)
        let height = view.frame.size.height
        let width = view.frame.size.width
//        // in case you you want the cell to be 40% of your controllers view
        return CGSize(width: self.collection.frame.width * 0.46, height: self.collection.frame.height * 0.4)
//        return CGSize(width: width * 0.4, height: height * 0.4)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0.0
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        print("Hania")
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if (scrollView.contentOffset.y < self.lastContentOffset) {
            //Scrolling up
        }
        else if (scrollView.contentOffset.y > self.lastContentOffset) {
            //Scrolling down
            switch checkuri {
            case true:
                if checktype == 1{
                    ReloadData()
                }else if checktype == 2{
                    ReloadDatabyBrandDL()
                }
            case false:
                if checktype == 1{
                    ReloadData()
                }else{
                  ReloadDatabyBrand()
                }
            }
        }
        self.lastContentOffset = scrollView.contentOffset.y
        
    }
}
