//
//  CategoryPageEx.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 7/26/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

@available(iOS 13.0, *)
extension CategoryPageVC{
    
    func getData(){
        SharedHandler.startloadingGIF(self.view)//startSpinner(spinner, v, self.view)
        let headers : HTTPHeaders  = [
                "cache-control": "no-cache"
            ]
//        SharedHandler
        print(URLs.productCatsv2 + "\(self.catid)", "hjgjfjh")
        APIManager.sharedInstance.getRequest(URLs.productCatsv2 + "\(self.catid)", header: headers) { (res) in
            SharedHandler.stoploadingGIF(self.view)//stopSpinner(self.spinner, v, self.view)
            let decoder = JSONDecoder()
            do {
                if let products = res.data{
                    let model = try decoder.decode(_Product_By_CatsV2.self, from: res.data!)
                    self.productsourcev2 = model
                    print(self.productsourcev2?.imgs_url, " EWWRQW")
                    self.productsarr = self.productsourcev2?.products?.data
                    self.nextpage = self.productsourcev2?.products?.next_page_url ?? ""
                    print(self.productsarr, self.productsourcev2?.products?.data)
//                    for i in p{
//                        self.productsarr?.append(i)
//                    }
                    
                    print("modelproducts ==> \(self.productsourcev2!)")
                    self.collection.dataSource = self
                    self.collection.delegate   = self
                    self.collection.reloadData()
                }
            }catch{
                print(JSON(res.value))
                SharedHandler.alertDialog2lang(self, msgEg: "invalid data Parsing.", msgAr: "خطأ في تحويل البيانات.")
            }

        }
    }
    
    func getDataDL(){
        SharedHandler.startSpinner(spinner, v, self.view)
        let headers : HTTPHeaders  = [
                "cache-control": "no-cache"
            ]
//        SharedHandler
        print(URLs.productCatsv2 + "\(self.catid)", "hjgjfjh")
        APIManager.sharedInstance.getRequest(URLs.categoryDL + "\(self.producturi)", header: headers) { (res) in
            SharedHandler.stopSpinner(self.spinner, v, self.view)
            let decoder = JSONDecoder()
            do {
                if let products = res.data{
                    let model = try decoder.decode(_Product_By_CatsV2.self, from: res.data!)
                    self.productsourcev2 = model
                    self.productsarr = self.productsourcev2?.products?.data
                    self.nextpage = self.productsourcev2?.products?.next_page_url ?? ""
                    print(self.productsarr, self.productsourcev2?.products?.data)
//                    for i in p{
//                        self.productsarr?.append(i)
//                    }
                    
                    print("modelproducts ==> \(self.productsourcev2!)")
                    self.collection.dataSource = self
                    self.collection.delegate   = self
                    self.collection.reloadData()
                }
            }catch{
                print(JSON(res.value))
                SharedHandler.alertDialog2lang(self, msgEg: "invalid data Parsing.", msgAr: "خطأ في تحويل البيانات.")
            }

        }
    }
    
    func ReloadData(){
        SharedHandler.startSpinner(spinner, v, self.view)
        let headers : HTTPHeaders  = [
                "cache-control": "no-cache"
            ]
//        SharedHandler
        print(nextpage, "hjgjfjh098ojjko")
        APIManager.sharedInstance.getRequest(nextpage, header: headers) { (res) in
            SharedHandler.stopSpinner(self.spinner, v, self.view)
            let decoder = JSONDecoder()
            do {
                if let products = res.data{
                    let model = try decoder.decode(_Product_By_CatsV2.self, from: res.data!)
                    self.productsourcev2 = model
                    let p = (self.productsourcev2?.products?.data)!
                    print(self.productsarr, self.productsourcev2?.products?.data)
                    for i in p{
                        self.productsarr?.append(i)
                    }
                    self.nextpage = self.productsourcev2?.products?.next_page_url ?? ""
                    print("modelproducts ==> \(self.productsourcev2!)")
                    self.collection.dataSource = self
                    self.collection.delegate   = self
                    self.collection.reloadData()
                }
            }catch{
                print(JSON(res.value))
                SharedHandler.alertDialog2lang(self, msgEg: "invalid data Parsing.", msgAr: "خطأ في تحويل البيانات.")
            }

        }
    }
    
    func getDatabyBrand(){
            SharedHandler.startSpinner(spinner, v, self.view)
            let headers : HTTPHeaders  = [
                    "cache-control": "no-cache"
                ]
    //        SharedHandler
            print(URLs.selectbrand + "\(self.brandid)", "hjgjfjh")
            APIManager.sharedInstance.getRequest(URLs.selectbrand + "\(self.brandid)", header: headers) { (res) in
                SharedHandler.stopSpinner(self.spinner, v, self.view)
                let decoder = JSONDecoder()
                do {
                    if let products = res.data{
                        let model = try decoder.decode(_Product_By_BrandsV2.self, from: res.data!)
                        self.productsourcedatav2 = model
                        print(self.productsourcev2?.imgs_url, " EWWRQW")
                        print(self.productsarr,"modelproducts ==> \(self.productsourcedatav2!)")
                        if LanguageHelper.getCurrentLanguage() == "ar"{
                            self.cattitle = model.brand?[0].title_ar as! String
                        }else{
                            self.cattitle = model.brand?[0].title_en as! String
                        }
                        self.productsarr = self.productsourcedatav2?.products?.data
                        self.nextpage = self.productsourcedatav2?.products?.next_page_url ?? ""
                        
                        self.collection.dataSource = self
                        self.collection.delegate   = self
                        self.collection.reloadData()
                    }
                }catch{
                    print(JSON(res.value))
                    SharedHandler.alertDialog2lang(self, msgEg: "invalid data Parsing.", msgAr: "خطأ في تحويل البيانات.")
                }

            }
        }
    
    func ReloadDatabyBrand(){
            SharedHandler.startSpinner(spinner, v, self.view)
            let headers : HTTPHeaders  = [
                    "cache-control": "no-cache"
                ]
    //        SharedHandler
            print(nextpage, "hjgjfjh")
            APIManager.sharedInstance.getRequest(nextpage, header: headers) { (res) in
                SharedHandler.stopSpinner(self.spinner, v, self.view)
                let decoder = JSONDecoder()
                do {
                    if let products = res.data{
                        let model = try decoder.decode(_Product_By_BrandsV2.self, from: res.data!)
                        self.productsourcedatav2 = model

                        let p = (self.productsourcedatav2?.products?.data)!
                        for i in p{
                            self.productsarr?.append(i)
                        }
                        print("modelproducts ==> \(self.productsourcedatav2!)")
                        self.nextpage = self.productsourcev2?.products?.next_page_url ?? ""
                        self.collection.dataSource = self
                        self.collection.delegate   = self
                        self.collection.reloadData()
                    }
                }catch{
                    print(JSON(res.value))
                    SharedHandler.alertDialog2lang(self, msgEg: "invalid data Parsing.", msgAr: "خطأ في تحويل البيانات.")
                }

            }
        }
    
    func getBrands(){
        let headers : HTTPHeaders  = [
            "cache-control": "no-cache"
        ]
        
        
        SharedHandler.startSpinner(spinner, v, self.view)
        APIManager.sharedInstance.getRequest(URLs.brands, header: headers) { (res) in
            SharedHandler.stopSpinner(self.spinner, v, self.view)
            let decoder = JSONDecoder()
            do{
                if let data = res.data{
                    let model = try decoder.decode(Brands_Base.self, from: data)
                    self.brandsource = model
                    self.getData()
                    
                }else{
                    self.getBrands()
                }
            }catch{
                
            }
        }
    }
    
    func getDatabyBrandDL(){
            SharedHandler.startSpinner(spinner, v, self.view)
            let headers : HTTPHeaders  = [
                    "cache-control": "no-cache"
                ]
    //        SharedHandler
            print(URLs.brandDL + "\(self.producturi)", "hjgjfjh")
            APIManager.sharedInstance.getRequest(URLs.brandDL + "\(self.producturi)", header: headers) { (res) in
                SharedHandler.stopSpinner(self.spinner, v, self.view)
                let decoder = JSONDecoder()
                do {
                    if let products = res.data{
                        let model = try decoder.decode(_Product_By_BrandsDL.self, from: res.data!)
                        self.productsourcedataDL = model
                        print(self.productsarr,"modelproducts ==> \(self.productsourcedataDL!)")
                        if LanguageHelper.getCurrentLanguage() == "ar"{
                            self.cattitle = model.brand?.title_ar as! String
                        }else{
                            self.cattitle = model.brand?.title_en as! String
                        }
                        self.productsarr = self.productsourcedataDL?.products?.data
                        self.nextpage = self.productsourcedataDL?.products?.next_page_url ?? ""
                        
                        self.collection.dataSource = self
                        self.collection.delegate   = self
                        self.collection.reloadData()
                    }
                }catch{
                    print(JSON(res.value))
                    SharedHandler.alertDialog2lang(self, msgEg: "invalid data Parsing.", msgAr: "خطأ في تحويل البيانات.")
                }

            }
        }
    
    func ReloadDatabyBrandDL(){
        SharedHandler.startSpinner(spinner, v, self.view)
        let headers : HTTPHeaders  = [
                "cache-control": "no-cache"
        ]
        print(nextpage, "hjgjfjh")
        APIManager.sharedInstance.getRequest(nextpage, header: headers) { (res) in
            SharedHandler.stopSpinner(self.spinner, v, self.view)
                let decoder = JSONDecoder()
                do {
                    if let products = res.data{
                        let model = try decoder.decode(_Product_By_BrandsDL.self, from: res.data!)
                        self.productsourcedataDL = model

                        let p = (self.productsourcedataDL?.products?.data)!
                        for i in p{
                            self.productsarr?.append(i)
                        }
                        print("modelproducts ==> \(self.productsourcedataDL!)")
                        self.nextpage = self.productsourcev2?.products?.next_page_url ?? ""
                        self.collection.dataSource = self
                        self.collection.delegate   = self
                        self.collection.reloadData()
                    }
                }catch{
                    print(JSON(res.value))
                    SharedHandler.alertDialog2lang(self, msgEg: "invalid data Parsing.", msgAr: "خطأ في تحويل البيانات.")
                }

            }
        }
    
    @objc func addcounter(){
        self.badgeView.isHidden = false
        self.badgeView.setTitle(String(describing: cartarr.count + 1), for: .normal)
    }
    
    @objc func openFilter(){
        let nextStoryboard: UIStoryboard = UIStoryboard(name: "Order", bundle: nil)
        let vc = nextStoryboard.instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
        vc.catsource = catsource
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func openBrandProducts(){
        let nextStoryboard: UIStoryboard = UIStoryboard(name: "Order", bundle: nil)
        let vc = nextStoryboard.instantiateViewController(withIdentifier: "CategoryPageVC") as! CategoryPageVC
        vc.cartdelegate = self
        vc.checktype = 2
        print(vc.checktype,"Sdfewfwe")
        vc.brandid = cellbrandtag// self.searchsource?.products?[cellbrandtag].brand_id ?? 0
        vc.brandsource = brandsource
        /// Solving URL String by removing "\n"
        let url = pbrandimg
        vc.imageURL = url
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
