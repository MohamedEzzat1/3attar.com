//
//  SignupEx.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 7/23/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import UIKit

extension SignupVC{
    
    func getData(){
        SharedHandler.startSpinner(spinner, v, self.view)
        
        let body = [
            "name":"\(nameTF.text!)",
            "email":"\(emailTF.text!)",
            "mobile":"\(phoneTF.text!)",
            "password":"\(passTF.text!)"
        ]
        
        print(body)
        APIManager.sharedInstance.postRequest(URLs.signup, Parameters: body) { (res) in
            SharedHandler.stopSpinner(self.spinner, self.v, self.view)
            let decoder = JSONDecoder()
            
            do{
                let model = try decoder.decode(Login_Base.self, from: res.data!)
                self.datasource = model
                
                let status = model.success
                if status == true{
                    def.set(true, forKey: _LoggedIn)
                    def.set(self.datasource?.user?.email, forKey: "logEmail")
                    def.set(self.datasource?.user?.name, forKey: "logName")
                    def.set(self.datasource?.user?.mobile, forKey: "logPhone")
                    def.set(self.datasource?.user?.image, forKey: "logImg")
                    def.set(self.datasource?.token, forKey: "logToken")
                    print("model ==> \(self.datasource!)")
                    
                    
                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
                    let viewController = mainStoryboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
                    UIApplication.shared.keyWindow?.rootViewController = viewController
                }else{
                    SharedHandler.alertDialog2lang(self, msgEg: "\(model.message!)", msgAr: "\(model.message!)")
                }
                print(model, "dsfewfwe")
                
            }catch{
                
            }
        }
    }
    
}
