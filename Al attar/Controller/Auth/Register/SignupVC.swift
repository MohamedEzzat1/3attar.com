//
//  SignupVC.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 7/19/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import UIKit

class SignupVC: UIViewController {

    @IBOutlet weak var welcomeBtn: UILabel!
    @IBOutlet weak var signupLbl: UILabel!
    @IBOutlet weak var nameTF: UITextFieldX!
    @IBOutlet weak var phoneTF: UITextFieldX!
    @IBOutlet weak var emailTF: UITextFieldX!
    @IBOutlet weak var passTF: UITextFieldX!
    @IBOutlet weak var joinBtn: UIButtonX!
    @IBOutlet weak var loginBtn: UIButtonX!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    var v = UIView()
    var datasource : Login_Base?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.spinner.stopAnimating()
        setDefault()
        self.loginBtn.addTarget(self, action: #selector(openLogin), for: .touchUpInside)

        // Do any additional setup after loading the view.
    }
    
    @objc func openLogin(){
        self.pushVC(storyboard: "Main", viewController: "LoginVC")
    }
    @IBAction func closeBtn(_ sender: Any) {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }
    
    @IBAction func joinBtn(_ sender: Any) {
        getData()
    }
    
    func setDefault(){
        if LanguageHelper.getCurrentLanguage() == "ar"{
            
        }else{
            
        }
        self.setSemantic(view: self.view, languageKey: LanguageHelper.getCurrentLanguage())
        self.nameTF.setAttributedPlaceholder(forKey: "name", withColor: UIColor(red: 87/255, green: 95/255, blue: 98/255, alpha: 1))
        self.phoneTF.setAttributedPlaceholder(forKey: "phone", withColor: UIColor(red: 87/255, green: 95/255, blue: 98/255, alpha: 1))
        self.passTF.setAttributedPlaceholder(forKey: "pass", withColor: UIColor(red: 87/255, green: 95/255, blue: 98/255, alpha: 1))
        self.emailTF.setAttributedPlaceholder(forKey: "email", withColor: UIColor(red: 87/255, green: 95/255, blue: 98/255, alpha: 1))
        self.loginBtn.setText(forKey: "openLogin")
        self.welcomeBtn.setText(forKey: "welcome")
        self.signupLbl.setText(forKey: "signuptext")
        self.joinBtn.setText(forKey: "join")
    }
    
}
