//
//  LoginVC.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 7/19/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {

    var datasource: Login_Base?
    
    @IBOutlet weak var welcomeLbl: UILabel!
    @IBOutlet weak var loginLbl: UILabel!
    @IBOutlet weak var emailTF: UITextFieldX!
    @IBOutlet weak var passTF: UITextFieldX!
    @IBOutlet weak var loginBtn: UIButtonX!
    @IBOutlet weak var signupBtn: UIButtonX!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var forgetBtn: UIButtonX!
    
    var v = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.spinner.stopAnimating()
        self.signupBtn.addTarget(self, action: #selector(openSignup), for: .touchUpInside)
        self.forgetBtn.addTarget(self, action: #selector(openPass), for: .touchUpInside)
        setDefault()
    }
    
    @objc func openSignup(){
        self.pushVC(storyboard: "Main", viewController: "SignupVC")
    }
    
    
    @IBAction func closeBtn(_ sender: Any) {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }
    @IBAction func loginBtn(_ sender: Any) {
        getData()
        
    }
    
    @objc func openPass(){
        self.presentVC(storyboard: "Main", viewController: "ForgetPassVC", animated: true)
    }
    
    func setDefault(){
        if LanguageHelper.getCurrentLanguage() == "ar"{
            
        }else{
            
        }
        self.setSemantic(view: self.view, languageKey: LanguageHelper.getCurrentLanguage())
        self.passTF.setAttributedPlaceholder(forKey: "pass", withColor: UIColor(red: 87/255, green: 95/255, blue: 98/255, alpha: 1))
        self.emailTF.setAttributedPlaceholder(forKey: "email", withColor: UIColor(red: 87/255, green: 95/255, blue: 98/255, alpha: 1))
        self.loginBtn.setText(forKey: "login")
        self.welcomeLbl.setText(forKey: "welcome")
        self.loginLbl.setText(forKey: "logintext")
        self.signupBtn.setText(forKey: "opensignup")
        self.forgetBtn.setText(forKey: "forgetpass")
        self.forgetBtn.isHidden = true
    }
}
