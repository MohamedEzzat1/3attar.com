//
//  LoginEx.swift
//  greeb
//
//  Created by apple on 1/17/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import Adjust

@available(iOS 11.0, *)
extension LoginVC{
    

    
    func getData()  {
        
        SharedHandler.startSpinner(spinner, v, self.view)
        let header = [
            "Content-Type": "application/json"
                     ]
        let body = [
            "email": "\(emailTF.text!)",
            "password": "\(passTF.text!)"
//            "token": def.string(forKey: _Token) ?? "dfg65df54g6d5f4g65d4fg6565e65e65re56"
            
        ]
        
        APIManager.sharedInstance.postRequest(URLs.login,Parameters: body) { (res) in
            SharedHandler.stopSpinner(self.spinner, self.v, self.view)
            print("sajkasdka", res.response?.statusCode)
            if  res.error == nil {
               print("@@\(String.init(data: res.data!, encoding: String.Encoding.utf8))")
                if res.response?.statusCode == 1001{
                    print("Time is out , ffff")
                    SharedHandler.alertDialog2lang(self, msgEg: "The request timed out.", msgAr: "تم انتهاء الطلب.")
                }//else if res.response?.statusCode ==
                let decoder = JSONDecoder()
                do{
                    let model = try decoder.decode(Login_Base.self, from: res.data!)
                    self.datasource = model
                    print("!!\(model.message)")
                    if model.success! == false{
                        SharedHandler.alertDialog2lang(self, msgEg: "\(model.message!)", msgAr: "\(model.message!)")
                    }else{
                        //20k1ox
                        let event = ADJEvent(eventToken: "20k1ox")
                        Adjust.trackEvent(event)
//                        def.set(self.datasource!.user!.id!, forKey: _ID)
                        def.set(true, forKey: _LoggedIn)
                        def.set(self.datasource?.user?.email, forKey: "logEmail")
                        def.set(self.datasource?.user?.name, forKey: "logName")
                        def.set(self.datasource?.user?.mobile, forKey: "logPhone")
                        def.set(self.datasource?.user?.image, forKey: "logImg")
                        def.set(self.datasource?.token, forKey: "logToken")
                        print("model ==> \(self.datasource!)")
                        
                        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
                        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
                        UIApplication.shared.keyWindow?.rootViewController = viewController
                        
                    }
           
                    
                    
                }catch{
                    SharedHandler.alertDialog2lang(self, msgEg: "invalid data Parsing.", msgAr: "خطأ في تحويل البيانات.")
                    print("model ==> \(self.datasource!)")
                }
               
                
            }else{
                print("koijsiod")
                SharedHandler.alertDialog2lang(self, msgEg: res.error?.localizedDescription ?? "No internet connection", msgAr: "لا يوجد انترنت")
                
            }
        }
    }
}
