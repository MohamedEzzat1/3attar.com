//
//  ForgetPassVC.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 7/22/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import UIKit

class ForgetPassVC: UIViewController {

    @IBOutlet weak var forgetLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var emailTF: UITextFieldX!
    @IBOutlet weak var changeBtn: UIButtonX!
    @IBOutlet weak var signupBtn: UIButtonX!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.signupBtn.addTarget(self, action: #selector(openSignup), for: .touchUpInside)
        self.setDefault()

    }
    
    @objc func openSignup(){
        self.presentVC(storyboard: "Main", viewController: "SignupVC", animated: false)
    }

    @IBAction func closeBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func setDefault(){
        if LanguageHelper.getCurrentLanguage() == "ar"{
            
        }else{
            
        }
        self.setSemantic(view: self.view, languageKey: LanguageHelper.getCurrentLanguage())
        self.emailTF.setAttributedPlaceholder(forKey: "email", withColor: UIColor(red: 87/255, green: 95/255, blue: 98/255, alpha: 1))
        self.forgetLbl.setText(forKey: "forgetpass")
        self.emailLbl.setText(forKey: "enteremail")
        self.signupBtn.setText(forKey: "opensignup")
        self.changeBtn.setText(forKey: "changepass")

    }
}
