//
//  SplashVC.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 7/18/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import UIKit

class SplashVC: UIViewController {

    @IBOutlet weak var startBtn: UIButtonX!
    override func viewDidLoad() {
        super.viewDidLoad()
        def.set(0, forKey: "menuadd")
        LanguageHelper.setCurrentLanguage(langugaeCode: "ar")
        self.startBtn.isHidden = true
        if currentReachabilityStatus == .notReachable{
            SharedHandler.alertDialog2lang(self, msgEg: "Internet Connection Lost", msgAr: "لا يوجد انترنت")
        }else{
            if def.bool(forKey: _FirstLaunch) != false{
                self.startBtn.isHidden = true
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    if def.bool(forKey: _LoggedIn){
                        self.pushVC(storyboard: "Home", viewController: "SWRevealViewController")
                    }else{
                        self.pushVC(storyboard: "Home", viewController: "SWRevealViewController")
                    }
                }
            }else{
                self.startBtn.isHidden = false
                self.startBtn.addTarget(self, action: #selector(openApp), for: .touchUpInside)
            }
        }
        
    }
    
    @objc func openApp(){
        LanguageHelper.setCurrentLanguage(langugaeCode: "ar")
        def.set(true, forKey: _FirstLaunch)
        self.pushVC(storyboard: "Home", viewController: "SWRevealViewController")
    }
    
    

}
