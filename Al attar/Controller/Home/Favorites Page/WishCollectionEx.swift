//
//  WishCollectionEx.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 7/27/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation

@available(iOS 13.0, *)
extension WishVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print(self.datasource?.wishlist?.count)
        return self.datasource?.wishlist?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductsCell", for: indexPath) as! ProductsCell
        cell.productid = String(describing:self.datasource?.wishlist?[indexPath.row].user_wishlist_items_a_p_i?[0].id)
                cell.cellPrice = self.datasource?.wishlist![indexPath.row].user_wishlist_items_a_p_i?[0].regular_price as! String
//                cell.cellBaseimg = self.datasource?.imgs_url as! String
                cell.celltitle_ar = self.datasource?.wishlist![indexPath.row].user_wishlist_items_a_p_i?[0].title_ar as! String
                cell.celltitle_en = self.datasource?.wishlist![indexPath.row].user_wishlist_items_a_p_i?[0].title_en as! String
        cell.cellbrandid = self.datasource?.wishlist![indexPath.row].user_wishlist_items_a_p_i?[0].brand_id ?? 0
                if LanguageHelper.getCurrentLanguage() == "ar"{
                    cell.addnewItem.roundCorners(corners: [.topRight, .bottomLeft], radius: 10)
                    cell.prdouctName.text = self.datasource?.wishlist![indexPath.row].user_wishlist_items_a_p_i![0].title_ar
                    cell.desc.text = self.datasource?.wishlist![indexPath.row].user_wishlist_items_a_p_i![0].description_ar
                    cell.price.text = (self.datasource?.wishlist![indexPath.row].user_wishlist_items_a_p_i![0].regular_price)! + " جنيه"
                }else{
                    cell.addnewItem.roundCorners(corners: [.topLeft, .bottomRight], radius: 10)
                    cell.prdouctName.text = self.datasource?.wishlist![indexPath.row].user_wishlist_items_a_p_i![0].title_en
                    cell.desc.text = self.datasource?.wishlist![indexPath.row].user_wishlist_items_a_p_i![0].description_en
                    cell.price.text = (self.datasource?.wishlist![indexPath.row].user_wishlist_items_a_p_i![0].regular_price)! + " LE"
                }
        //        cell.price.text = self.productsource?.categories?[indexPath.row].regular_price
//                let base_url = self.datasource?.imgs_url
        
        let imgurl   = self.datasource?.wishlist?[indexPath.row].user_wishlist_items_a_p_i?[0].image
                let url = imgurl!
        print(url, "pdsfdsF")
                let u = URL(string: url)
        print(u, "dsafewfW")
        cell.img.kf.setImage(with: u)
        if cell.img.image == nil{
            cell.img.image = UIImage(named: "logo_splash_screen")
        }
        
        if let brands = self.brandsource?.brands{
            var glburl : URL?
            for i in brands{
                
                if self.datasource?.wishlist?[indexPath.row].user_wishlist_items_a_p_i?[0].brand_id == i.id{
                    let base_url = self.brandsource?.imgs_url
                    let imgurl   = brands[brands.index(where: { $0.id == i.id }) ?? 0 ].image
                    
                    let url = base_url! + "/" + imgurl!
                    print(brands.index(where: { $0.id == i.id }) ?? 0 , "ioouo", url)
                    let u = URL(string: url)
                    glburl = u
                    cell.brandImg.cornerRadius = 22.5
                    cell.brandImg.kf.setImage(with: glburl)
                    if cell.brandImg.image == nil{
                        cell.brandImg.image = UIImage(named: "logo_splash_screen")
                    }
                }
            }
            cell.brandBtn.addTarget(self, action: #selector(openBrandProducts), for: .touchUpInside)
//            cell.brandBtn.isHidden = true
            
        }
//        cell.addnewItem.addTarget(self, action: #selector(addcounter), for: .touchUpInside)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductsCell", for: indexPath) as! ProductsCell
        
        let nextStoryboard: UIStoryboard = UIStoryboard(name: "Order", bundle: nil)
        let vc = nextStoryboard.instantiateViewController(withIdentifier: "ProductDetailsVC") as! ProductDetailsVC
        vc.productid = self.datasource?.wishlist?[indexPath.row].user_wishlist_items_a_p_i?[0].id
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    //        return CGSize(width: 180, height: 228)
            return CGSize(width: self.collection.frame.width * 0.46, height: self.collection.frame.height * 0.4)
        }
    
}
