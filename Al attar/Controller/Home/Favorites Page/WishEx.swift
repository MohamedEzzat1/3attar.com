//
//  WishEx.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 7/27/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation
import Alamofire

@available(iOS 13.0, *)
extension WishVC{
    
    func getData(){
        let headers : HTTPHeaders  = [
            "authorization": "bearer \(def.string(forKey: "logToken") ?? "")",
        "cache-control": "no-cache",
            ]
        print(headers,   URLs.wishes)
        
        APIManager.sharedInstance.getRequest(URLs.wishes, header: headers) { (res) in
            print(res.value)
            let decoder = JSONDecoder()
            do{
                let model = try decoder.decode(_Wishes.self, from: res.data!)
                self.datasource = model
                print(model.wishlist, "psdDS")
                if model.wishlist?.count == 0{
                    self.collection.isHidden = true
                    self.imagePAge.isHidden = false
                    self.descLbl.isHidden = false
                    self.startBtn.isHidden = false
                    self.pageTitle.isHidden = false
                    print("test")
                }else{
                    self.collection.isHidden = false
                    self.imagePAge.isHidden = true
                    self.descLbl.isHidden = true
                    self.startBtn.isHidden = true
                    self.pageTitle.isHidden = true
                    self.collection.delegate = self
                    self.collection.dataSource = self
                    self.collection.reloadData()
                    print("tes3t")
                }
            }catch{
                
            }
        }
    }
    
    @objc func openBrandProducts(){
        let nextStoryboard: UIStoryboard = UIStoryboard(name: "Order", bundle: nil)
        let vc = nextStoryboard.instantiateViewController(withIdentifier: "CategoryPageVC") as! CategoryPageVC
//        vc.cartdelegate = self
        vc.checktype = 2
        vc.brandid = cellbrandtag// self.searchsource?.products?[cellbrandtag].brand_id ?? 0
        vc.brandsource = brandsource
        print(vc.checktype,"Sdfewfwe", vc.brandid)
        /// Solving URL String by removing "\n"
        let url = pbrandimg
        vc.imageURL = url
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func getBrands(){
            let headers : HTTPHeaders  = [
                "cache-control": "no-cache"
            ]
            
            
            SharedHandler.startSpinner(spinner, v, self.view)
            APIManager.sharedInstance.getRequest(URLs.brands, header: headers) { (res) in
                SharedHandler.stopSpinner(self.spinner, v, self.view)
                let decoder = JSONDecoder()
                do{
                    if let data = res.data{
                        let model = try decoder.decode(Brands_Base.self, from: data)
                        self.brandsource = model
                        self.getData()
                        
                    }else{
                        self.getBrands()
                    }
                }catch{
                    
                }
            }
        }
    
    @objc func addcounter(){
        self.badgeView.isHidden = false
        self.badgeView.setTitle(String(describing: cartcounterglb! + 1), for: .normal)
    }
    
}
