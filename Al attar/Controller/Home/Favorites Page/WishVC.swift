//
//  WishVC.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 7/25/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class WishVC: UIViewController {

    @IBOutlet weak var pageTitle: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var startBtn: UIButtonX!
    @IBOutlet weak var collection: UICollectionView!
    @IBOutlet weak var imagePAge: UIImageView!
    @IBOutlet weak var badgeView: UIButtonX!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    var datasource: _Wishes?
    var brandsource: Brands_Base?
    var CartArr = MyCartData.getCartData().0
    var cartarr = [[String:Any]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.badgeView.isHidden = true
        if let arr = def.array(forKey: "cartarr") as? [[String:Any]]{
         cartarr = arr
        }
        setDefault()
        getBrands()
        self.startBtn.addTarget(self, action: #selector(openWishlist), for: .touchUpInside)
    }
    @IBAction func openCart(_ sender: Any) {
        let nextStoryboard: UIStoryboard = UIStoryboard(name: "Order", bundle: nil)
        let vc = nextStoryboard.instantiateViewController(withIdentifier: "CartVC") as! CartVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func openWishlist(){
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
        def.set(100, forKey: "menuadd")
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.navigationItem.title = LanguageHelper.getString(forKey: "wishes")
    }
 

    func setDefault(){
        self.setSemantic(view: self.view, languageKey: LanguageHelper.getCurrentLanguage())
        SharedHandler.sideMenus(ViewController: self)
        self.collection.isHidden = true
        pageTitle.setText(forKey: "wishlist")
        descLbl.setText(forKey: "wishdesc")
        startBtn.setText(forKey: "startshopping")
        self.navigationItem.title = LanguageHelper.getString(forKey: "wishes")
        if MyCartData.getCartData().0.count != 0 {
            self.badgeView.isHidden = false
            self.badgeView.setTitle(String(describing: MyCartData.getCartData().0.count), for: .normal)
        }
    }
}
