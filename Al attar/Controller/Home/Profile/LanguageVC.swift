//
//  LanguageVC.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 8/12/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import UIKit

class LanguageVC: UIViewController {

@IBOutlet weak var pageTitle: UILabel!
@IBOutlet weak var arImg: UIImageView!
@IBOutlet weak var enImg: UIImageView!
@IBOutlet weak var updateBtn: UIButtonX!

var langu = LanguageHelper.getCurrentLanguage()

override func viewDidLoad() {
    super.viewDidLoad()
    setDefault()
    self.updateBtn.addTarget(self, action: #selector(changeLang), for: .touchUpInside)
    self.updateBtn.alpha = 0.5
    self.updateBtn.isUserInteractionEnabled = false
}

@IBAction func closeBtn(_ sender: Any) {
    self.dismiss(animated: true, completion: nil)
}

@IBAction func arBtn(_ sender: Any) {
    arImg.image = UIImage(named: "check")
    enImg.image = UIImage(named: "checkmark-circle")
    self.updateBtn.alpha = 1
    self.updateBtn.isUserInteractionEnabled = true
    langu = "ar"
}
@IBAction func enBtn(_ sender: Any) {
    enImg.image = UIImage(named: "check")
    arImg.image = UIImage(named: "checkmark-circle")
    self.updateBtn.alpha = 1
    self.updateBtn.isUserInteractionEnabled = true
    langu = "en"
}

@objc func changeLang(){
    LanguageHelper.setCurrentLanguage(langugaeCode: langu)
    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
    let viewController = mainStoryboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
    UIApplication.shared.keyWindow?.rootViewController = viewController
}

func setDefault(){
    self.setSemantic(view: self.view, languageKey: LanguageHelper.getCurrentLanguage())
    if langu == "ar"{
        arImg.image = UIImage(named: "check")
        enImg.image = UIImage(named: "checkmark-circle")
    }else{
        arImg.image = UIImage(named: "checkmark-circle")
        enImg.image = UIImage(named: "check")
    }
    self.updateBtn.setText(forKey: "updatelang")
    self.pageTitle.setText(forKey: "language")
}

}
