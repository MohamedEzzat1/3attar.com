//
//  MyProfileVC.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 7/24/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import UIKit

class MyProfileVC: UIViewController {

    @IBOutlet weak var bar: UIButton!
    @IBOutlet weak var pagetitle: UILabel!
    @IBOutlet weak var changeBtn: UIButtonX!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var nameTF: UITextFieldX!
    @IBOutlet weak var emailTF: UITextFieldX!
    @IBOutlet weak var passTF: UITextFieldX!
    @IBOutlet weak var phoneTF: UITextFieldX!
    @IBOutlet weak var saveBtn: UIButtonX!
    override func viewDidLoad() {
        super.viewDidLoad()
        setDefault()
        
    }
    
    @IBAction func submitBtn(_ sender: Any) {
        submitData()
    }
    
    func setDefault(){
        self.setSemantic(view: self.view, languageKey: LanguageHelper.getCurrentLanguage())
//        SharedHandler.sideMenus(ViewController: self)
        self.navigationItem.title = LanguageHelper.getString(forKey: "profileinfo")
//        self.nameTF.setAttributedPlaceholder(forKey: "name", withColor: UIColor(red: 87/255, green: 95/255, blue: 98/255, alpha: 1))
//        self.phoneTF.setAttributedPlaceholder(forKey: "phone", withColor: UIColor(red: 87/255, green: 95/255, blue: 98/255, alpha: 1))
        self.passTF.setAttributedPlaceholder(forKey: "pass", withColor: UIColor(red: 87/255, green: 95/255, blue: 98/255, alpha: 1))
//        self.emailTF.setAttributedPlaceholder(forKey: "email", withColor: UIColor(red: 87/255, green: 95/255, blue: 98/255, alpha: 1))
        
        self.nameTF.text  = def.string(forKey: "logName")
        self.emailTF.text = def.string(forKey: "logEmail")
        self.phoneTF.text = def.string(forKey: "logPhone")
        self.nameLbl.text = def.string(forKey: "logName")
        self.emailLbl.text = def.string(forKey: "logEmail")
        
        saveBtn.setText(forKey: "saveinfo")
    }
    
}
