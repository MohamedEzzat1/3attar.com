//
//  MyProfileEx.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 7/24/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import UIKit
import Alamofire

extension MyProfileVC {
    
    func submitData(){
        if let token = def.string(forKey: "logToken"){
            let headers : HTTPHeaders = [
            "Authorization": "bearer \(token)"]
            
            let body = [
                "name": self.nameTF.text!,
                "email": self.emailTF.text!,
                "mobile": self.phoneTF.text!,
                "user_image": "",
                "password": self.passTF.text!
            ]
            
            APIManager.sharedInstance.postRequest(URLs.submitprofile, header: headers, Parameters: body) { (res) in
                print(res.error?.localizedDescription, " reeewewre")
            }
        }
        
//
    }
    
}
