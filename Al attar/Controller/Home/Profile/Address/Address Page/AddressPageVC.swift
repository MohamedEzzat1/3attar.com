//
//  AddressPageVC.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 8/22/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class AddressPageVC: UIViewController {

    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var address1Lbl: UITextField!
    @IBOutlet weak var address2Lbl: UITextField!
    @IBOutlet weak var infoLbl: UILabel!
    @IBOutlet weak var cityTF: UITextFieldX!
    @IBOutlet weak var districtTF: UITextField!
    @IBOutlet weak var addNameLbl: UILabel!
    @IBOutlet weak var optionLbl: UILabel!
    @IBOutlet weak var homeBtn: UIButtonX!
    @IBOutlet weak var workBtn: UIButtonX!
    @IBOutlet weak var saveBtn: UIButtonX!
    
    let pickerView = ToolbarPickerView()
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    var locCount = 0
    var locationsource: _get_address?
    var cityzones: _Shipping_Base?
    var addressid = 0
    var nameaddress = 1
    let token = def.string(forKey: "logToken")
    var locId = 0
    var subLocId = 0
    var rowcount = Int()
    var navID = 0
    var editvar = false
    var addressdelegate: updateAddress?
    var addresssource: address_Base?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setDefault()
        getData()
        getCity()
        self.homeBtn.addTarget(self, action: #selector(checkHome), for: .touchUpInside)
        self.workBtn.addTarget(self, action: #selector(checkWork), for: .touchUpInside)
        self.cityTF.addTarget(self, action: #selector(cityBtn), for: .allTouchEvents)
        self.districtTF.addTarget(self, action: #selector(districtbtn), for: .allTouchEvents)
    }
    
    @objc func cityBtn() {
        locCount = 1
        print(locCount,"SSqq")
        self.cityTF.inputView = self.pickerView
        self.cityTF.inputAccessoryView = self.pickerView.toolbar
        self.pickerView.reloadAllComponents()
        self.pickerView.isHidden = false
    }
    @objc func districtbtn() {
        if rowcount != nil{
            locCount = 2
            self.districtTF.inputView = self.pickerView
            self.districtTF.inputAccessoryView = self.pickerView.toolbar
            self.pickerView.reloadAllComponents()
            self.pickerView.isHidden = false
        }else{
            SharedHandler.alert(message: LanguageHelper.getString(forKey: "selectcityfirst"))
        }
    }
    
    func setDefault(){
        setSemantic(view: self.view, languageKey: LanguageHelper.getCurrentLanguage())
        self.navigationItem.title = LanguageHelper.getString(forKey: "myaddress")
        
        
        
        cityTF.delegate = self
        districtTF.delegate = self
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        self.pickerView.toolbarDelegate = self
        self.pickerView.isHidden = false
        self.cityTF.inputView = self.pickerView
        self.cityTF.inputAccessoryView = self.pickerView.toolbar
        if navID == 1{
            if editvar != false{
                self.saveBtn.setText(forKey: "editaddress")
                self.saveBtn.addTarget(self, action: #selector(addAddressfunc), for: .touchUpInside)
            }else{
                self.saveBtn.setText(forKey: "saveaddress")
                self.saveBtn.addTarget(self, action: #selector(addAddressfunc), for: .touchUpInside)
            }
        }else{
            self.saveBtn.setText(forKey: "confirmaddress")
            self.saveBtn.addTarget(self, action: #selector(addAddressfunc), for: .touchUpInside)
            
        }
//        self.districtTF.inputView = self.pickerView
//        self.districtTF.inputAccessoryView = self.pickerView.toolbar

        self.addressLbl.setText(forKey: "myaddress")
        self.infoLbl.setText(forKey: "otherinfo")
        
        self.homeBtn.setText(forKey: "home")
        self.workBtn.setText(forKey: "work")
        self.addNameLbl.setText(forKey: "addnaming")
        self.optionLbl.setText(forKey: "optional")
        self.address1Lbl.setAttributedPlaceholder(forKey: "address1", withColor: UIColor(red: 87/255, green: 95/255, blue: 98/255, alpha: 1))
        self.address2Lbl.setAttributedPlaceholder(forKey: "address2", withColor: UIColor(red: 87/255, green: 95/255, blue: 98/255, alpha: 1))
        self.cityTF.setAttributedPlaceholder(forKey: "city", withColor: UIColor(red: 87/255, green: 95/255, blue: 98/255, alpha: 1))
        self.districtTF.setAttributedPlaceholder(forKey: "district", withColor: UIColor(red: 87/255, green: 95/255, blue: 98/255, alpha: 1))
    }

}
