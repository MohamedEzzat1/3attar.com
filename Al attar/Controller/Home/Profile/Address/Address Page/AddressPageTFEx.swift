//
//  AddressPageTFEx.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 8/22/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation

@available(iOS 13.0, *)
extension AddressPageVC: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == cityTF{
            self.cityTF.inputView = self.pickerView
            self.cityTF.inputAccessoryView = self.pickerView.toolbar
        }else if textField == districtTF{
            self.districtTF.inputView = self.pickerView
            self.districtTF.inputAccessoryView = self.pickerView.toolbar
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == cityTF{
            locCount = 1
            self.cityTF.inputView = self.pickerView
            self.cityTF.inputAccessoryView = self.pickerView.toolbar
        }else if textField == districtTF{
            if rowcount != nil{
                locCount = 2
                self.districtTF.inputView = self.pickerView
                self.districtTF.inputAccessoryView = self.pickerView.toolbar
                
                
            }else{
                SharedHandler.alert(message: LanguageHelper.getString(forKey: "selectcityfirst"))
            }
            
        }
    }
}
