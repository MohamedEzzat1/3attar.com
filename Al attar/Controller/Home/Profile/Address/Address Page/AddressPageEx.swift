//
//  AddressPageEx.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 8/22/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation
import Alamofire

@available(iOS 13.0, *)
extension AddressPageVC{
    
    func getData(){
        SharedHandler.startSpinner(spinner, v, self.view)
        
        let header: HTTPHeaders = [
            "Authorization": "bearer \(token ?? "")"
        ]
        print(addressid)
        APIManager.sharedInstance.getRequest(URLs.get_user_address + "?address_id=\(addressid)", header: header) { (res) in
            SharedHandler.stopSpinner(self.spinner, v, self.view)
            let decoder = JSONDecoder()
            if let data = res.data{
                do{
                    let model = try decoder.decode(_get_address.self, from: data)
                    self.locationsource = model
                    self.address1Lbl.text = self.locationsource?.user_address?.address1 ?? ""
                    self.address2Lbl.text = self.locationsource?.user_address?.address2 ?? ""
                    
                }catch{
                    
                }
                
            }else{
                
            }
        }
    }
    
    func getCity(){
        SharedHandler.startSpinner(spinner, v, self.view)
        let header: HTTPHeaders = [
            "Authorization": "bearer \(token ?? "")",
            "cache-control": "no-cache"
        ]
        
        APIManager.sharedInstance.getRequest(URLs.Shippingzones, header: header) { (res) in
            SharedHandler.stopSpinner(self.spinner, v, self.view)
            let decoder = JSONDecoder()
            if let data = res.data{
                do{
                    let model = try decoder.decode(_Shipping_Base.self, from: data)
                    self.cityzones = model
                    print(model, "aasfafW")
//                    self.pickerView.reloadInputViews()
                }catch{
                    print(res.error?.localizedDescription, res.value, res.response?.statusCode)
                }
                
            }else{
                
            }
        }
        
    }
    
    func updateAddress(){
        SharedHandler.startSpinner(spinner, v, self.view)
        let header: HTTPHeaders = [
            "Authorization": "bearer \(token ?? "")"
        ]
        
        let body = [
            "title": address1Lbl.text!,
            "address1": address1Lbl.text!,
            "address2": address2Lbl.text!,
            "city": cityTF.text!,
            "state": subLocId,
            "zip": 1111,
            "primary": nameaddress,
            "address_id": addressid
            ] as [String : Any]
        
        APIManager.sharedInstance.postRequest(URLs.update_address, header: header, Parameters: body) { (res) in
            SharedHandler.stopSpinner(self.spinner, v, self.view)
            let decoder = JSONDecoder()
            if let data = res.data{
                do{
                    let model = try decoder.decode(address_Base.self, from: data)
                    if model.success == true{
                        self.navigationController?.popViewController(animated: true)
                        SharedHandler.DisAppearMessage(viewController: self, message: model.message ?? "")
                        print(self.navID, "hjff")
                        if self.navID == 1{
                            print("ioyuyiyuiyi")
                            self.addressdelegate?.setaddress(check: "1")
                            self.navigationController?.popViewController(animated: true)
                        }else{
                            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Order", bundle: nil)
                            let vc = mainStoryboard.instantiateViewController(withIdentifier: "ConfirmationVC") as! ConfirmationVC
                            vc.shippingcost  = self.locationsource?.user_address?.shipping_cost?.cost ?? ""
                            vc.addresstitle1 = self.locationsource?.user_address?.address1 ?? ""
                            vc.addresstitle2 = self.locationsource?.user_address?.address2 ?? ""
                            vc.addressid = self.locationsource?.user_address?.id ?? 0
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }else{
                        
                    }
                }catch{
                    
                }
                
            }else{
                
            }
        }
    }
    
    func sendAddress(){
        SharedHandler.startSpinner(spinner, v, self.view)
        let header: HTTPHeaders = [
            "Authorization": "bearer \(token ?? "")"
        ]
        
        let body = [
            "title": address1Lbl.text!,
            "address1": address1Lbl.text!,
            "address2": address2Lbl.text!,
            "city": cityTF.text!,
            "state": subLocId,
            "zip": 34234,
            "primary": nameaddress,
            ] as [String : Any]
        
        APIManager.sharedInstance.postRequest(URLs.create_address, header: header, Parameters: body) { (res) in
            SharedHandler.stopSpinner(self.spinner, v, self.view)
            let decoder = JSONDecoder()
            if let data = res.data{
                do{
                    let model = try decoder.decode(address_Base.self, from: data)
                    if model.success == true{
//                        SharedHandler.DisAppearMessage(viewController: self, message: model.message ?? "")
                        if self.navID == 1{
                            self.addressdelegate?.setaddress(check: "1")
                            self.navigationController?.popViewController(animated: true)
                        }else{
                            
                            self.getAddress()
                        }
                    }else{
                        
                    }
                }catch{
                    
                }
                
            }else{
                
            }
        }
        
    }
    
    @objc func addAddressfunc(){
        if subLocId != 0{
            guard let address1 = address1Lbl.text, !address1.isEmpty else {
                print("?")
                return
            }
            guard let address2 = address2Lbl.text, !address1.isEmpty else {
                print("?")
                return
            }
            guard let city = cityTF.text, !address1.isEmpty else {
                print("?")
                return
            }
            guard let state = districtTF.text, !address1.isEmpty else {
                print("?")
                return
            }
            self.cityTF.filled()
            if navID == 1{
                if self.editvar != false{
                    print("iutirtfj")
                    updateAddress()
                }else{
                    sendAddress()
                }
            }else{
//                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Order", bundle: nil)
//                let vc = mainStoryboard.instantiateViewController(withIdentifier: "ConfirmationVC") as! ConfirmationVC
//                vc.shippingcost  = self.locationsource?.user_address?.shipping_cost?.cost ?? ""
//                vc.addresstitle1 = self.locationsource?.user_address?.address1 ?? ""
//                vc.addresstitle2 = self.locationsource?.user_address?.address2 ?? ""
//                vc.addressid     = self.locationsource?.user_address?.id ?? 0
//                self.navigationController?.pushViewController(vc, animated: true)
                sendAddress()
            }
        }else{
            if locId == 0{
                self.cityTF.emptyField(msg: "")
                self.districtTF.emptyField(msg: "")
            }else{
                self.cityTF.filled()
                
                self.districtTF.emptyField(msg: "")
            }
        }
    }
    
    @objc func checkHome(){
        nameaddress = 1
        homeBtn.borderColor = UIColor(red: 127/255, green: 216/255, blue: 190/255, alpha: 1)
        homeBtn.setTitleColor(UIColor(red: 31/255, green: 47/255, blue: 52/255, alpha: 1), for: .normal)
        workBtn.borderColor = UIColor(red: 229/255, green: 229/255, blue: 229/255, alpha: 1)
        workBtn.setTitleColor(UIColor(red: 229/255, green: 229/255, blue: 229/255, alpha: 1), for: .normal)
    }
    
    @objc func openConfirmation(){
        
    }
    
    @objc func checkWork(){
        nameaddress = 2
        workBtn.borderColor = UIColor(red: 127/255, green: 216/255, blue: 190/255, alpha: 1)
        workBtn.setTitleColor(UIColor(red: 31/255, green: 47/255, blue: 52/255, alpha: 1), for: .normal)
        homeBtn.borderColor = UIColor(red: 229/255, green: 229/255, blue: 229/255, alpha: 1)
        homeBtn.setTitleColor(UIColor(red: 229/255, green: 229/255, blue: 229/255, alpha: 1), for: .normal)
    }
    
    func getAddress(){
        if let token = def.string(forKey: "logToken"){
            let headers : HTTPHeaders = [
                "Authorization": "bearer \(token)"]
                   print(token)
                   APIManager.sharedInstance.getRequest(URLs.user_address , header: headers) { (res) in
                       let decoder = JSONDecoder()
                       if let data = res.data{
                           do{
                               let model = try decoder.decode(address_Base.self, from: data)
                            self.addresssource = model
                            print(self.addresssource?.user_address?.last?.id, "hjgjjjjy")
                               self.addresssource = model
                              let mainStoryboard: UIStoryboard = UIStoryboard(name: "Order", bundle: nil)
                              let vc = mainStoryboard.instantiateViewController(withIdentifier: "ConfirmationVC") as! ConfirmationVC
                            vc.shippingcost  = self.addresssource?.user_address?.last?.shipping_cost?.cost ?? ""
                            vc.addresstitle1 = self.addresssource?.user_address?.last?.address1 ?? ""
                              vc.addresstitle2 = self.addresssource?.user_address?.last?.address2 ?? ""
                            vc.addressid = self.addresssource?.user_address?.last?.id ?? 0
                              self.navigationController?.pushViewController(vc, animated: true)
                           }catch{
                           }
                           
                       }else{
                       }
                   }
        }   
    }
}
