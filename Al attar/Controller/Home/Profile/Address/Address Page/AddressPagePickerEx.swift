//
//  AddressPagePickerEx.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 8/22/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation

@available(iOS 13.0, *)
extension AddressPageVC: UIPickerViewDelegate, UIPickerViewDataSource, ToolbarPickerViewDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if locCount == 1{
            return cityzones?.shipping_zones?.count ?? 0
        }else{
            return cityzones?.shipping_zones?[self.rowcount].areas?.count ?? 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if locCount == 1{
            return cityzones?.shipping_zones?[row].title ?? ""
        }else{
            return cityzones?.shipping_zones?[self.rowcount].areas?[row].title ?? ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if locCount == 1{
            self.cityTF.text = cityzones?.shipping_zones?[row].title ?? ""
            self.locId = cityzones?.shipping_zones?[row].id ?? 0
            self.rowcount = row
        }else{
            self.districtTF.text = cityzones?.shipping_zones?[self.rowcount].areas?[row].title ?? ""
            self.subLocId = cityzones?.shipping_zones?[self.rowcount].areas?[row].id ?? 0
        }
    }
    
    func didTapDone() {
        let row = self.pickerView.selectedRow(inComponent: 0)
        self.pickerView.selectRow(row, inComponent: 0, animated: false)
        if locCount == 1{
            self.cityTF.text = cityzones?.shipping_zones?[row].title ?? ""
            self.locId = cityzones?.shipping_zones?[row].id ?? 0
            self.cityTF.resignFirstResponder()
        }else{
            self.districtTF.text = cityzones?.shipping_zones?[rowcount].areas?[row].title ?? ""
            self.subLocId = cityzones?.shipping_zones?[rowcount].areas?[row].id ?? 0
            self.districtTF.resignFirstResponder()
        }
    }
    
    func didTapCancel() {
        if locCount == 1{
            self.cityTF.text = nil
            self.cityTF.resignFirstResponder()
        }else{
            self.districtTF.text = nil
            self.districtTF.resignFirstResponder()
        }
    }
    
}
