//
//  AddressListVC.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 8/21/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import UIKit

protocol updateAddress {
    func setaddress(check:String)
}
@available(iOS 13.0, *)
class AddressListVC: UIViewController, updateAddress {
    
    

    @IBOutlet weak var addimg: UIImageView!
    @IBOutlet weak var titleimg: UILabel!
    @IBOutlet weak var addBtn: UIButtonX!
    @IBOutlet weak var locBtn: UIButton!
    @IBOutlet weak var askLbl: UILabel!
    @IBOutlet weak var pageTitle: UILabel!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var addView: UIViewX!
    @IBOutlet weak var addnewBtn: UIButton!
    
    let btn2 = UIButton(type: .custom)
    
    var addresssource : address_Base?
    var navID = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        self.table.delegate = self
        self.table.dataSource = self
        setDefault()
        getAddress()
        self.addBtn.addTarget(self, action: #selector(addAddress), for: .touchUpInside)
        self.addnewBtn.addTarget(self, action: #selector(openaddressnew), for: .touchUpInside)
    }
    
    @objc func addAddress(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddressPageVC") as! AddressPageVC
        vc.navID = self.navID
        vc.addressdelegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func setDefault(){
        self.setSemantic(view: self.view, languageKey: LanguageHelper.getCurrentLanguage())
        self.navigationItem.title = LanguageHelper.getString(forKey: "myaddress")
        
        btn2.setTitle(LanguageHelper.getString(forKey: "cancel"), for: .normal)
        btn2.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn2.setTitleColor(UIColor.black, for: .normal)
        btn2.tintColor = UIColor.black
        btn2.addTarget(self, action: #selector(backtoviewcontroller), for: .touchUpInside)
        let item2 = UIBarButtonItem(customView: btn2)
        self.navigationItem.setLeftBarButtonItems([item2], animated: true)
        addView.addDashedBorder(view: self.view)
        switch navID {
        case 1:
            addBtn.setText(forKey: "addaddress")
        case 2:
            addBtn.setText(forKey: "confirmaddress")
        default:
            return
        }
        pageTitle.setText(forKey: "myaddress")
        
        askLbl.setText(forKey: "wherelocation")
        titleimg.setText(forKey: "myaddress")
        addnewBtn.setText(forKey: "addnewaddress")
        self.table.isHidden = true
        self.addBtn.isHidden = false
        self.addimg.isHidden = false
        self.askLbl.isHidden = false
        self.titleimg.isHidden = false
        self.addnewBtn.isHidden = true
        self.addView.isHidden = true
    }
    
}
