//
//  AddressListTableEx.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 8/21/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation

@available(iOS 13.0, *)
extension AddressListVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return (self.addresssource?.user_address!.count ?? 0)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddressCell1", for: indexPath) as! AddressCell
        cell.addresslbl.text = self.addresssource?.user_address?[indexPath.row].address1 ?? ""
        cell.priceLbl.text   = self.addresssource?.user_address?[indexPath.row].shipping_cost?.cost ?? ""
        cell.editBtn.setText(forKey: "editaddress")
        cell.editBtn.addTarget(self, action: #selector(openaddressnew), for: .touchUpInside)
            
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.navID == 1{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddressPageVC") as! AddressPageVC
            vc.addressid = self.addresssource?.user_address?[indexPath.row].id ?? 0
            vc.navID = self.navID
            vc.editvar = true
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Order", bundle: nil)
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "ConfirmationVC") as! ConfirmationVC
            vc.shippingcost  = self.addresssource?.user_address?[indexPath.row].shipping_cost?.cost ?? ""
            print(self.addresssource?.user_address?[indexPath.row].shipping_cost?.cost ?? "", "hiouhouhou")
            vc.addresstitle1 = self.addresssource?.user_address?[indexPath.row].address1 ?? ""
            vc.addresstitle2 = self.addresssource?.user_address?[indexPath.row].address2 ?? ""
            vc.addressid = self.addresssource?.user_address?[indexPath.row].id ?? 0
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 2{
            return 110
        }else{
            return 126
        }
    }
    
    
}
