//
//  AddressListEx.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 8/21/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation
import Alamofire

@available(iOS 13.0, *)
extension AddressListVC{
    
    func getAddress(){
        if let token = def.string(forKey: "logToken"){
            let headers : HTTPHeaders = [
                "Authorization": "bearer \(token)"]
                   print(token)
                   APIManager.sharedInstance.getRequest(URLs.user_address , header: headers) { (res) in
                       let decoder = JSONDecoder()
                       if let data = res.data{
                           do{
                               let model = try decoder.decode(address_Base.self, from: data)
                            self.addresssource = model
                            print(self.addresssource?.user_address, "hjgy")
                               self.addresssource = model
                               if (self.addresssource?.user_address?.count)! != 0{
                                self.table.isHidden = false
                                self.addBtn.isHidden = true
                                self.addimg.isHidden = true
                                self.askLbl.isHidden = true
                                self.titleimg.isHidden = true
                                self.addnewBtn.isHidden = false
                                self.addView.isHidden = false
                                self.table.delegate = self
                                self.table.dataSource = self
                                self.table.reloadData()
                               }
                           }catch{
                           }
                           
                       }else{
                       }
                   }
        }
       
        
    }
    
    func setaddress(check: String) {
        getAddress()
    }
    
    @objc func backtoviewcontroller(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func openaddressnew(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddressPageVC") as! AddressPageVC
        vc.navID = self.navID
        vc.editvar = false
//        if self.addresssource?.user_address?.count != 0{
//            vc.addressid = self.addresssource?.user_address?[0].id ?? 0
//        }
        vc.addressdelegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
