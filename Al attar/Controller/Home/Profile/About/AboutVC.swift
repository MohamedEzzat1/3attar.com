//
//  AboutVC.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 7/25/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import UIKit

class AboutVC: UIViewController {

    @IBOutlet weak var aboutTV: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setDefault()
    }
    
    func setDefault(){
        self.navigationItem.title = LanguageHelper.getString(forKey: "aboutattar")
    }

}
