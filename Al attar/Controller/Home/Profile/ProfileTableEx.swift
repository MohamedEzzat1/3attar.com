//
//  ProfileTableEx.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 7/24/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
extension ProfileVC: UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if def.bool(forKey: _LoggedIn) == true{
            return self.loggedArrImgs.count
        }else{
            return 3
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch def.bool(forKey: _LoggedIn) {
        case true:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell", for: indexPath) as! ProfileCell
            cell.img.image = self.loggedArrImgs[indexPath.row]
            if loggedArr[indexPath.row] == "mycredit"{
                cell.soon.setText(forKey: "soon")
            }
            cell.infoLbl.setText(forKey: self.loggedArr[indexPath.row])
            return cell
            
        case false:
            if indexPath.row == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "LoginCell", for: indexPath) as! LoginCell
                cell.loginBtn.addTarget(self, action: #selector(openLogin), for: .touchUpInside)
                cell.joinBtn.addTarget(self, action: #selector(openSignup), for: .touchUpInside)
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell", for: indexPath) as! ProfileCell
                cell.img.image = self.notLoggedArrImgs[indexPath.row - 1]
                cell.infoLbl.setText(forKey: self.notLoggedArr[indexPath.row - 1])
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if def.bool(forKey: _LoggedIn) == true{
            return 75
        }else{
            if indexPath.row == 0{
                return 130
            }else{
                return 75
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if def.bool(forKey: _LoggedIn) == true{
            switch indexPath.row {
            case 0:
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyProfileVC") as! MyProfileVC
//                vc.parentId = datasource?[indexPath.row].id ?? 0
                self.navigationController?.pushViewController(vc, animated: true)
                print("Profile Info")
            case 1:
                self.performSegue(withIdentifier: "openlang", sender: self)
            case 2:
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "AboutVC") as! AboutVC
                //                vc.parentId = datasource?[indexPath.row].id ?? 0
                self.navigationController?.pushViewController(vc, animated: true)
            case 3:
                print("Orders")
                let vc = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "tab")  as! UITabBarController
                vc.selectedIndex = 2
                UIApplication.shared.keyWindow?.rootViewController = vc
            case 4:
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddressListVC") as! AddressListVC
                vc.navID = 1
                self.navigationController?.pushViewController(vc, animated: true)
            case 5:
                
                
                self.getPaymentKey()
                print("Credit Cards")
            case 6:
                def.set(false, forKey: _LoggedIn)
                def.set("", forKey: "logToken")
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
                UIApplication.shared.keyWindow?.rootViewController = viewController
                print("Logout")
            default:
                print("0")
            }
        }else{
           switch indexPath.row {
            case 0:
                print("Profile Info")
            case 1:
                self.performSegue(withIdentifier: "openlang", sender: self)
            case 2:
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "AboutVC") as! AboutVC
                //                vc.parentId = datasource?[indexPath.row].id ?? 0
                self.navigationController?.pushViewController(vc, animated: true)
            default:
                print("0")
            }
        }
    }
    
    
}
