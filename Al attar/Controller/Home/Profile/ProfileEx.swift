//
//  ProfileEx.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 7/24/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import UIKit
import SwiftyJSON

@available(iOS 13.0, *)
extension ProfileVC{
    
    func getdata(){
        tbl.delegate = self
        tbl.dataSource = self
        tbl.reloadData()
    }
    
    @objc func openLogin(){
//        self.presentVC(storyboard: "Main", viewController: "LoginVC")
        self.pushVC(storyboard: "Main", viewController: "LoginVC")
    }
    
    @objc func openSignup(){
        self.pushVC(storyboard: "Main", viewController: "SignupVC")
    }
    
    func getPaymentKey(){
        
        let data = [
            "api_key": "ZXlKMGVYQWlPaUpLVjFRaUxDSmhiR2NpT2lKSVV6VXhNaUo5LmV5SndjbTltYVd4bFgzQnJJam95TkRRNU15d2libUZ0WlNJNkltbHVhWFJwWVd3aUxDSmpiR0Z6Y3lJNklrMWxjbU5vWVc1MEluMC5PeXItRlNjb3hBYlVKeW0ybDFJbXByZEVxenY1ZWxCVEdpYUZoblFCSDVsU2ZkUDl4TTFkeGFQdzNwV0pUWWZXZm90WFdyY1lfNzV1dzhXODFiS3FlQQ=="
        ]
        APIManager.sharedInstance.postRequest("https://accept.paymobsolutions.com/api/auth/tokens", Parameters: data) { (res) in
            
            let json = JSON(res.value)
            if let token = json["token"].string{
                print(token, "first")
                let data = [
                  "id": 103,
                  "created_at": "2017-01-10T05:41:15.700814Z",
                  "delivery_needed": "false",
                  "merchant": [
                      "id": 28,
                      "created_at": "2016-11-17T15:02:53.646620Z",
                      "phones": [
                          "011111111111",
                          "012324151432"
                      ],
                      "company_emails": [
                          "brendon42@cummings-windler.biz",
                          "jim50@sipes-kunze.com"
                      ],
                      "company_name": "Wuckert, Zieme and Dach",
                      "state": "Oklahoma",
                      "country": "Oman",
                      "city": "Port Arvillachester",
                      "postal_code": "83372",
                      "street": "Walker Ramp"
                  ],
                  "collector": "null",
                  "amount_cents": 100,
                  "shipping_data": [
                      "id": 80,
                      "first_name": "test",
                      "last_name": "account",
                      "street": "example",
                      "building": "6",
                      "floor": "4",
                      "apartment": "404",
                      "city": "cairo",
                      "state": "egypt",
                      "country": "egypt",
                      "email": "test@example.com",
                      "phone_number": "00201000212058",
                      "postal_code": "123456",
                      "extra_description": "test asdf",
                      "shipping_method": "EM",
                      "order_id": 103,
                      "order": 103
                  ],
                  "currency": "EGP",
                  "is_payment_locked": "false",
                  "merchant_order_id": "null",
                  "wallet_notification": "null",
                  "paid_amount_cents": 0,
                  "items": []
                ] as? [String:Any]
                APIManager.sharedInstance.postRequest("https://accept.paymobsolutions.com/api/ecommerce/orders", Parameters: data) { (res) in
                    print(res.value, "second")
                    let data1 = [
                      "auth_token": token,
                      "amount_cents": "100",
                      "expiration": 3600,
                      "order_id": "103",
                      "billing_data": [
                        "apartment": "803",
                        "email": "claudette09@exa.com",
                        "floor": "42",
                        "first_name": "Clifford",
                        "street": "Ethan Land",
                        "building": "8028",
                        "phone_number": "+86(8)9135210487",
                        "shipping_method": "PKG",
                        "postal_code": "01898",
                        "city": "Jaskolskiburgh",
                        "country": "CR",
                        "last_name": "Nicolas",
                        "state": "Utah"
                      ],
                      "currency": "EGP",
                      "integration_id": 1,
                      "lock_order_when_paid": "false"
                    ] as? [String:Any]
                    APIManager.sharedInstance.postRequest("https://accept.paymobsolutions.com/api/acceptance/payment_keys", Parameters: data) { (res) in
                        print(res.value, "third")

                    }

                }
            }
        }

    }
    
}
