//
//  ProfileVC.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 7/24/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import UIKit
//import AcceptSDK

@available(iOS 13.0, *) 
class ProfileVC: UIViewController {
//    func userDidCancel() {
//        print("canceled")
//    }
//
//    func paymentAttemptFailed(_ error: AcceptSDKError, detailedDescription: String) {
//        print("Failed")
//    }
//
//    func transactionRejected(_ payData: PayResponse) {
//            print("rejected")
//    }
//
//    func transactionAccepted(_ payData: PayResponse) {
//        print("accepted")
//    }
//
//    func transactionAccepted(_ payData: PayResponse, savedCardData: SaveCardResponse) {
//        print("accepted savecard")
//    }
//
//    func userDidCancel3dSecurePayment(_ pendingPayData: PayResponse) {
//        print("canceled3")
//    }
    

    @IBOutlet weak var pageTitle: UILabel!
    @IBOutlet weak var welcomeLbl: UILabel!
    @IBOutlet weak var loginLbl: UILabel!
    @IBOutlet weak var tbl: UITableView!
    
    var notLoggedArrImgs = [UIImage(named: "language"), UIImage(named: "info")]
    var notLoggedArr     = ["language", "info"]
    var loggedArrImgs    = [UIImage(named: "account"), UIImage(named: "language"), UIImage(named: "info"), UIImage(named: "order"), UIImage(named: "location"), UIImage(named: "credit_card"), UIImage(named: "logout")]
    var loggedArr        = ["profileinfo", "language", "info", "myorders", "myaddress", "mycredit", "logout"]
    
//    let accept = AcceptSDK()
    
    // place your payment key here
    let KEY: String = "ZXlKMGVYQWlPaUpLVjFRaUxDSmhiR2NpT2lKSVV6VXhNaUo5LmV5SndjbTltYVd4bFgzQnJJam95TkRRNU15d2libUZ0WlNJNkltbHVhWFJwWVd3aUxDSmpiR0Z6Y3lJNklrMWxjbU5vWVc1MEluMC5PeXItRlNjb3hBYlVKeW0ybDFJbXByZEVxenY1ZWxCVEdpYUZoblFCSDVsU2ZkUDl4TTFkeGFQdzNwV0pUWWZXZm90WFdyY1lfNzV1dzhXODFiS3FlQQ=="

    // Place your billing data here
    // billing data can not be empty
    // if empty, type in NA instead
    let bData = [  "apartment": "1",
                   "email": "ios.ezzat@gmail.com",
                   "floor": "2",
                   "first_name": "mohamed",
                   "street": "zaki",
                   "building": "2",
                   "phone_number": "01112983774",
                   "shipping_method": "e",
                   "postal_code": "3223",
                   "city": "cairo",
                   "country": "egypt",
                   "last_name": "mohamed",
                   "state": "w"
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        accept.delegate = self
        setDefault()
        getdata()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.navigationItem.title = LanguageHelper.getString(forKey: "profile")
    }
    
    func setDefault() {
        self.setSemantic(view: self.view, languageKey: LanguageHelper.getCurrentLanguage())
        self.navigationItem.title = LanguageHelper.getString(forKey: "profile")
//        self.pageTitle.setText(forKey: "profile")
        self.welcomeLbl.setText(forKey: "welcometoattar")
        self.loginLbl.setText(forKey: "logintopurchase")
        SharedHandler.sideMenus(ViewController: self)
    }

}
