//
//  BasketTableEx.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 8/22/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation

@available(iOS 13.0, *)
extension BasketVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.datasource?.orders?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BasketCell", for: indexPath) as! BasketCell
        
        switch self.datasource?.orders?[indexPath.row].status {
        case 1:
            cell.statusLbl.setText(forKey: "notworking")
        case 2:
            cell.statusLbl.setText(forKey: "ongoing")
        case 3:
            cell.statusLbl.setText(forKey: "delivered")
        case 4:
            cell.statusLbl.setText(forKey: "recived")
        case 5:
            cell.statusLbl.setText(forKey: "canceled")
        default:
            print("")
        }
        cell.addressLbl.text = self.datasource?.orders?[indexPath.row].address?.address1 ?? ""
        cell.dateLbl.text    = self.datasource?.orders?[indexPath.row].shipping_date ?? ""
        cell.ordernum1.text  = self.datasource?.orders?[indexPath.row].order_no ?? "0"
        cell.ordernum2.text  = "#" + (self.datasource?.orders?[indexPath.row].order_no ?? "0")

//        cell.priceView.roundCorners(corners: [.bottomRight, .bottomLeft], radius: 8)
        cell.priceView.layer.cornerRadius = 8
        cell.priceView.layer.maskedCorners = CACornerMask()
        cell.priceView.frame.size.width = cell.contentView.frame.width
        cell.currencyLbl.setText(forKey: "le")
        cell.priceLbl.text   = (self.datasource?.orders?[indexPath.row].total)!
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let nextStoryboard: UIStoryboard = UIStoryboard(name: "Order", bundle: nil)
        let vc = nextStoryboard.instantiateViewController(withIdentifier: "OrderDetailsVC") as! OrderDetailsVC
        vc.orderid = self.datasource?.orders?[indexPath.row].order_no ?? "0"
        print(vc.orderid)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
    
    
}
