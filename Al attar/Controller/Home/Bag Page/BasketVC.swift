//
//  BasketVC.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 8/22/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class BasketVC: UIViewController {

    @IBOutlet weak var pageImg: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var askLbl: UILabel!
    @IBOutlet weak var startBtn: UIButtonX!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var badgeView: UIButtonX!
    
    var datasource: _Order_Base?
    var CartArr = MyCartData.getCartData().0
    var cartarr = [[String:Any]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.badgeView.isHidden = true
        if let arr = def.array(forKey: "cartarr") as? [[String:Any]]{
         cartarr = arr
        }
        setDefault()
        self.startBtn.addTarget(self, action: #selector(openWishlist), for: .touchUpInside)

    }
    
    @objc func openWishlist(){
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
        def.set(100, forKey: "menuadd")
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }
    @IBAction func openCart(_ sender: Any) {
        let nextStoryboard: UIStoryboard = UIStoryboard(name: "Order", bundle: nil)
        let vc = nextStoryboard.instantiateViewController(withIdentifier: "CartVC") as! CartVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.navigationItem.title = LanguageHelper.getString(forKey: "basket")
    }
    
    
    func setDefault(){
        self.setSemantic(view: self.view, languageKey: LanguageHelper.getCurrentLanguage())
        if def.bool(forKey: _LoggedIn) == true{
            getData()
        }else{
            setpage()
        }
        if MyCartData.getCartData().0.count != 0 {
            self.badgeView.isHidden = false
            self.badgeView.setTitle(String(describing: MyCartData.getCartData().0.count), for: .normal)
        }
        SharedHandler.sideMenus(ViewController: self)
        titleLbl.setText(forKey: "basket")
        askLbl.setText(forKey: "wishdesc")
        startBtn.setText(forKey: "startshopping")
        self.navigationItem.title = LanguageHelper.getString(forKey: "basket")
    }

}
