//
//  BasketEx.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 8/22/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation
import Alamofire

@available(iOS 13.0, *)
extension BasketVC{
    
    func getData(){
        if let token = def.string(forKey: "logToken"){
            let headers : HTTPHeaders = [
                "Authorization": "bearer \(token)"]
            
            APIManager.sharedInstance.getRequest(URLs.MyOrders, header: headers) { (res) in
                let decoder = JSONDecoder()
                do {
                    if let dta = res.data{
                        let model = try decoder.decode(_Order_Base.self, from: dta)
                        self.datasource = model
                        print("model ==> \(self.datasource!)")
                        if self.datasource?.orders?.count != 0{
                            self.titleLbl.isHidden = true
                            self.askLbl.isHidden = true
                            self.startBtn.isHidden = true
                           self.table.isHidden = false
                            self.table.dataSource = self
                            self.table.delegate   = self
                            self.table.reloadData()
                        }else{
                            self.setpage()
                        }
                    }else{
                        self.getData()
                    }
                }catch{
                    self.setpage()
                    print("model ==> \(res.value)")
                }
            }
        }else{
           setpage()
        }
        
    }
    
    func setpage(){
        pageImg.isHidden = false
                   titleLbl.isHidden = false
                   askLbl.isHidden = false
                   startBtn.isHidden = false
                   table.isHidden = true
    }
    
}
