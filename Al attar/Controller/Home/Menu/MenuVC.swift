//
//  MenuVC.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 7/22/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import UIKit
import Alamofire

var glbcatsource: Cat_Base?

class MenuVC: UITableViewController {
    
    let catsArrImg = [UIImage(named: "tea_sidemenu"), UIImage(named: "oil_sidemenu"), UIImage(named: "dates_sidemenu"), UIImage(named: "cinnamon"), UIImage(named: "honey_sidemenu"), UIImage(named: "gym_sidemenu"), UIImage(named: "dessert_sidemenu"), UIImage(named: "surface1"), UIImage(named: "drinks_sidemenu"), UIImage(named: "logout_sidemenu")]
    
    let catsArr    = ["tea", "oil", "dates", "cinnamon", "honey", "gym", "dessert", "herbs", "drinks", "logout"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return (glbcatsource?.categories!.count ?? 0) + 1
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MenuProfileCell", for: indexPath) as! MenuProfileCell
            cell.nameLbl.text = def.string(forKey: "logName")
//            let url = URL(string: def.string(forKey: "logImg")!)
//            cell.img.kf.setImage(with: url)
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "menuCell", for: indexPath) as! ProfileCell
            cell.img.isHidden = true
//            cell.id =
            if LanguageHelper.getCurrentLanguage() == "ar"{
                cell.infoLbl.text = glbcatsource?.categories?[indexPath.row - 1].title_ar
            }else{
                cell.infoLbl.text = glbcatsource?.categories?[indexPath.row - 1].title_en
            }
            return cell
        }
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 269
        }else{
            return 45
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            print("rere")
            if def.bool(forKey: _LoggedIn) == true{
                def.set(4, forKey: "menuadd")
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
                UIApplication.shared.keyWindow?.rootViewController = viewController
//                let vc = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "tab")  as! UITabBarController
//                vc.selectedIndex = 3
//                UIApplication.shared.keyWindow?.rootViewController = vc
            }
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "menuCell", for: indexPath) as! ProfileCell
            def.set(glbcatsource?.categories?[indexPath.row - 1].id ?? 0, forKey: "catid")
            var strURL = ""
            let word = (glbcatsource?.imgs_url)!
            if let base = word.range(of: "\n")?.lowerBound {
                let substring = word[..<base]                 // "ora"
                let string = String(substring)
                strURL = string
                print(string)  // "ora"
            }
            
            var tailStr = (glbcatsource?.categories?[indexPath.row - 1].image)!
            tailStr = tailStr.replacingOccurrences(of: " ", with: "%20")
            def.set(strURL + "/" + tailStr, forKey: "catimg")
            if LanguageHelper.getCurrentLanguage() == "ar"{
                def.set(glbcatsource?.categories?[indexPath.row - 1].title_ar ?? "", forKey: "cattitle")
            }else{
                def.set(glbcatsource?.categories?[indexPath.row - 1].title_en ?? "", forKey: "cattitle")
            }
            
//            checktypeglb = 1
            
            def.set(1, forKey: "menuadd")
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
            UIApplication.shared.keyWindow?.rootViewController = viewController
        }
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
