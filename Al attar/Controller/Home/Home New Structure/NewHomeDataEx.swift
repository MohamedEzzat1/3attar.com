//
//  NewHomeDataEx.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 11/12/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation
import Alamofire

@available(iOS 13.0, *)
extension NewHomeVC{
    
    func getSliderData(){
        let headers : HTTPHeaders  = [
            ]
        
        APIManager.sharedInstance.getRequest(URL(string:URLs.gallery)!) { (res) in
            
            
            
            let decoder = JSONDecoder()
            do {
                if let dta = res.data{
                    let model = try decoder.decode(Gallery_Base.self, from: dta)
                    self.gallerysource = model
                    
                    

//                    print("model ==> \(self.gallerysource!)")
                    self.table.reloadData()
                }else{
                    self.getSliderData()
                }
            }catch{
                SharedHandler.alertDialog2lang(self, msgEg: "invalid data Parsing.", msgAr: "خطأ في تحويل البيانات.")
            }
        }
    }
    
    func getBestSellingData(){
        let headers : HTTPHeaders  = [
            ]
        
        APIManager.sharedInstance.getRequest(URL(string:URLs.bestselling)!) { (res) in
            
//            print(res.value, "sadwef")
            
            let decoder = JSONDecoder()
            do {
                if let dta = res.data{
                    let model = try decoder.decode(HomeProductDetails.self, from: dta)
                    self.productssource = model
                    print(self.productssource?.article, "DsfwefW")
//                    print("model ==> \(self.productssource!)")
                    self.table.reloadData()
                }else{
                    self.getBestSellingData()
                }
            }catch{
                SharedHandler.alertDialog2lang(self, msgEg: "invalid data Parsing.", msgAr: "خطأ في تحويل البيانات.")
            }
        }
    }
    
    func getNewArrivedData(){
        let headers : HTTPHeaders  = [
            ]
        
        APIManager.sharedInstance.getRequest(URL(string:URLs.newly_arrived)!) { (res) in
            
//            print(res.value, "sadwef")
            
            let decoder = JSONDecoder()
            do {
                if let dta = res.data{
                    let model = try decoder.decode(HomeProductDetails.self, from: dta)
                    self.productssourcenewarrived = model
                    print(self.productssourcenewarrived?.article, "DsfwefW")
//                    print("model ==> \(self.productssource!)")
                    self.table.reloadData()
                }else{
                    self.getNewArrivedData  ()
                }
            }catch{
                SharedHandler.alertDialog2lang(self, msgEg: "invalid data Parsing.", msgAr: "خطأ في تحويل البيانات.")
            }
        }
    }
    
    func getData(catid: Int){
        
        let headers : HTTPHeaders  = [
            "cache-control": "no-cache"
        ]
        
        
//        SharedHandler.startloadingGIF(self.view)//startSpinner(spinner, v, self.view)
        APIManager.sharedInstance.getRequest(URLs.subCategory + "\(catid)", header: headers) { (res) in
//            SharedHandler.stoploadingGIF(self.view)//stopSpinner(self.spinner, self.v, self.view)
            let decoder = JSONDecoder()
            do{
                if let data = res.data{
                    let model = try decoder.decode(Cat_Base.self, from: data)
                    self.catsource = model
                    glbcatsource = model
                    self.table.reloadData()
                    print(self.catsource?.categories, "dsfsdfewfW")
                }else{
                    self.getData(catid: catid)
                }
            }catch{
                
            }
        }
    }
    
    func getBrands(){
        let headers : HTTPHeaders  = [
            "cache-control": "no-cache"
        ]
        
        
//        SharedHandler.startSpinner(spinner, v, self.view)
        APIManager.sharedInstance.getRequest(URLs.brands, header: headers) { (res) in
//            SharedHandler.stopSpinner(self.spinner, self.v, self.view)
            let decoder = JSONDecoder()
            do{
                if let data = res.data{
                    let model = try decoder.decode(Brands_Base.self, from: data)
                    self.brandsource = model
//                    glbcatsource = model
                    self.table.reloadData()
//                    self.brandsCollection.dataSource = self
//                    self.brandsCollection.delegate = self
//                    self.brandsCollection.reloadData()
                }else{
                    self.getBrands()
                }
            }catch{
                
            }
        }
    }
    
    func getBanners2(bannerid: Int){
        let headers : HTTPHeaders  = [
            "cache-control": "no-cache"
        ]
        
        
//        SharedHandler.startSpinner(spinner, v, self.view)
        APIManager.sharedInstance.getRequest(URLs.newbanners + "\(bannerid)", header: headers) { (res) in
            print(res.value, "Sasdwfw")
//            SharedHandler.stopSpinner(self.spinner, self.v, self.view)
            let decoder = JSONDecoder()
            do{
                if let data = res.data{
                    let model = try decoder.decode(Banner_Base.self, from: data)
                    self.bannersource = model
                    print(model, "sfqwfe")
//                    glbcatsource = model
                    self.table.reloadData()
//                    self.brandsCollection.dataSource = self
//                    self.brandsCollection.delegate = self
//                    self.brandsCollection.reloadData()
                }else{
                    
                }
            }catch{
                
            }
        }
    }
    
    func getHomeList(){
        let headers : HTTPHeaders  = [
            "cache-control": "no-cache"
        ]
        APIManager.sharedInstance.getRequest(URLs.homeList, header: headers) { (res) in
            
            let decoder = JSONDecoder()
            do{
                if let data = res.data{
                    let model = try decoder.decode(_Home_List_Base.self, from: data)
                    self.homelist = model
                    if let applist = self.homelist?.app_list{
                        for i in applist{
                            switch i.type {
                            case 1:
                                self.getData(catid: i.category_id ?? 0)
                            case 2:
                                self.getBanners2(bannerid: i.banner_id ?? 0)
                            case 3:
                                self.getBrands()
                            default:
                                print(0)
                            }
                        }
                    }
                    self.table.reloadData()
                    
                    print(self.homelist, "hahaha:")
                }else{
                    self.getHomeList()
                }
            }catch{
                
            }
        }
    }
    
    @objc func sendSearch(){
        
        if searchTF.text == "" {
            print("Enter text")
        }else{
            self.sendsearch()
        }
    }
    
    func checkmenu(){
        if def.integer(forKey: "menuadd") == 4{
            tabBarController?.selectedIndex = 3
        }else if def.integer(forKey: "menuadd") == 1{
            let nextStoryboard: UIStoryboard = UIStoryboard(name: "Order", bundle: nil)
            let vc = nextStoryboard.instantiateViewController(withIdentifier: "CategoryPageVC") as! CategoryPageVC
            // Open Category Page from Side Menu
            vc.checktype = 1
            vc.catid = def.integer(forKey: "catid")
            vc.cattitle = def.string(forKey: "cattitle") ?? ""
            vc.imageURL = def.string(forKey: "catimg") ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func sendsearch(){
        let search = searchTF.text
        SharedHandler.startloadingGIF(self.view) //startSpinner(spinner, v, self.view)
        let body = [
            "keyword": searchTF.text!
        ]
        APIManager.sharedInstance.postRequest(URLs.search, Parameters: body) { (res) in
            SharedHandler.stoploadingGIF(self.view) //stopSpinner(self.spinner, self.v, self.view)
            let decoder = JSONDecoder()
            do{
                let model = try decoder.decode(Search_Base.self, from: res.data!)
                if model.products?.count != 0{
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchVC") as! SearchVC
                    vc.searchWord = search ?? ""
                    vc.checksearch = 1
                    vc.searchsource = model
                    vc.brandsource  = self.brandsource
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    SharedHandler.alertDialog2lang(self, msgEg: "No products found!", msgAr: "لا يوجد منتجات!")
                }
                
            }catch{
                
            }
        }
    }
    
}
