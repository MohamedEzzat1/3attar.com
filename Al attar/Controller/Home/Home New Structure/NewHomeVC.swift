//
//  NewHomeVC.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 11/12/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import UIKit


@available(iOS 13.0, *)
class NewHomeVC: UIViewController, CartDelegate, OpenBanner1Delegate, OpenProductNewDelegate, OpenProductBestSellingDelegate, OpenBrandsDelegate, OpenSubCategoryDelegate, UITextFieldDelegate {
    
    
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var badgeView: UIButtonX!
    @IBOutlet weak var searchTF: UITextFieldX!
    
    var gallerysource: Gallery_Base?
    var catsource: Cat_Base?
    var brandsource: Brands_Base?
    var productssource: HomeProductDetails?
    var productssourcenewarrived: HomeProductDetails?
    var homelist: _Home_List_Base?
    var bannersource: Banner_Base?
    var CartArr = MyCartData.getCartData().0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        checkmenu()
        self.searchTF.delegate = self
        self.badgeView.isHidden = true
        getSliderData()
        getBestSellingData()
        getNewArrivedData()
        getBanners2(bannerid: 1)
        getBrands()
        getData(catid: 3)
        setDefault()
//        self.getHomeList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.navigationItem.title = LanguageHelper.getString(forKey: "home")
        if MyCartData.getCartData().0.count != 0 {
            print("SADASFSAF")
            self.badgeView.isHidden = false
            self.badgeView.setTitle(String(describing: MyCartData.getCartData().0.count), for: .normal)
        }
    }
    
    @IBAction func addToCartBtn(_ sender: Any) {
        let nextStoryboard: UIStoryboard = UIStoryboard(name: "Order", bundle: nil)
        let vc = nextStoryboard.instantiateViewController(withIdentifier: "CartVC") as! CartVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == searchTF{
            self.sendSearch()
        }
        return true
    }
    
    func openBestProduct(productid: Int) {
        let nextStoryboard: UIStoryboard = UIStoryboard(name: "Order", bundle: nil)
        let vc = nextStoryboard.instantiateViewController(withIdentifier: "ProductDetailsVC") as! ProductDetailsVC
        vc.productid = productid
        vc.cartdelegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func openProduct(productid: Int) {
        let nextStoryboard: UIStoryboard = UIStoryboard(name: "Order", bundle: nil)
        let vc = nextStoryboard.instantiateViewController(withIdentifier: "ProductDetailsVC") as! ProductDetailsVC
        vc.productid = productid
        vc.cartdelegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func openBanner(checkuri: Bool, producturi: String, checktype: Int, imageURL: String) {
        
        if checktype == 0{
            let nextStoryboard: UIStoryboard = UIStoryboard(name: "Order", bundle: nil)
            let vc = nextStoryboard.instantiateViewController(withIdentifier: "ProductDetailsVC") as! ProductDetailsVC
            vc.checkuri = true
            vc.producturi = producturi
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let nextStoryboard: UIStoryboard = UIStoryboard(name: "Order", bundle: nil)
            let vc = nextStoryboard.instantiateViewController(withIdentifier: "CategoryPageVC") as! CategoryPageVC
            vc.checkuri = true
            vc.producturi = producturi
            vc.checktype = 1
            vc.imageURL = imageURL
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    func openBrands(brandid: Int, brandsource: Brands_Base, cattitle: String, imageURL: String) {
        let nextStoryboard: UIStoryboard = UIStoryboard(name: "Order", bundle: nil)
        let vc = nextStoryboard.instantiateViewController(withIdentifier: "CategoryPageVC") as! CategoryPageVC
        vc.cartdelegate = self
        vc.brandid = brandid
        vc.checktype = 2
        print(vc.checktype,"Sdfewfwe")
        vc.brandsource = brandsource
        vc.cattitle = cattitle
//        vc.checkuri = false
        print(brandid, "ewrwetre")
        vc.imageURL = imageURL
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func openCats(checktype: Int, catid: Int, catsource: Cat_Base, imageURL: String, cattitle: String) {
        
        let nextStoryboard: UIStoryboard = UIStoryboard(name: "Order", bundle: nil)
        let vc = nextStoryboard.instantiateViewController(withIdentifier: "CategoryPageVC") as! CategoryPageVC
        vc.cartdelegate = self
        vc.checktype = 1
        
        vc.catid = catid
        vc.catsource = catsource

        
        vc.imageURL = imageURL
        vc.cattitle = cattitle
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func openseeallNewArrived() {
        
        let nextStoryboard: UIStoryboard = UIStoryboard(name: "Order", bundle: nil)
        let vc = nextStoryboard.instantiateViewController(withIdentifier: "CategoryPageVC") as! CategoryPageVC
        vc.cartdelegate = self
        vc.checktype = 3
        
        vc.productsArray = productssourcenewarrived
       
        vc.cattitle = LanguageHelper.getString(forKey: "Newlyarrived")
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func openseeallBestSale() {
        
        let nextStoryboard: UIStoryboard = UIStoryboard(name: "Order", bundle: nil)
        let vc = nextStoryboard.instantiateViewController(withIdentifier: "CategoryPageVC") as! CategoryPageVC
        vc.cartdelegate = self
        vc.checktype = 3
        
        vc.productsArray = self.productssource

        vc.cattitle = LanguageHelper.getString(forKey: "Bestselling")
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    

    func setCart(cartcount: String) {
        self.badgeView.isHidden = false
        print(cartcount, "rerqrqwE")
        self.badgeView.setTitle(cartcount, for: .normal)
    }

    func setDefault(){
        self.setSemantic(view: self.view, languageKey: LanguageHelper.getCurrentLanguage())
//        self.navigationItem.title = "عطَّار"//LanguageHelper.getString(forKey: "attar")
        self.navigationItem.backBarButtonItem?.tintColor = UIColor.black
        SharedHandler.sideMenus(ViewController: self)
//        self.setcellsspacing()
        
        
    }
    
}
