//
//  NewHomeTableEx.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 11/12/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation

@available(iOS 13.0, *)
extension NewHomeVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6//(self.homelist?.app_list?.count ?? 0) + 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "BannerCell", for: indexPath) as! BannerCell
            cell.gallerysource = self.gallerysource

            cell.bannerdelegate = self
            
            cell.slidercollection.delegate = cell
            cell.slidercollection.dataSource = cell
            cell.slidercollection.reloadData()
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeCategoriesCell", for: indexPath) as! HomeCategoriesCell
            if LanguageHelper.getCurrentLanguage() == "ar"{
                cell.titleLbl.text = self.catsource?.categories?[0].title_ar
            }else{
                cell.titleLbl.text = self.catsource?.categories?[0].title_en
            }
            cell.catdelegate = self
            cell.catsource = self.catsource
            cell.catcollection.delegate = cell
            cell.catcollection.dataSource = cell
            cell.catcollection.reloadData()
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeProductsCell", for: indexPath) as! HomeProductsCell
            cell.productsource = self.productssource
            
            cell.cartdelegete  = self
            cell.productdelegate = self
            cell.brandsdelegate = self
            cell.seeallBtn.setText(forKey: "seeall")
            cell.titleLbl.setText(forKey: "Bestselling")
            cell.brandsource = self.brandsource
            cell.productsarr = self.productssource?.article
            cell.productsCollection.delegate = cell
            cell.productsCollection.dataSource = cell
            cell.productsCollection.reloadData()
            cell.seeallBtn.addTarget(self, action: #selector(openseeallBestSale), for: .touchUpInside)
            return cell
            
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "NewBannerCell", for: indexPath) as! NewBannerCell
            cell.bannersource = self.bannersource
            cell.slidercollection.delegate = cell
            cell.slidercollection.dataSource = cell
            cell.slidercollection.reloadData()
            return cell
            
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "NewArrivedProductsCell", for: indexPath) as! NewArrivedProductsCell
            cell.productsource = self.productssourcenewarrived
            cell.seeallBtn.setText(forKey: "seeall")
            cell.cartdelegete  = self
            cell.newcartdelegate = self
            cell.productdelegate = self
            cell.brandsdelegate = self
            
            cell.titleLbl.setText(forKey: "Newlyarrived")
            cell.brandsource = self.brandsource
            cell.productsarr = self.productssourcenewarrived?.article
            cell.productsCollection.delegate = cell
            cell.productsCollection.dataSource = cell
            cell.productsCollection.reloadData()
            cell.seeallBtn.addTarget(self, action: #selector(openseeallNewArrived), for: .touchUpInside)
            return cell
        case 5:
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeBrandsCell", for: indexPath) as! HomeBrandsCell
            cell.brandsource = self.brandsource
            
            cell.branddelegate = self
            
            cell.collection.delegate = cell
            cell.collection.dataSource = cell
            cell.collection.reloadData()
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "BannerCell", for: indexPath) as! BannerCell
            return cell
        }
        
//        if indexPath.row == 0{
//            let cell = tableView.dequeueReusableCell(withIdentifier: "BannerCell", for: indexPath) as! BannerCell
//            cell.gallerysource = self.gallerysource
//            cell.slidercollection.delegate = cell
//            cell.slidercollection.dataSource = cell
//            cell.slidercollection.reloadData()
//            return cell
//        }else if indexPath.row == 1{
//            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeProductsCell", for: indexPath) as! HomeProductsCell
//            cell.productsource = self.productssource
//            cell.cartdelegete  = self
//            cell.productsarr = self.productssource?.article
//            cell.productsCollection.delegate = cell
//            cell.productsCollection.dataSource = cell
//            cell.productsCollection.reloadData()
//            return cell
//        }else if indexPath.row == 2{
//            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeProductsCell", for: indexPath) as! HomeProductsCell
//            cell.productsource = self.productssource
//            cell.cartdelegete  = self
//            cell.productsarr = self.productssource?.article
//            cell.productsCollection.delegate = cell
//            cell.productsCollection.dataSource = cell
//            cell.productsCollection.reloadData()
//            return cell
//        }else{
//            switch self.homelist?.app_list?[indexPath.row].type {
//            case 1:
//                //category
//                let cell = tableView.dequeueReusableCell(withIdentifier: "HomeCategoriesCell", for: indexPath) as! HomeCategoriesCell
//                if LanguageHelper.getCurrentLanguage() == "ar"{
//                    cell.titleLbl.text = self.catsource?.categories?[0].title_ar
//                }else{
//                    cell.titleLbl.text = self.catsource?.categories?[0].title_en
//                }
//                cell.catsource = self.catsource
//                cell.catcollection.delegate = cell
//                cell.catcollection.dataSource = cell
//                cell.catcollection.reloadData()
//                return cell
//            case 2:
//                //banner
//                let cell = tableView.dequeueReusableCell(withIdentifier: "NewBannerCell", for: indexPath) as! NewBannerCell
//                cell.slidercollection.delegate = cell
//                cell.slidercollection.dataSource = cell
//                return cell
//            case 3:
//                //brand
//                let cell = tableView.dequeueReusableCell(withIdentifier: "HomeBrandsCell", for: indexPath) as! HomeBrandsCell
//                cell.brandsource = self.brandsource
//                cell.collection.delegate = cell
//                cell.collection.dataSource = cell
//                cell.collection.reloadData()
//                return cell
//            default:
//                let cell = tableView.dequeueReusableCell(withIdentifier: "HomeProductsCell", for: indexPath) as! HomeProductsCell
//                cell.productsource = self.productssource
//                cell.cartdelegete  = self
//                cell.productsarr = self.productssource?.article
//                cell.productsCollection.delegate = cell
//                cell.productsCollection.dataSource = cell
//                cell.productsCollection.reloadData()
//                return cell
//            }
//        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 180
        case 1:
            return 190
        case 2:
            return 278
        case 3:
            return 278
        case 4:
            return 278
        case 5:
            return 182
        default:
            return 0
        }
    }
}
