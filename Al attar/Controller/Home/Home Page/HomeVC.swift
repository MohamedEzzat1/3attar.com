//
//  HomeVC.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 7/23/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import UIKit
import Lottie

@available(iOS 13.0, *)
class HomeVC: UIViewController, UITextFieldDelegate, CartDelegate{
    
    

    @IBOutlet weak var bar: UIButton!
//    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var collectionview: UICollectionView!
    @IBOutlet weak var sliderControl: UIPageControl!
    @IBOutlet weak var catLbl: UILabel!
    @IBOutlet weak var catCollection: UICollectionView!
    @IBOutlet weak var badgeView: UIButtonX!
    @IBOutlet weak var brandsLbl: UILabel!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var searchTF: UITextFieldX!
    @IBOutlet weak var brandsCollection: UICollectionView!
    
    // 1. Create the AnimationView
    
    
    // search bar
    lazy   var searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: ((self.navigationController?.navigationBar.frame.width ?? 0) / 1.4)  , height: 0))
    lazy var srch = UIBarButtonItem(image: #imageLiteral(resourceName: "serach_icon"), style: .plain, target: self, action: #selector(self.sendSearch))
    
    var gallerysource: Gallery_Base?
    var catsource: Cat_Base?
    var brandsource: Brands_Base?
    var CartArr = MyCartData.getCartData().0
    
    var v = UIView()
    var cartarr = [[String:Any]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        checkmenu()
        if let arr = def.array(forKey: "cartarr") as? [[String:Any]]{
         cartarr = arr
        }
        self.navigationItem.backBarButtonItem?.title = ""
        searchBar.placeholder = "Search for Items"
        let leftNavBarButton = UIBarButtonItem(customView:searchBar)
//            self.searchTF.addTarget(self, action: #selector(sendSearch), for: .editingChanged)
            // Fallback on earlier versions
        self.searchTF.delegate = self
        self.badgeView.isHidden = true
        spinner.stopAnimating()
        sliderControl.hidesForSinglePage = true
        getSliderData()
        getData()
        getBrands()
        getCity()
        setDefault()
//        SharedHandler.startloadingGIF(self.view)
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.navigationItem.title = LanguageHelper.getString(forKey: "home")
        if MyCartData.getCartData().0.count != 0 {
            print("SADASFSAF")
            self.badgeView.isHidden = false
            self.badgeView.setTitle(String(describing: MyCartData.getCartData().0.count), for: .normal)
        }
    }
    
    func setCart(cartcount: String) {
        self.badgeView.isHidden = false
        self.badgeView.setTitle(cartcount, for: .normal)
    }
    
    @IBAction func serachBtn(_ sender: Any) {
        self.sendSearch()
    }
    @IBAction func openCart(_ sender: Any) {
        let nextStoryboard: UIStoryboard = UIStoryboard(name: "Order", bundle: nil)
        let vc = nextStoryboard.instantiateViewController(withIdentifier: "CartVC") as! CartVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == searchTF{
            self.sendSearch()
        }
        return true
    }
    
    func setDefault(){
        self.setSemantic(view: self.view, languageKey: LanguageHelper.getCurrentLanguage())
        self.catLbl.setText(forKey: "categories")
        self.brandsLbl.setText(forKey: "brands")
        if LanguageHelper.getCurrentLanguage() == "ar"{
            self.brandsCollection.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            self.brandsCollection.semanticContentAttribute = .forceLeftToRight
        }
//        self.navigationItem.title = "عطَّار"//LanguageHelper.getString(forKey: "attar")
        self.navigationItem.backBarButtonItem?.tintColor = UIColor.black
        SharedHandler.sideMenus(ViewController: self)
//        self.setcellsspacing()
        
        
    }
    
}
