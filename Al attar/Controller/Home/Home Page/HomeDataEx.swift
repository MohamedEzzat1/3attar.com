//
//  HomeDataEx.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 7/23/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import UIKit
import Alamofire

@available(iOS 13.0, *)
extension HomeVC{
    
    func getSliderData(){
        let headers : HTTPHeaders  = [
            ]
        
        APIManager.sharedInstance.getRequest(URL(string:URLs.gallery)!) { (res) in
            
            
            
            let decoder = JSONDecoder()
            do {
                if let dta = res.data{
                    let model = try decoder.decode(Gallery_Base.self, from: dta)
                    self.gallerysource = model
                    
                    

                    print("model ==> \(self.gallerysource!)")
                    self.sliderControl.numberOfPages = self.gallerysource?.gallery?.count ?? 0
                    self.collectionview.dataSource = self
                    self.collectionview.delegate   = self
//                    self.collectionview.scrollToItem(at: NSIndexPath(item: 1, section: (self.gallerysource?.gallery!.count)! - 1) as IndexPath, at: .left, animated: true)
                    self.collectionview.reloadData()
                }else{
                    self.getSliderData()
                }
            }catch{
                SharedHandler.alertDialog2lang(self, msgEg: "invalid data Parsing.", msgAr: "خطأ في تحويل البيانات.")
            }
        }
            
    }
    
    func sendsearch(){
        let search = searchTF.text
        SharedHandler.startloadingGIF(self.view) //startSpinner(spinner, v, self.view)
        let body = [
            "keyword": searchTF.text!
        ]
        APIManager.sharedInstance.postRequest(URLs.search, Parameters: body) { (res) in
            SharedHandler.stoploadingGIF(self.view) //stopSpinner(self.spinner, self.v, self.view)
            let decoder = JSONDecoder()
            do{
                let model = try decoder.decode(Search_Base.self, from: res.data!)
                if model.products?.count != 0{
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchVC") as! SearchVC
                    vc.searchWord = search ?? ""
                    vc.checksearch = 1
                    vc.searchsource = model
                    vc.brandsource  = self.brandsource
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    SharedHandler.alertDialog2lang(self, msgEg: "No products found!", msgAr: "لا يوجد منتجات!")
                }
                
            }catch{
                
            }
        }
    }
    
    func getCity(){
        let token = def.string(forKey: "logToken")
//            SharedHandler.startSpinner(spinner, v, self.view)
            let header: HTTPHeaders = [
                "Authorization": "bearer \(token ?? "")",
                "cache-control": "no-cache"
            ]
            print(token, "fsdfsdgSD")
            APIManager.sharedInstance.getRequest(URLs.Shippingzones, header: header) { (res) in
//                SharedHandler.stopSpinner(self.spinner, v, self.view)
                let decoder = JSONDecoder()
                if let data = res.data{
                    do{
                        let model = try decoder.decode(_Shipping_Base.self, from: data)
//                        self.cityzones = model
                        print(model, "aasfafW")
    //                    self.pickerView.reloadInputViews()
                    }catch{
                        print(res.error?.localizedDescription, res.value, res.response?.statusCode)
                    }
                    
                }else{
                    
                }
            }
            
        }
    
    func getData(){
        
        let headers : HTTPHeaders  = [
            "cache-control": "no-cache"
        ]
        
        
        SharedHandler.startloadingGIF(self.view)//startSpinner(spinner, v, self.view)
        APIManager.sharedInstance.getRequest(URLs.allcats, header: headers) { (res) in
            SharedHandler.stoploadingGIF(self.view)//stopSpinner(self.spinner, self.v, self.view)
            let decoder = JSONDecoder()
            do{
                if let data = res.data{
                    let model = try decoder.decode(Cat_Base.self, from: data)
                    self.catsource = model
                    glbcatsource = model
                    
                    self.catCollection.dataSource = self
                    self.catCollection.delegate = self
                    self.catCollection.reloadData()
                }else{
                    self.getData()
                }
            }catch{
                
            }
        }
    }
    
    func getBrands(){
        let headers : HTTPHeaders  = [
            "cache-control": "no-cache"
        ]
        
        
        SharedHandler.startSpinner(spinner, v, self.view)
        APIManager.sharedInstance.getRequest(URLs.brands, header: headers) { (res) in
            SharedHandler.stopSpinner(self.spinner, self.v, self.view)
            let decoder = JSONDecoder()
            do{
                if let data = res.data{
                    let model = try decoder.decode(Brands_Base.self, from: data)
                    self.brandsource = model
//                    glbcatsource = model
                    
                    self.brandsCollection.dataSource = self
                    self.brandsCollection.delegate = self
                    self.brandsCollection.reloadData()
                }else{
                    self.getBrands()
                }
            }catch{
                
            }
        }
    }
    
    @objc func sendSearch(){
        
        if searchTF.text == "" {
            print("Enter text")
        }else{
            self.sendsearch()
        }
    }
    
    func setcellsspacing(){
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        let width = UIScreen.main.bounds.width
        layout.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        layout.itemSize = CGSize(width: width / 2, height: width / 2)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        self.collectionview!.collectionViewLayout = layout
    }
    
    func checkmenu(){
        if def.integer(forKey: "menuadd") == 4{
            tabBarController?.selectedIndex = 3
        }else if def.integer(forKey: "menuadd") == 1{
            let nextStoryboard: UIStoryboard = UIStoryboard(name: "Order", bundle: nil)
            let vc = nextStoryboard.instantiateViewController(withIdentifier: "CategoryPageVC") as! CategoryPageVC
            // Open Category Page from Side Menu
            vc.checktype = 1
            vc.catid = def.integer(forKey: "catid")
            vc.cattitle = def.string(forKey: "cattitle") ?? ""
            vc.imageURL = def.string(forKey: "catimg") ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}
    
