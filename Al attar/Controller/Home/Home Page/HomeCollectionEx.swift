//
//  HomeCollectionEx.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 7/23/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import UIKit
import Kingfisher

@available(iOS 13.0, *)
extension HomeVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == catCollection{
            return (self.catsource?.categories?.count) ?? 0
        }else if collectionView == collectionview{
            return 1
        }else{
            return (self.brandsource?.brands?.count) ?? 0
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if collectionView == collectionview{
            return (self.gallerysource?.gallery?.count) ?? 0
        }else{
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == catCollection{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCatsCell", for: indexPath) as! HomeCatsCell
            /// Solving URL String by removing "\n"
            var strURL = ""
            let word = (self.catsource?.imgs_url)!
            if let base = word.range(of: "\n")?.lowerBound {
                let substring = word[..<base]                 // "ora"
                let string = String(substring)
                strURL = string
                print(string)  // "ora"
            }
            
            var tailStr = (self.catsource?.categories?[indexPath.row].image)!
            tailStr = tailStr.replacingOccurrences(of: " ", with: "%20")
            
            
            print(strURL + "/" + tailStr , "EEEE")
            let url = URL(string: strURL + "/" + tailStr )
            if LanguageHelper.getCurrentLanguage() == "ar"{
                cell.catLbl.text = self.catsource?.categories?[indexPath.row].title_ar
            }else{
                cell.catLbl.text = self.catsource?.categories?[indexPath.row].title_en
            }
            cell.img.kf.setImage(with: url)
            if cell.img.image == nil{
                cell.img.image = UIImage(named: "logo_splash_screen")
            }
            return cell
            
        }else if collectionView == collectionview{
             let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SliderCell", for: indexPath) as! SliderCell
            
                let url = URL(string: (self.gallerysource?.imgs_url)! + "/" + (self.gallerysource?.gallery?[indexPath.section].img)! )
                print((self.gallerysource?.imgs_url)! + "/" + (self.gallerysource?.gallery?[indexPath.section].img)! , "Asffwefwe")
                cell.img.kf.setImage(with: url)

            if cell.img.image == nil{
            cell.img.image = UIImage(named: "logo_splash_screen")
            }
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "homebrandCell", for: indexPath) as! homebrandCell
            let url = URL(string: (self.brandsource?.imgs_url)! + "/" + (self.brandsource?.brands?[indexPath.row].image)! )
            if LanguageHelper.getCurrentLanguage() == "ar"{
                cell.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            }
            
            cell.img.kf.setImage(with: url)
            if cell.img.image == nil{
            cell.img.image = UIImage(named: "logo_splash_screen")
            }
           return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.sliderControl.currentPage = indexPath.section
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == catCollection{
            return CGSize(width: self.view.frame.width/3, height: 155)
        }else if collectionView == collectionview{
            return CGSize(width: 324, height: 180)
        }else{
            return CGSize(width: 100, height: 100)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let nextStoryboard: UIStoryboard = UIStoryboard(name: "Order", bundle: nil)
        let vc = nextStoryboard.instantiateViewController(withIdentifier: "CategoryPageVC") as! CategoryPageVC
        vc.cartdelegate = self
        if collectionView == catCollection{
            
            vc.checktype = 1
            
            vc.catid = self.catsource?.categories?[indexPath.row].id ?? 0
            vc.catsource = catsource
            /// Solving URL String by removing "\n"
            var strURL = ""
            let word = (self.catsource?.imgs_url)!
            if let base = word.range(of: "\n")?.lowerBound {
                let substring = word[..<base]                 // "ora"
                let string = String(substring)
                strURL = string
                print(string)  // "ora"
            }
            
            var tailStr = (self.catsource?.categories?[indexPath.row].image)!
            tailStr = tailStr.replacingOccurrences(of: " ", with: "%20")
            vc.imageURL = strURL + "/" + tailStr
            if LanguageHelper.getCurrentLanguage() == "ar"{
                vc.cattitle = self.catsource?.categories?[indexPath.row].title_ar ?? ""
            }else{
                vc.cattitle = self.catsource?.categories?[indexPath.row].title_en ?? ""
            }
            
            self.navigationController?.pushViewController(vc, animated: true)
        }else if collectionView == brandsCollection{
            
            vc.checktype = 2
            print(vc.checktype,"Sdfewfwe")
            vc.brandid = self.brandsource?.brands?[indexPath.row].id ?? 0
            vc.brandsource = brandsource
            /// Solving URL String by removing "\n"
            let url = (self.brandsource?.imgs_url)! + "/" + (self.brandsource?.brands?[indexPath.row].image)!
            vc.imageURL = url
            if LanguageHelper.getCurrentLanguage() == "ar"{
                vc.cattitle = self.brandsource?.brands?[indexPath.row].title_ar ?? ""
            }else{
                vc.cattitle = self.brandsource?.brands?[indexPath.row].title_en ?? ""
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }else if collectionView == collectionview{
            let url = self.gallerysource?.gallery?[indexPath.section].img_url ?? ""
            if let range = url.range(of: "attar:///catalog/") {
                let uriof = url[range.upperBound...].trimmingCharacters(in: .whitespaces)
                if uriof.contains("product/"){
                    guard let urirange = uriof.range(of: "product/") else{return}
                    let uri = uriof[urirange.upperBound...].trimmingCharacters(in: .whitespaces)
                    print(uri)
                    let nextStoryboard: UIStoryboard = UIStoryboard(name: "Order", bundle: nil)
                    let vc = nextStoryboard.instantiateViewController(withIdentifier: "ProductDetailsVC") as! ProductDetailsVC
                    vc.checkuri = true
                    vc.producturi = uri
                    self.navigationController?.pushViewController(vc, animated: true)
                }else if uriof.contains("brands/"){
                    guard let urirange = uriof.range(of: "brands/") else{return}
                    let uri = uriof[urirange.upperBound...].trimmingCharacters(in: .whitespaces)
                    print(uri)
                    vc.checkuri = true
                    vc.producturi = uri
                    vc.checktype = 2
                    vc.imageURL = (self.gallerysource?.imgs_url)! + "/" + (self.gallerysource?.gallery?[indexPath.section].img)!
                    self.navigationController?.pushViewController(vc, animated: true)
                }else if uriof.contains("category/"){
                    guard let urirange = uriof.range(of: "category/") else{return}
                    let uri = uriof[urirange.upperBound...].trimmingCharacters(in: .whitespaces)
                    print(uri)
                    vc.checkuri = true
                    vc.producturi = uri
                    vc.checktype = 1
                    vc.imageURL = (self.gallerysource?.imgs_url)! + "/" + (self.gallerysource?.gallery?[indexPath.section].img)!
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
        
        
    }
    
    
//    open  var flipsHorizontallyInOppositeLayoutDirection: Bool {
//            return true
//        }
//
//    var developmentLayoutDirection: UIUserInterfaceLayoutDirection {
//        if LanguageHelper.getCurrentLanguage() == "ar"{
//            return UIUserInterfaceLayoutDirection.rightToLeft
//        }else{
//            return UIUserInterfaceLayoutDirection.leftToRight
//        }
//    }
    
    
}
