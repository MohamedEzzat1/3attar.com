//
//  HomeTab.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 7/24/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class HomeTab: UITabBarController {
    
    
    
    var cartbutton = UIButtonX()
    var badge      = UIButtonX()

    override func viewDidLoad() {
        super.viewDidLoad()
        if def.integer(forKey: "menuadd") == 0{
            
        }
        self.cartbutton.addTarget(self, action: #selector(openCart), for: .touchUpInside)
        // Do any additional setup after loading the view.
    }

    
    override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
        
        self.badge.isHidden = false
        self.badge.setTitle(String(describing: cartcounterglb), for: .normal)
//            setText()
        self.cartbutton.backgroundColor = UIColor.white
        self.cartbutton.frame = CGRect(x: 16, y: 550, width: 60, height: 60)
        self.badge.backgroundColor = UIColor.red
        self.badge.frame = CGRect(x: 50, y: 525, width: 30, height: 30)
        self.badge.isHidden = true
        self.badge.cornerRadius = 15
        
//        view.addConstraints([horizontalConstraint, verticalConstraint, widthConstraint, heightConstraint])
        self.cartbutton.setImage(UIImage(named: "supermarket"), for: .normal)
        self.cartbutton.borderColor = UIColor(red: 127/255, green: 216/255, blue: 190/255, alpha: 1)
        self.cartbutton.borderWidth = 1
        self.cartbutton.cornerRadius = 30
//        self.view.addSubview(cartbutton)
//        self.view.addSubview(badge)
//            NotificationCenter.default.addObserver(self, selector: #selector(setText), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
        }
    
    @objc func openCart() {
        let nextStoryboard: UIStoryboard = UIStoryboard(name: "Order", bundle: nil)
        let vc = nextStoryboard.instantiateViewController(withIdentifier: "CartVC") as! CartVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
        
//        override func viewWillDisappear(_ animated: Bool) {
//            super.viewWillDisappear(animated)
//            NotificationCenter.default.removeObserver(self)
//        }

}
