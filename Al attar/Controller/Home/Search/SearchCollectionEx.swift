//
//  SearchCollectionEx.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 7/24/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
@available(iOS 13.0, *)
extension SearchVC :UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if checksearch == 1{
            return searchsource?.products?.count ?? 0
        }else{
            return brandsource?.brands?[0].products?.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductsCell", for: indexPath) as! ProductsCell
        cell.greenview.isHidden = true
        cell.cartdelegete = self
        cell.celltag = indexPath.row
        
        if checksearch == 1{
            print("asfeefW")
            if let brands = self.brandsource?.brands{
                for i in brands{
                    
                    if self.searchsource?.products?[indexPath.row].brand_id == i.id{
                        let base_url = self.brandsource?.imgs_url
                        let imgurl   = brands[brands.index(where: { $0.id == i.id }) ?? 0 ].image
                        
                        let url = base_url! + "/" + imgurl!
                        cell.brandimage = url
                        let u = URL(string: url)
                        cell.brandImg.cornerRadius = 22.5
                        cell.brandImg.kf.setImage(with: u)
                        if cell.brandImg.image == nil{
                            cell.brandImg.image = UIImage(named: "logo_splash_screen")
                        }
                    }
                }
            }
            
            cell.cellbrandid = self.searchsource?.products?[indexPath.row].brand_id ?? 0
            cell.productid =  String(describing:(self.searchsource?.products?[indexPath.row].id!)!)
            cell.cellPrice = self.searchsource?.products?[indexPath.row].regular_price as! String
            cell.cellBaseimg = self.searchsource?.imgs_url as! String
            cell.celltitle_ar = self.searchsource?.products?[indexPath.row].title_ar as! String
            cell.celltitle_en = self.searchsource?.products?[indexPath.row].title_en as! String
                    
                if LanguageHelper.getCurrentLanguage() == "ar"{
                    cell.addnewItem.roundCorners(corners: [.topRight, .bottomLeft], radius: 10)
                    cell.prdouctName.text = self.searchsource?.products?[indexPath.row].title_ar
                    cell.desc.text = self.searchsource?.products?[indexPath.row].description_ar
                    cell.price.text = (self.searchsource?.products?[indexPath.row].regular_price)! + " جنيه"
                }else{
                    cell.addnewItem.roundCorners(corners: [.topLeft, .bottomRight], radius: 10)
                    cell.prdouctName.text = self.searchsource?.products?[indexPath.row].title_en
                    cell.desc.text = self.searchsource?.products?[indexPath.row].description_en
                    cell.price.text = (self.searchsource?.products?[indexPath.row].regular_price)! + " LE"
                }
            
            cell.brandBtn.addTarget(self, action: #selector(openBrandProducts), for: .touchUpInside)
            
            //        cell.price.text = self.productsource?.categories?[indexPath.row].regular_price
            let base_url = self.searchsource?.imgs_url
            let imgurl   = self.searchsource?.products?[indexPath.row].image
            let url = base_url! + "/" + imgurl!
            
            let u = URL(string: url)
            cell.img.kf.setImage(with: u)
//            cell.addnewItem.addTarget(self, action: #selector(addcounter), for: .touchUpInside)
        }else{
            cell.productid = String(describing:self.brandsource?.brands![0].products?[indexPath.row].id)
            cell.cellPrice = self.brandsource?.brands![0].products?[indexPath.row].regular_price as! String
            cell.cellBaseimg = self.brandsource?.imgs_url as! String
            cell.celltitle_ar = self.brandsource?.brands![0].products?[indexPath.row].title_ar as! String
            cell.celltitle_en = self.brandsource?.brands![0].products?[indexPath.row].title_en as! String
                    
                if LanguageHelper.getCurrentLanguage() == "ar"{
                    cell.addnewItem.roundCorners(corners: [.topRight, .bottomLeft], radius: 10)
                    cell.prdouctName.text = self.brandsource?.brands![0].products?[indexPath.row].title_ar
                    cell.desc.text = self.brandsource?.brands![0].products?[indexPath.row].description_ar
                    cell.price.text = (self.brandsource?.brands![0].products?[indexPath.row].regular_price)! + " جنيه"
                }else{
                    cell.addnewItem.roundCorners(corners: [.topLeft, .bottomRight], radius: 10)
                    cell.prdouctName.text = self.brandsource?.brands![0].products?[indexPath.row].title_en
                    cell.desc.text = self.brandsource?.brands![0].products?[indexPath.row].description_en
                    cell.price.text = (self.brandsource?.brands![0].products?[indexPath.row].regular_price)! + " LE"
                }
            //        cell.price.text = self.productsource?.categories?[indexPath.row].regular_price
            let base_url = self.brandsource?.imgs_url
            let imgurl   = self.brandsource?.brands![0].products?[indexPath.row].image
            let url = base_url! + "/" + imgurl!
            
            let u = URL(string: url)
            cell.img.kf.setImage(with: u)
            if cell.img.image == nil{
                cell.img.image = UIImage(named: "logo_splash_screen")
            }
//            cell.addnewItem.addTarget(self, action: #selector(addcounter), for: .touchUpInside)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductsCell", for: indexPath) as! ProductsCell
        let nextStoryboard: UIStoryboard = UIStoryboard(name: "Order", bundle: nil)
        let vc = nextStoryboard.instantiateViewController(withIdentifier: "ProductDetailsVC") as! ProductDetailsVC
        if checksearch == 1{
            vc.productid = self.searchsource?.products?[indexPath.row].id
        }else{
            vc.productid = self.brandsource?.brands![0].products?[indexPath.row].id
        }
        
        vc.cartdelegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView
    {
        if kind == UICollectionView.elementKindSectionHeader
        {
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "catheader", for: indexPath) as! CatReusableView
//            let url = URL(string:self.imageURL)
//            headerView.img.kf.setImage(with: url)
            headerView.catTitle.text = self.searchWord
//            headerView.filterBtn.setText(forKey: "chooseby")
//            headerView.filterBtn.addTarget(self, action: #selector(openFilter), for: .touchUpInside)
            return headerView
        }
        return UICollectionReusableView()
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplaySupplementaryView view: UICollectionReusableView, forElementKind elementKind: String, at indexPath: IndexPath) {
        self.navigationItem.title = ""
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplayingSupplementaryView view: UICollectionReusableView, forElementOfKind elementKind: String, at indexPath: IndexPath) {
        self.navigationItem.title = self.searchWord
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return CGSize(width: 180, height: 228)
        return CGSize(width: self.collection.frame.width * 0.46, height: self.collection.frame.height * 0.4)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0.0
    }
}
