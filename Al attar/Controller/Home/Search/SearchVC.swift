//
//  SearchVC.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 7/24/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class SearchVC: UIViewController, CartDelegate {
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var collection: UICollectionView!
    @IBOutlet weak var cartcounter: UIButtonX!
    @IBOutlet weak var notLbl: UILabel!
    
    let btn2 = UIButton(type: .custom)
    var searchWord = ""
    var brandid: Int?
    var brandsource: Brands_Base?
    var searchsource: Search_Base?
    var checksearch: Int?
    var CartArr = MyCartData.getCartData().0
    var cartarr = [[String:Any]]()

    override func viewDidLoad() {
        super.viewDidLoad()
        if searchsource?.products?.count != 0{
            self.notLbl.isHidden = true
            self.notLbl.setText(forKey: "notfound")
        }
        SharedHandler.stopSpinner(self.spinner, v, self.view)
        self.cartcounter.isHidden = true
        if let arr = def.array(forKey: "cartarr") as? [[String:Any]]{
         cartarr = arr
        }
        if MyCartData.getCartData().0.count != 0 {
            self.cartcounter.isHidden = false
            self.cartcounter.setTitle(String(describing: MyCartData.getCartData().0.count), for: .normal)
        }
        btn2.setTitle(LanguageHelper.getString(forKey: "cancel"), for: .normal)
        btn2.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn2.setTitleColor(UIColor.black, for: .normal)
        btn2.tintColor = UIColor.black
        btn2.addTarget(self, action: #selector(backtoviewcontroller), for: .touchUpInside)
        let item2 = UIBarButtonItem(customView: btn2)
        self.navigationItem.setLeftBarButtonItems([item2], animated: true)
    }
    
    @objc func backtoviewcontroller(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func setCart(cartcount: String) {
        self.cartcounter.isHidden = false
        self.cartcounter.setTitle(cartcount, for: .normal)
    }

    @IBAction func cartBtn(_ sender: Any) {
        let nextStoryboard: UIStoryboard = UIStoryboard(name: "Order", bundle: nil)
        let vc = nextStoryboard.instantiateViewController(withIdentifier: "CartVC") as! CartVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
