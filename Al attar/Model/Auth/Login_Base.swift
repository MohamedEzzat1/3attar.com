//
//  Login_Base.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 7/20/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation

struct Login_Base: Codable {
    let success : Bool?
    let token : String?
    let user : _Data?
    let message : String?
    
    enum CodingKeys : String, CodingKey  {
        case success = "success"
        case token = "token"
        case user = "user"
        case message = "message"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
        token = try values.decodeIfPresent(String.self, forKey: .token)
        user = try values.decodeIfPresent(_Data.self, forKey: .user)
        message = try values.decodeIfPresent(String.self, forKey: .message)
    }
}
