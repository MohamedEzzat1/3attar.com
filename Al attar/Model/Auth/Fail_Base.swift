//
//  Fail_Base.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 7/20/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation

struct Fail_Base{
    let success: Bool?
    let message: String?
}
