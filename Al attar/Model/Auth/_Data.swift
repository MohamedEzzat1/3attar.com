//
//  _Data.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 7/20/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation

struct _Data: Codable {
    let id : Int?
    let name : String?
    let username : String?
    let mobile : String?
    let email : String?
    var email_verified_at : String?
    var api_token :String?
    let image: String?
    let user_type: Int?
    let source: Int?
    let verified: Int?
    let created_at: String?
    let updated_at: String?
    
    enum CodingKeys : String, CodingKey  {
        case id = "id"
        case name = "name"
        case username = "username"
        case mobile = "mobile"
        case email = "email"
        case email_verified_at = "email_verified_at"
        case api_token = "api_token"
        case image = "image"
        case user_type = "user_type"
        case source = "source"
        case verified = "verified"
        case created_at = "created_at"
        case updated_at = "updated_at"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        username = try values.decodeIfPresent(String.self, forKey: .username)
        mobile = try values.decodeIfPresent(String.self, forKey: .mobile)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        email_verified_at = try values.decodeIfPresent(String.self, forKey: .email_verified_at) ?? "0"
        api_token = try values.decodeIfPresent(String.self, forKey: .api_token) ?? "0"
        image = try values.decodeIfPresent(String.self, forKey: .image)
        user_type = try values.decodeIfPresent(Int.self, forKey: .user_type)
        source = try values.decodeIfPresent(Int.self, forKey: .source)
        verified = try values.decodeIfPresent(Int.self, forKey: .verified)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
    }
    
}
