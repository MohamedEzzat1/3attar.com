//
//  _Product_By_Cats.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 7/25/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation

struct _Product_By_Cats : Codable{
    let message : String?
    let imgs_url: String?
    let categories: [_Cats]?
    
    enum CodingKeys: String, CodingKey {
        case message = "message"
        case imgs_url = "imgs_url"
        case categories = "categories"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        imgs_url = try values.decodeIfPresent(String.self, forKey: .imgs_url)
        categories = try values.decodeIfPresent([_Cats].self, forKey: .categories)
    }
}
