//
//  _Product_By_CatsV2.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 10/15/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation

struct _Product_By_CatsV2 : Codable{
    let message : String?
    let imgs_url: String?
    let category: _Cats?
    let products: products_v2?
    
    enum CodingKeys: String, CodingKey {
        case message = "message"
        case imgs_url = "imgs_url"
        case category = "category"
        case products = "products"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        imgs_url = try values.decodeIfPresent(String.self, forKey: .imgs_url)
        category = try values.decodeIfPresent(_Cats.self, forKey: .category)
        products = try values.decodeIfPresent(products_v2.self, forKey: .products)
    }
}
