//
//  _OrderDetails_Base.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 8/22/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation

struct _OrderDetails_Base: Codable {
    let success : Bool?
    let message : String?
    let order_details : _OrdersList?
    
    enum CodingKeys : String, CodingKey  {
        case success = "success"
        case message = "message"
        case order_details  = "order_details"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        order_details  = try values.decodeIfPresent(_OrdersList.self, forKey: .order_details)
    }
}
