//
//  _OrdersList.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 8/22/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation

struct _OrdersList: Codable {
    let id: Int?
    let order_no: String?
    let user_id: Int?
    let payment_method: Int?
    let courier_id: Int?
    let address_id: Int?
    let shipping_date: String?
    let status: Int?
    let total: String?
    let notes: String?
    let order_source: Int?
    let created_at: String?
    let updated_at: String?
    let address: _user_address?
    let order_items: [_Order_Items]?
    
    enum CodingKeys : String, CodingKey  {
        case id = "id"
        case order_no = "order_no"
        case user_id = "user_id"
        case payment_method = "payment_method"
        case courier_id = "courier_id"
        case address_id = "address_id"
        case shipping_date = "shipping_date"
        case status = "status"
        case total = "total"
        case notes = "notes"
        case order_source = "order_source"
        case created_at = "created_at"
        case updated_at = "updated_at"
        case address = "address"
        case order_items = "order_items"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        order_no = try values.decodeIfPresent(String.self, forKey: .order_no)
        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
        payment_method = try values.decodeIfPresent(Int.self, forKey: .payment_method)
        courier_id = try values.decodeIfPresent(Int.self, forKey: .courier_id)
        address_id = try values.decodeIfPresent(Int.self, forKey: .address_id)
        shipping_date = try values.decodeIfPresent(String.self, forKey: .shipping_date)
        status = try values.decodeIfPresent(Int.self, forKey: .status)
        total = try values.decodeIfPresent(String.self, forKey: .total)
        notes = try values.decodeIfPresent(String.self, forKey: .notes)
        order_source = try values.decodeIfPresent(Int.self, forKey: .order_source)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        address = try values.decodeIfPresent(_user_address.self, forKey: .address)
        order_items = try values.decodeIfPresent([_Order_Items].self, forKey: .order_items)
    }
}
