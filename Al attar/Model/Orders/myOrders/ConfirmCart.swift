//
//  ConfirmCart.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 8/26/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation

struct ConfirmCart: Codable {
    let product_id : Int?
    let quantity : Int?
    let item_cost : Int?
    
    enum CodingKeys : String, CodingKey  {
        case product_id = "product_id"
        case quantity = "quantity"
        case item_cost  = "item_cost"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        product_id = try values.decodeIfPresent(Int.self, forKey: .product_id)
        quantity = try values.decodeIfPresent(Int.self, forKey: .quantity)
        item_cost  = try values.decodeIfPresent(Int.self, forKey: .item_cost)
    }
}
