//
//  Order_Items.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 8/22/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation

struct _Order_Items: Codable {
    
    let id: Int?
    let order_id: Int?
    let item_id: Int?
    let quantity: Int?
    let item_cost:String?
    let item_total:String?
    let created_at:String?
    let updated_at:String?
    let title:String?
    let image:String?
    
    enum CodingKeys : String, CodingKey  {
        case id = "id"
        case item_id = "item_id"
        case order_id  = "order_id"
        case quantity = "quantity"
        case item_cost = "item_cost"
        case item_total  = "item_total"
        case created_at = "created_at"
        case updated_at = "updated_at"
        case title  = "title"
        case image  = "image"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        item_id = try values.decodeIfPresent(Int.self, forKey: .item_id)
        order_id = try values.decodeIfPresent(Int.self, forKey: .order_id)
        quantity = try values.decodeIfPresent(Int.self, forKey: .quantity)
        item_cost = try values.decodeIfPresent(String.self, forKey: .item_cost)
        item_total = try values.decodeIfPresent(String.self, forKey: .item_total)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        image = try values.decodeIfPresent(String.self, forKey: .image)
    }
}
