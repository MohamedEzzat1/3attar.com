//
//  _Order_Base.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 8/22/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation

struct _Order_Base: Codable {
    let success : Bool?
    let message : String?
    let orders : [_OrdersList]?
    
    enum CodingKeys : String, CodingKey  {
        case success = "success"
        case message = "message"
        case orders  = "orders"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        orders  = try values.decodeIfPresent([_OrdersList].self, forKey: .orders)
    }
}
