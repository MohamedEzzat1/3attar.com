//
//  MyCartData.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 8/24/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation
import CoreData

@available(iOS 13.0, *)
class MyCartData: NSObject {
    
    class func addCartData(qty:String, productID:String, Image:String, price:String, name_ar:String, name_en:String){
        if let managedObjectContext = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext
        {
                
            let myItem = NSEntityDescription.insertNewObject(forEntityName: "CartData", into: managedObjectContext) as! CartData
                
            myItem.qty        = qty
            myItem.product_id = productID
            myItem.title_ar   = name_ar
            myItem.title_en   = name_en
            myItem.price      = price
            myItem.image      = Image
                
            do {
                try managedObjectContext.save()
                    
            } catch { // Replace this implementation with code to handle the error appropriately. // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        } else {
            // Fallback on earlier versions
        }//end of check for managed object
    }
    
    class func getCartData()->(Array<Any>, String, String, String, String, String, String){
        var CartDataArr:[CartData] = []
        var qty       = ""
        var productID = ""
        var image     = ""
        var name_en   = ""
        var name_ar   = ""
        var price     = ""
        if let managedObjectContext = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CartData")
            
            // Add Sort Descriptor
            
            let sortDescriptorQty = NSSortDescriptor(key: "qty", ascending: true)
            fetchRequest.sortDescriptors = [sortDescriptorQty]
            
            let sortDescriptorID = NSSortDescriptor(key: "productID", ascending: true)
            fetchRequest.sortDescriptors = [sortDescriptorID]
            
            let sortDescriptorprice = NSSortDescriptor(key: "price", ascending: true)
            fetchRequest.sortDescriptors = [sortDescriptorprice]
            
            let sortDescriptortitleen = NSSortDescriptor(key: "title_en", ascending: true)
            fetchRequest.sortDescriptors = [sortDescriptortitleen]
            
            let sortDescriptortitlear = NSSortDescriptor(key: "title_ar", ascending: true)
            fetchRequest.sortDescriptors = [sortDescriptortitlear]
            
            let sortDescriptorimage = NSSortDescriptor(key: "image", ascending: true)
            fetchRequest.sortDescriptors = [sortDescriptorimage]
            
            
            do {
                CartDataArr = try managedObjectContext.fetch(CartData.fetchRequest()) as! [CartData]
                for i in CartDataArr{
                    qty       = i.qty!
                    productID = i.product_id!
                    name_en   = i.title_en!
                    name_ar   = i.title_ar!
                    price     = i.price!
                    image     = i.image!
                }
            } catch {
                print("Failed to retrieve record")
                print(error)
            }
        }
        return (CartDataArr, productID, qty, name_en, name_ar, image, price)
    }
    
    class func deleteCartData(){ // Load menu items from database
        if let managedObjectContext = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext {
            let user = NSFetchRequest<NSFetchRequestResult>(entityName: "CartData")
            let deleteRequest = NSBatchDeleteRequest(fetchRequest: user)
            
            do {
                try managedObjectContext.execute(deleteRequest)
                print(self.getCartData().0)
            } catch {
                let saveError = error as NSError
                print(saveError)
            }
        }
    }
    
    class func deleteObject(cart: CartData){
        let context = getContext()
        context.delete(cart)
        do {
            try context.save()
        }catch{
            
        }
    }
    
    
    class func updateQTYCart(cart:CartData, qty:String){
        let appDel: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
        let managedObjectContext = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext
        let fetchRequest:NSFetchRequest = CartData.fetchRequest()
        do{
            
            if let fetchResults = try appDel.persistentContainer.viewContext.fetch(fetchRequest) as? [NSManagedObject] {
                if fetchResults.count != 0{
                    
                    let user = cart as NSManagedObject
                    user.setValue(qty, forKey: "qty")
                    
                    do {
                        try managedObjectContext?.save()
                        
                    }catch {
                        let saveError = error as NSError
                        print(saveError)
                    }
                    
                }
            }
        }
            
        catch {
            
        }
    }
    
    class func updateCart(cart:CartData, qty:String, productID:String, Image:String, price:String, name_ar:String, name_en:String){
        let appDel: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
        let managedObjectContext = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext
        let fetchRequest:NSFetchRequest = CartData.fetchRequest()
        do{
            
            if let fetchResults = try appDel.persistentContainer.viewContext.fetch(fetchRequest) as? [NSManagedObject] {
                if fetchResults.count != 0{
                    
                    let user = cart as NSManagedObject
                    user.setValue(qty, forKey: "qty")
                    user.setValue(productID, forKey: "product_id")
                    user.setValue(name_ar, forKey: "title_ar")
                    user.setValue(name_en, forKey: "title_en")
                    user.setValue(Image, forKey: "image")
                    user.setValue(price, forKey: "price")
                    
                    do {
                        try managedObjectContext?.save()
                        
                    }catch {
                        let saveError = error as NSError
                        print(saveError)
                    }
                    
                }
            }
        }
            
        catch {
            
        }
    }
    
    class func getContext () -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
}
