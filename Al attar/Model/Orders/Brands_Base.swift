//
//  Brands_Base.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 9/4/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation

struct Brands_Base: Codable {
    let message: String?
    let imgs_url: String?
    let brands: [_Brand_Data]?
    
    enum CodingKeys: String, CodingKey {
        case message = "message"
        case imgs_url = "imgs_url"
        case brands = "brands"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        imgs_url = try values.decodeIfPresent(String.self, forKey: .imgs_url)
        brands = try values.decodeIfPresent([_Brand_Data].self, forKey: .brands)
    }
}
