//
//  article_Base.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 8/21/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation

struct article_Base: Codable {
    
    let id: Int?
    let title_ar: String?
    let title_en: String?
    let description_ar: String?
    let description_en: String?
    let short_description_ar: String?
    let short_description_en: String?
    let image: String?
    let slug_ar: String?
    let slug_en: String?
//    "category_id": 3,
    let brand_id: Int?
//    "seller_id": 0,
//    "no": 301147093,
//    "sale_price": null,
    let regular_price: String?
//    "cost": "0.00",
//    "msrp": "0",
//    "sku": null,
//    "ean": null,
//    "url": "",
//    "uri": "G71AMLKa8yOizg3l",
//    "tax_status": "taxable",
//    "quantity": "11",
//    "max_quantity": "1",
//    "brand_order": "0",
//    "home_order": "0",
//    "category_order": "0",
//    "type": 0,
//    "meta_author": "رقائق الخبز المشبعة باللوز والشيا 100 جرام | من منتجات الشيف سالى فؤاد",
//    "meta_description": "اشترى رقائق الخبز المشبعة باللوز والشيا",
//    "meta_keywords": null,
//    "featured": 0,
//    "best_selling": null,
//    "new_arrival": null,
//    "published": 1,
//    "date_sale_price_start": "2020-08-13 19:49:31",
//    "date_sale_price_end": "2020-08-13 19:49:31",
//    "created_at": "2020-08-13 19:49:32",
//    "updated_at":
    

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case title_en = "title_en"
        case title_ar = "title_ar"
        case description_ar = "description_ar"
        case description_en = "description_en"
        case short_description_ar = "short_description_ar"
        case short_description_en = "short_description_en"
        case image = "image"
        case slug_ar = "slug_ar"
        case slug_en = "slug_en"
        case regular_price = "regular_price"
        case brand_id = "brand_id"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        title_ar = try values.decodeIfPresent(String.self, forKey: .title_ar)
        title_en = try values.decodeIfPresent(String.self, forKey: .title_en)
        description_ar = try values.decodeIfPresent(String.self, forKey: .description_ar)
        description_en = try values.decodeIfPresent(String.self, forKey: .description_en)
        short_description_ar = try values.decodeIfPresent(String.self, forKey: .short_description_ar)
        short_description_en = try values.decodeIfPresent(String.self, forKey: .short_description_en)
        image = try values.decodeIfPresent(String.self, forKey: .image)
        slug_ar = try values.decodeIfPresent(String.self, forKey: .slug_ar)
        slug_en = try values.decodeIfPresent(String.self, forKey: .slug_en)
        regular_price = try values.decodeIfPresent(String.self, forKey: .regular_price)
        brand_id = try values.decodeIfPresent(Int.self, forKey: .brand_id)
    }
}

