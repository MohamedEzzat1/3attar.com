//
//  CartData.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 8/24/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation
import CoreData

class CartData: NSManagedObject {
    @NSManaged var qty:String?
    @NSManaged var product_id:String?
    @NSManaged var image:String?
    @NSManaged var title_ar:String?
    @NSManaged var title_en:String?
    @NSManaged var price:String?
}
