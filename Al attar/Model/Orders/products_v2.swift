//
//  products_v2.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 10/15/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation

struct products_v2 : Codable{
    let first_page_url: String?
    let from: Int?
    let last_page: Int?
    let last_page_url: String?
    let next_page_url: String?
    let path: String?
    let per_page: String?
//    let prev_page_url:
    let to: Int?
    let total: Int?
    let current_page: Int?
    let data: [_Products_Base]?
    
    enum CodingKeys: String, CodingKey {
        case first_page_url = "first_page_url"
        case from = "from"
        case last_page = "last_page"
        case last_page_url = "last_page_url"
        case next_page_url = "next_page_url"
        case path = "path"
        case per_page = "per_page"
    //    let prev_page_url:
        case to = "to"
        case total = "total"
        case current_page = "current_page"
        case data = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        first_page_url = try values.decodeIfPresent(String.self, forKey: .first_page_url)
        from = try values.decodeIfPresent(Int.self, forKey: .from)
        to = try values.decodeIfPresent(Int.self, forKey: .to)
        total = try values.decodeIfPresent(Int.self, forKey: .total)
        last_page = try values.decodeIfPresent(Int.self, forKey: .last_page)
        current_page = try values.decodeIfPresent(Int.self, forKey: .current_page)
        last_page_url = try values.decodeIfPresent(String.self, forKey: .last_page_url)
        next_page_url = try values.decodeIfPresent(String.self, forKey: .next_page_url)
        per_page = try values.decodeIfPresent(String.self, forKey: .per_page)
        path = try values.decodeIfPresent(String.self, forKey: .path)
        data = try values.decodeIfPresent([_Products_Base].self, forKey: .data)
    }
}
