//
//  CouponList.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 9/10/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation

struct CouponList: Codable {
    let id: Int?
    let title: String?
    let description: String?
    let code: String?
    let type: String?
    let value: Int?
    let max_discount: Int?
    let min_basket: Int?
    let count_per_user: Int?
//    let count": null,
    let start_date: String?
    let end_date: String?
    let active: Int?
//    let created_at: String?
//    let updated_at: String?
    
    enum CodingKeys : String, CodingKey  {
        case id = "id"
        case title = "title"
        case description  = "description"
        case code = "code"
        case type = "type"
        case value  = "value"
        case max_discount = "max_discount"
        case min_basket = "min_basket"
        case count_per_user = "count_per_user"
        case start_date  = "start_date"
        case end_date = "end_date"
        case active = "active"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id  = try values.decodeIfPresent(Int.self, forKey: .id)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        code  = try values.decodeIfPresent(String.self, forKey: .code)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        value = try values.decodeIfPresent(Int.self, forKey: .value)
        max_discount  = try values.decodeIfPresent(Int.self, forKey: .max_discount)
        min_basket = try values.decodeIfPresent(Int.self, forKey: .min_basket)
        count_per_user = try values.decodeIfPresent(Int.self, forKey: .count_per_user)
        start_date = try values.decodeIfPresent(String.self, forKey: .start_date)
        end_date  = try values.decodeIfPresent(String.self, forKey: .end_date)
        active  = try values.decodeIfPresent(Int.self, forKey: .active)
        
    }
}

