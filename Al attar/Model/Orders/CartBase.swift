//
//  CartBase.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 9/15/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation

class CartBase:Codable {

    var name_ar: String?
    var name_en: String?
    var image: String?
    var product_id: Int?
    var item_cost: String?
    var quantity: Int?

    init(name_ar : String, product_id : Int, name_en: String, quantity : Int, item_cost: String, image: String) {
        self.image = image
        self.name_ar = name_ar
        self.name_en = name_en
        self.item_cost = item_cost
        self.product_id = product_id
        self.quantity = quantity

    }
}
