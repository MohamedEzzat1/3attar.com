//
//  _Product_By_BrandsDL.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 10/16/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation

struct _Product_By_BrandsDL : Codable{
    let message : String?
    let imgs_url: String?
    let brand: _Brand_Data?
    let products: products_v2?
    
    enum CodingKeys: String, CodingKey {
        case message = "message"
        case imgs_url = "imgs_url"
        case brand = "brand"
        case products = "products"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        imgs_url = try values.decodeIfPresent(String.self, forKey: .imgs_url)
        brand = try values.decodeIfPresent(_Brand_Data.self, forKey: .brand)
        products = try values.decodeIfPresent(products_v2.self, forKey: .products)
    }
}
