//
//  HomeProductDetails.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 11/15/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation

struct HomeProductDetails: Codable {
    let message: String?
    let imgs_url: String?
    let product_images: String?
    let article: [article_Base]?
    
    enum CodingKeys: String, CodingKey {
        case message = "message"
        case imgs_url = "imgs_url"
        case product_images = "product_images"
        case article = "article"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        imgs_url = try values.decodeIfPresent(String.self, forKey: .imgs_url)
        product_images = try values.decodeIfPresent(String.self, forKey: .product_images)
        article = try values.decodeIfPresent([article_Base].self, forKey: .article)
    }
}
