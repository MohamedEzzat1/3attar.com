//
//  _Wishes.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 7/27/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation

struct _Wishes: Codable {
    let message: String?
    let success: Bool?
    let wishlist: [_wishlist]?
    
    enum CodingKeys: String, CodingKey {
        case message  = "message"
        case success  = "success"
        case wishlist = "wishlist"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
        wishlist = try values.decodeIfPresent([_wishlist].self, forKey: .wishlist)
    }
    
}
