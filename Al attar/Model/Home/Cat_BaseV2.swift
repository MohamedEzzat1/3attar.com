//
//  Cat_BaseV2.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 10/15/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation

struct Cat_BaseV2 : Codable {
    let message : String?
    let imgs_url : String?
    let category : _Cats?
    
    enum CodingKeys : String, CodingKey {
        case message    = "message"
        case imgs_url   = "imgs_url"
        case category = "category"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        imgs_url = try values.decodeIfPresent(String.self, forKey: .imgs_url)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        category = try values.decodeIfPresent(_Cats.self, forKey: .category)
    }
    
}
