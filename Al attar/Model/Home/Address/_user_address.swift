//
//  user_address.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 8/21/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation

struct _user_address:Codable {
    let id:Int?
    let user_id:Int?
    let title:String?
    let address1:String?
    let address2:String?
    let city:String?
    let state:Int?
    let zip_code:Int?
    let primary:Int?
    let created_at:String?
    let updated_at:String?
    let shipping_cost: Shipping_Cost?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case user_id = "user_id"
        case title = "title"
        case address1 = "address1"
        case address2 = "address2"
        case city = "city"
        case state = "state"
        case zip_code = "zip_code"
        case primary = "primary"
        case created_at = "created_at"
        case updated_at = "updated_at"
        case shipping_cost = "shipping_cost"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        address1 = try values.decodeIfPresent(String.self, forKey: .address1)
        address2 = try values.decodeIfPresent(String.self, forKey: .address2)
        city = try values.decodeIfPresent(String.self, forKey: .city)
        state = try values.decodeIfPresent(Int.self, forKey: .state)
        primary = try values.decodeIfPresent(Int.self, forKey: .primary)
        zip_code = try values.decodeIfPresent(Int.self, forKey: .zip_code)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        shipping_cost = try values.decodeIfPresent(Shipping_Cost.self, forKey: .shipping_cost)
    }
}
