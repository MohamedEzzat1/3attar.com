//
//  _Shipping_Zones.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 8/22/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation

struct _Shipping_Base:Codable {
    let success: Bool?
    let message: String?
    let shipping_zones: [_ShippingZones]?
    
    enum CodingKeys: String, CodingKey {
        case success = "success"
        case message = "message"
        case shipping_zones = "shipping_zones"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        shipping_zones = try values.decodeIfPresent([_ShippingZones].self, forKey: .shipping_zones)
    }
}
