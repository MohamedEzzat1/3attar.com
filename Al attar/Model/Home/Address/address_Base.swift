//
//  address_Base.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 8/21/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation

struct address_Base:Codable {
    let success: Bool?
    let message: String?
    let user_address: [_user_address]?
    
    enum CodingKeys: String, CodingKey {
        case success = "success"
        case message = "message"
        case user_address = "user_address"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        user_address = try values.decodeIfPresent([_user_address].self, forKey: .user_address)
    }
}


