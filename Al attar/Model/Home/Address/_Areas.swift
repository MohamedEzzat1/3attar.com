//
//  _Areas.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 8/22/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation

struct _Areas:Codable {
    let id: Int?
    let title: String?
    let zone_id: Int?
    let cost: String?
    let published: Int?
    let created_at: String?
    let updated_at: String?
   
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case title = "title"
        case zone_id = "zone_id"
        case cost = "cost"
        case published = "published"
        case created_at = "created_at"
        case updated_at = "updated_at"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        zone_id = try values.decodeIfPresent(Int.self, forKey: .zone_id)
        cost = try values.decodeIfPresent(String.self, forKey: .cost)
        published = try values.decodeIfPresent(Int.self, forKey: .published)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        
    }
}
