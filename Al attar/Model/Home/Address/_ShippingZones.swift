//
//  _ShippingZones.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 8/22/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation

struct _ShippingZones:Codable {
    let id: Int?
    let title: String?
    let published: Int?
    let created_at: String?
    let updated_at: String?
    let areas: [_Areas]?
   
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case title = "title"
        case published = "published"
        case created_at = "created_at"
        case updated_at = "updated_at"
        case areas = "areas"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        published = try values.decodeIfPresent(Int.self, forKey: .published)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        areas = try values.decodeIfPresent([_Areas].self, forKey: .areas)
    }
}
