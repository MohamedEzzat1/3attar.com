//
//  _Home_List.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 11/15/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation

struct _Home_List: Codable {
    let id: Int?
    let title: String?
    let type: Int?
    let category_id: Int?
    let banner_id: Int?
    let published: Int?
//    let created_at: Bool?
//    let updated_at: [_wishlist]?
    
    enum CodingKeys: String, CodingKey {
        case id  = "id"
        case title  = "title"
        case type = "type"
        case category_id  = "category_id"
        case banner_id  = "banner_id"
        case published = "published"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        type = try values.decodeIfPresent(Int.self, forKey: .type)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        category_id = try values.decodeIfPresent(Int.self, forKey: .category_id)
        banner_id = try values.decodeIfPresent(Int.self, forKey: .banner_id)
        published = try values.decodeIfPresent(Int.self, forKey: .published)
    }
    
}
