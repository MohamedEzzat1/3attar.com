//
//  _Home_List_Base.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 11/15/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation

struct _Home_List_Base: Codable {
    let message: String?
    let app_list: [_Home_List]?
    
    enum CodingKeys: String, CodingKey {
        case message  = "message"
        case app_list  = "app_list"

    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        app_list = try values.decodeIfPresent([_Home_List].self, forKey: .app_list)
    }
   
}
