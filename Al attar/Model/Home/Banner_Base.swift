//
//  Banner_Base.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 11/16/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation

struct Banner_Base: Codable{
    let message : String?
    let banner : [Sub_Banner]?
    let imgs_url : String?

    enum CodingKeys : String, CodingKey  {
        case imgs_url = "imgs_url"
        case banner = "banner"
        case message = "message"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        banner = try values.decodeIfPresent([Sub_Banner].self, forKey: .banner)
        imgs_url = try values.decodeIfPresent(String.self, forKey: .imgs_url)
        message = try values.decodeIfPresent(String.self, forKey: .message)
    }

}

