//
//  Search_Base.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 9/8/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation

struct Search_Base: Codable {
    let message: String?
    let imgs_url: String?
    let products: [_Products_Base]?
    let product_images: String?
    
    enum CodingKeys: String, CodingKey {
        case message  = "message"
        case imgs_url  = "imgs_url"
        case products = "products"
        case product_images = "product_images"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        imgs_url = try values.decodeIfPresent(String.self, forKey: .imgs_url)
        products = try values.decodeIfPresent([_Products_Base].self, forKey: .products)
        product_images = try values.decodeIfPresent(String.self, forKey: .product_images)
    }
    
}
