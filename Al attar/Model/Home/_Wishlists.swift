//
//  _Wishlists.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 9/5/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation

struct _Wishlists : Codable {
    let id: Int?
    let title_ar: String?
    let title_en: String?
    let description_ar: String?
    let description_en: String?
    let short_description_ar: String?
    let short_description_en: String?
    let image: String?
    let slug_ar: String?
    let slug_en: String?
    let category_id: Int?
    let brand_id: Int?
    let seller_id: Int?
    let no: Int?

    let sale_price: String?
    let regular_price: String?
    let cost: String? // not sure int or string
//    let msrp: String?
//    let sku: String?
//    let ean: String?
//    let url: String?
//    let uri: String?
//    let tax_status: String?
//    let quantity: String?
//    let max_quantity: String?
//
//    let brand_order: String?
//    let home_order: String?
//    let category_order: String?
//    let type: Int?
//    let meta_author: String?
//    let meta_description: String?
//    let meta_keywords: Int? // not sure int or string
//    let featured: Int?
//
//    let best_selling: Int?
//    let new_arrival: Int? // not sure int or string
//
//    let published: Int?
//    let date_sale_price_start: String?
//    let date_sale_price_end: String?
//    let created_at: String?
//    let updated_at: String?
    
    enum CodingKeys : String, CodingKey  {
        case id = "id"
        case title_ar = "title_ar"
        case title_en = "title_en"
        case description_ar = "description_ar"
        case description_en = "description_en"
        case short_description_ar = "short_description_ar"
        case short_description_en = "short_description_en"
        case image = "image"
        case slug_ar = "slug_ar"
        case slug_en = "slug_en"
        case category_id = "category_id"
        case brand_id = "brand_id"
        case seller_id = "seller_id"
        case no = "no"
        case sale_price = "sale_price"
        case regular_price = "regular_price"
        case cost = "cost"
//        case msrp = "msrp"
//        case sku = "sku"
//        case ean = "ean"
//        case url = "url"
//        case uri = "uri"
//        case tax_status = "tax_status"
//        case quantity = "quantity"
//        case max_quantity = "max_quantity"
//        case brand_order = "brand_order"
//        case home_order = "home_order"
//        case category_order = "category_order"
//        case type = "type"
//        case meta_author = "meta_author"
//        case meta_description = "meta_description"
//        case meta_keywords = "meta_keywords"
//        case featured = "featured"
//        case best_selling = "best_selling"
//        case new_arrival = "new_arrival"
//        case published = "published"
//        case date_sale_price_start = "date_sale_price_start"
//        case date_sale_price_end = "date_sale_price_end"
//        case created_at = "created_at"
//        case updated_at = "updated_at"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id) ?? 0
        title_ar = try values.decodeIfPresent(String.self, forKey: .title_ar) ?? "0"
        title_en = try values.decodeIfPresent(String.self, forKey: .title_en) ?? "0"
        description_ar = try values.decodeIfPresent(String.self, forKey: .description_ar) ?? "0"
        description_en = try values.decodeIfPresent(String.self, forKey: .description_en) ?? "0"
        short_description_ar = try values.decodeIfPresent(String.self, forKey: .short_description_ar) ?? "0"
        short_description_en = try values.decodeIfPresent(String.self, forKey: .short_description_en) ?? "0"
        image = try values.decodeIfPresent(String.self, forKey: .image) ?? "0"
        slug_ar = try values.decodeIfPresent(String.self, forKey: .slug_ar) ?? "0"
        slug_en = try values.decodeIfPresent(String.self, forKey: .slug_en) ?? "0"
        category_id = try values.decodeIfPresent(Int.self, forKey: .category_id) ?? 0
        brand_id = try values.decodeIfPresent(Int.self, forKey: .brand_id) ?? 0
        seller_id = try values.decodeIfPresent(Int.self, forKey: .seller_id) ?? 0
        no = try values.decodeIfPresent(Int.self, forKey: .no) ?? 0
        sale_price = try values.decodeIfPresent(String.self, forKey: .sale_price) ?? "0.0"
        regular_price = try values.decodeIfPresent(String.self, forKey: .regular_price) ?? "0.0"
        cost = try values.decodeIfPresent(String.self, forKey: .cost) ?? "0.0"
//         msrp = try values.decodeIfPresent(String.self, forKey: .msrp) ?? "0"
//         sku = try values.decodeIfPresent(String.self, forKey: .sku) ?? "0"
//         ean = try values.decodeIfPresent(String.self, forKey: .ean) ?? "0"
//         url = try values.decodeIfPresent(String.self, forKey: .url) ?? "0"
//         uri = try values.decodeIfPresent(String.self, forKey: .uri) ?? "0"
//         tax_status = try values.decodeIfPresent(String.self, forKey: .tax_status) ?? "0"
//         quantity = try values.decodeIfPresent(String.self, forKey: .quantity) ?? "0"
//         max_quantity = try values.decodeIfPresent(String.self, forKey: .max_quantity) ?? "0"
//         brand_order = try values.decodeIfPresent(String.self, forKey: .brand_id) ?? "0"
//         home_order = try values.decodeIfPresent(String.self, forKey: .home_order) ?? "0"
//         category_order = try values.decodeIfPresent(String.self, forKey: .category_id) ?? "0"
//         type = try values.decodeIfPresent(Int.self, forKey: .type) ?? 0
//         meta_author = try values.decodeIfPresent(String.self, forKey: .meta_author) ?? "0"
//         meta_description = try values.decodeIfPresent(String.self, forKey: .meta_description) ?? "0"
//         meta_keywords = try values.decodeIfPresent(Int.self, forKey: .meta_keywords) ?? 0
//         featured = try values.decodeIfPresent(Int.self, forKey: .featured) ?? 0
//         best_selling = try values.decodeIfPresent(Int.self, forKey: .best_selling) ?? 0
//         new_arrival = try values.decodeIfPresent(Int.self, forKey: .new_arrival) ?? 0
//         published = try values.decodeIfPresent(Int.self, forKey: .published) ?? 0
//         date_sale_price_start = try values.decodeIfPresent(String.self, forKey: .date_sale_price_start) ?? "0"
//         date_sale_price_end = try values.decodeIfPresent(String.self, forKey: .date_sale_price_end) ?? "0"
//         created_at = try values.decodeIfPresent(String.self, forKey: .created_at) ?? "0"
//         updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at) ?? "0"
    }
    
}
