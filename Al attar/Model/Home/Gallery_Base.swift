//
//  Gallery_Base.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 7/23/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation

struct Gallery_Base: Codable{
    let message : String?
    let gallery : [Sub_Gallery]?
    let imgs_url : String?

    enum CodingKeys : String, CodingKey  {
        case imgs_url = "imgs_url"
        case gallery = "gallery"
        case message = "message"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        gallery = try values.decodeIfPresent([Sub_Gallery].self, forKey: .gallery)
        imgs_url = try values.decodeIfPresent(String.self, forKey: .imgs_url)
        message = try values.decodeIfPresent(String.self, forKey: .message)
    }

}

