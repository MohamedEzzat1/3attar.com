//
//  _wishlist.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 8/23/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation

struct _wishlist: Codable {
    let created_at: String?
    let id: Int?
    let product_id: Int?
    let updated_at: String?
    let user_id: Int?
    let user_wishlist_items_a_p_i: [_Wishlists]?
    
    enum CodingKeys: String, CodingKey {
        case created_at  = "created_at"
        case id  = "id"
        case product_id = "product_id"
        case updated_at  = "updated_at"
        case user_id  = "user_id"
        case user_wishlist_items_a_p_i = "user_wishlist_items_a_p_i"
        
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        product_id = try values.decodeIfPresent(Int.self, forKey: .product_id)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
        user_wishlist_items_a_p_i = try values.decodeIfPresent([_Wishlists].self, forKey: .user_wishlist_items_a_p_i)
    }
    
}
