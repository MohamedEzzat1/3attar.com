//
//  _Cats.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 7/23/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation

struct _Cats : Codable {
    
//    let data: [_Category_Data]?
//    let current_page: Int?
//    let first_page_url: String?
//    let from: Int?
//    let last_page: Int?
//    let last_page_url:String?
//    let next_page_url:String?
//    let path:String?
//    let per_page:String?
//    let prev_page_url:String?
//    let to:Int?
//    let total:Int?
    
    let id: Int?
    let title_ar: String?
    let title_en: String?
    let description_ar: String?
    let description_en: String?
    let slug_ar: String?
    let slug_en: String?
    let parent_id: Int?
    let no: Int?
    let uri: String?
    let image: String?
    let banner_image: String?
    let meta_author: String?
    let meta_description: String?
    let meta_keywords: String?
    let published: Int?
    let featured: Int?
    let created_at: String?
    let updated_at: String?
    let products: [_Products_Base]?
    let sub_category: [_Cats]?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case title_ar = "title_ar"
        case title_en = "title_en"
        case description_ar = "description_ar"
        case description_en = "description_en"
        case slug_ar = "slug_ar"
        case slug_en = "slug_en"
        case parent_id = "parent_id"
        case no = "no"
        case uri = "uri"
        case image = "image"
        case banner_image = "banner_image"
        case meta_author = "meta_author"
        case meta_description = "meta_description"
        case meta_keywords = "meta_keywords"
        case published = "published"
        case featured = "featured"
        case created_at = "created_at"
        case updated_at = "updated_at"
        case products = "products"
        case sub_category = "sub_category"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        title_ar = try values.decodeIfPresent(String.self, forKey: .title_ar)
        title_en = try values.decodeIfPresent(String.self, forKey: .title_en)
        description_ar = try values.decodeIfPresent(String.self, forKey: .description_ar)
        description_en = try values.decodeIfPresent(String.self, forKey: .description_en)
        slug_ar = try values.decodeIfPresent(String.self, forKey: .slug_ar)
        slug_en = try values.decodeIfPresent(String.self, forKey: .slug_en)
        parent_id = try values.decodeIfPresent(Int.self, forKey: .parent_id)
        no = try values.decodeIfPresent(Int.self, forKey: .no)
        uri = try values.decodeIfPresent(String.self, forKey: .uri)
        image = try values.decodeIfPresent(String.self, forKey: .image)
        banner_image = try values.decodeIfPresent(String.self, forKey: .banner_image)
        meta_author = try values.decodeIfPresent(String.self, forKey: .meta_author)
        meta_description = try values.decodeIfPresent(String.self, forKey: .meta_description)
        meta_keywords = try values.decodeIfPresent(String.self, forKey: .meta_keywords)
        published = try values.decodeIfPresent(Int.self, forKey: .published)
        featured = try values.decodeIfPresent(Int.self, forKey: .featured)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        products = try values.decodeIfPresent([_Products_Base].self, forKey: .products)
        sub_category = try values.decodeIfPresent([_Cats].self, forKey: .sub_category)
    }
}
