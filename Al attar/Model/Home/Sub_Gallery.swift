//
//  Sub_Gallery.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 7/23/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation

struct Sub_Gallery: Codable {
    let id : Int?
    let title : String?
    let img : String?
    let img_head : String?
    let img_desc : String?
    let imgs_url : String?
    let img_title : String?
    let img_alt : String?
    let img_url : String?
    let no : Int?
    let position : Int?
    let active_url : Int?
    let slider : Int?
    let active: Int?
    let created_at : String?
    let updated_at : String?

    enum CodingKeys : String, CodingKey  {
        case id = "id"
        case title = "title"
        case img = "img"
        case img_head = "img_head"
        case img_desc = "img_desc"
        case imgs_url = "imgs_url"
        case img_url = "img_url"
        case img_title = "img_title"
        case no = "no"
        case img_alt = "img_alt"
        case position = "position"
        case active_url = "active_url"
        case slider = "slider"
        case active = "active"
        case created_at = "created_at"
        case updated_at = "updated_at"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        img = try values.decodeIfPresent(String.self, forKey: .img)
        img_head = try values.decodeIfPresent(String.self, forKey: .img_head)
        img_desc = try values.decodeIfPresent(String.self, forKey: .img_desc)
        imgs_url = try values.decodeIfPresent(String.self, forKey: .imgs_url)
        img_url = try values.decodeIfPresent(String.self, forKey: .img_url)
        img_title = try values.decodeIfPresent(String.self, forKey: .img_title)
        no = try values.decodeIfPresent(Int.self, forKey: .no)
        img_alt = try values.decodeIfPresent(String.self, forKey: .img_alt)
        position = try values.decodeIfPresent(Int.self, forKey: .position)
        active_url = try values.decodeIfPresent(Int.self, forKey: .active_url)
        slider = try values.decodeIfPresent(Int.self, forKey: .slider)
        active = try values.decodeIfPresent(Int.self, forKey: .active)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
    }
}
