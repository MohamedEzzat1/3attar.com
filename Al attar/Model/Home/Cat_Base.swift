//
//  Cat_Base.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 7/23/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation

struct Cat_Base : Codable {
    let message : String?
    let imgs_url : String?
    let categories : [_Cats]?
    
    enum CodingKeys : String, CodingKey {
        case message    = "message"
        case imgs_url   = "imgs_url"
        case categories = "categories"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        imgs_url = try values.decodeIfPresent(String.self, forKey: .imgs_url)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        categories = try values.decodeIfPresent([_Cats].self, forKey: .categories)
    }
    
}
