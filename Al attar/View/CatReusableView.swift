//
//  CatReusableView.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 9/4/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import UIKit

class CatReusableView: UICollectionReusableView {
    @IBOutlet weak var img: UIImageViewX!
    @IBOutlet weak var catTitle: UILabel!
    @IBOutlet weak var filterBtn: UIButton!
    
}
