//
//  ConfirmationAllTableEx.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 10/14/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation


@available(iOS 13.0, *)
class TableConfirmCell: UITableViewCell, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var orderslbl: UILabel!
    @IBOutlet weak var tbl: UITableView!
    
    var CartArr = MyCartData.getCartData().0 as! [CartData]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.CartArr.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CartCell", for: indexPath) as! CartCell
        
        cell.productid = CartArr[indexPath.row].product_id ?? "0"
//        cell.counterBtn.setTitle(String(describing:(CartArr[indexPath.row].qty) ?? "0") , for: .normal)
        cell.cellimg = CartArr[indexPath.row].image ?? ""
        cell.cellPrice = CartArr[indexPath.row].price ?? ""
        cell.celltitle_ar = CartArr[indexPath.row].title_ar ?? ""
            cell.celltitle_en = CartArr[indexPath.row].title_en ?? ""
            
        let qtty  = CartArr[indexPath.row].qty
        let price = CartArr[indexPath.row].price
        let intPrice = Double(price!)
        let intqtty = Double(qtty!)
        cell.priceint = intPrice ?? 0.0
        cell.qtyint   = intqtty ?? 0.0
        let total = (intPrice ?? 0.0) * (intqtty ?? 0.0)
        
        cell.priceLbl.text = String(total)
        cell.currencyLbl.setText(forKey: "le")
        cell.qtyCounter.text = CartArr[indexPath.row].qty!
        if LanguageHelper.getCurrentLanguage() == "ar"{
            cell.titleLbl.text = CartArr[indexPath.row].title_ar
            cell.ttl2Lbl.text  = "الكمية: " + CartArr[indexPath.row].qty!
        }else{
            cell.titleLbl.text = CartArr[indexPath.row].title_en
            cell.ttl2Lbl.text = "QTY: " + CartArr[indexPath.row].qty!
        }
                    
        let img = CartArr[indexPath.row].image
        let u = URL(string: img as? String ?? "")
        cell.img.kf.setImage(with: u)
        if cell.img.image == nil{
            cell.img.image = UIImage(named: "logo_splash_screen")
        }
        tblid = indexPath.row
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
