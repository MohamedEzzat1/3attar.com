//
//  ProductDetailsCell.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 10/23/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import UIKit

class ProductDetailsCell: UITableViewCell {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var ratedesc: UITextViewX!
    @IBOutlet weak var sendbtn: UIButton!
    @IBOutlet weak var addtoCart: UIButtonX!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func sendBtn(_ sender: Any) {
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
