//
//  ProfileCell.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 7/24/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import UIKit

class ProfileCell: UITableViewCell {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var infoLbl: UILabel!
    @IBOutlet weak var arrowLbl: UILabel!
    @IBOutlet weak var soon: UILabel!
    
    var id: Int?
    override func awakeFromNib() {
        super.awakeFromNib()
        self.arrowLbl.setText(forKey: ">")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    

}
