//
//  ProductsCell.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 7/24/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import UIKit

var cartcounterglb: Int?
var cellbrandtag = 0
var pbrandid = 0
var pbrandimg = ""
@available(iOS 13.0, *)
class ProductsCell: UICollectionViewCell {
    
    @IBOutlet weak var img: UIImageViewX!
    @IBOutlet weak var prdouctName: UILabel!
    @IBOutlet weak var desc: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var addnewItem: UIButtonX!
    @IBOutlet weak var counterBtn: UIButtonX!
    @IBOutlet weak var plusBtn: UIButtonX!
    @IBOutlet weak var minusBtn: UIButtonX!
    @IBOutlet weak var greenview: UIView!
    @IBOutlet weak var greenviewheight: NSLayoutConstraint!
    @IBOutlet weak var greenviewwidth: NSLayoutConstraint!
    @IBOutlet weak var brandBtn: UIButtonX!
    @IBOutlet weak var brandImg: UIImageViewX!
    
    var cartdelegete: CartDelegate?

    
    let CartArrr = MyCartData.getCartData().0 as? [CartData]
    var cartarr = [[String:Any]]()
    var celltitle_ar = ""
    var celltitle_en = ""
    var cellimg = ""
    var cellBaseimg = ""
    var cellPrice = ""
    var celltag = 0
    var cellbrandid = 0
    var brandimage = ""
    
    override func awakeFromNib() {
        
        if let arr = def.array(forKey: "cartarr") as? [[String:Any]]{
         cartarr = arr
        }
        self.brandBtn.addTarget(self, action: #selector(brandbutton), for: .touchUpInside)
        self.addnewItem.addTarget(self, action: #selector(addcounter), for: .touchUpInside)
    }
    
    @objc func addcounter(){
       cartcounterglb = MyCartData.getCartData().0.count + 1
    }
    
    @objc func brandbutton(){
        cellbrandtag = cellbrandid
        pbrandid = self.celltag
        pbrandimg = brandimage 
    }
    
    var productid = ""
    @IBAction func dismissview(_ sender: Any) {
        self.addnewItem.isHidden = false
        UIView.animate(withDuration: 1, animations: {
            self.greenviewheight.constant = 0
            self.greenviewwidth.constant = 0
            self.greenview.isHidden = true
        })
    }
    
    @objc func removecart(){
       addnewItem.backgroundColor = UIColor(red: 203/255, green: 36/255, blue: 47/255, alpha: 1)
        let CartArr = MyCartData.getCartData().0 as! [CartData]
        for i in CartArr{
            if i.product_id == productid{
                MyCartData.deleteObject(cart: i)
                cartdelegete?.setCart(cartcount: String(describing:MyCartData.getCartData().0.count))
                self.addnewItem.isHidden = false
                UIView.animate(withDuration: 1, animations: {
                    self.greenviewheight.constant = 0
                    self.greenviewwidth.constant = 0
                    self.greenview.isHidden = true
                })
            }
                print("تم إضافة هذا الإعلان من قبل")
            }
    }
    
    @IBAction func addnew(_ sender: Any) {
        self.addnewItem.isHidden = true
        UIView.animate(withDuration: 1, animations: {
            self.greenview.isHidden = false
            self.greenviewheight.constant = self.contentView.frame.height
            self.greenviewwidth.constant = self.contentView.frame.width
            
            self.addtoCart()
//            self.addcart()
        })
    }
    
    @IBAction func plusBtn(_ sender: Any) {
        updateItem()
    }
    @IBAction func minusBtn(_ sender: Any) {
        removeitem()
    }
    
    
    @objc func updateItem(){
        var checkID = [String]()
        if let CartArr = MyCartData.getCartData().0 as? [CartData]{
            for i in CartArr{
                let idCheck = i.product_id!
                checkID.append(idCheck)
            }
            if checkID.contains(productid){
                for i in CartArr{
                    let count = Int(i.qty!)
                    if i.product_id == productid{
                        MyCartData.updateQTYCart(cart: i, qty: String(count! + 1))
                        self.counterBtn.setTitle(String(count! + 1), for: .normal)
                        print("تم إضافة هذا الإعلان من قبل")
                    }
                }
            }
        }
    }
    
    @objc func addtoCart(){
        var checkID = [String]()
        if let CartArr = MyCartData.getCartData().0 as? [CartData]{
            
            switch CartArrr?.count {
            case 0:
                MyCartData.addCartData(qty: "1", productID: productid, Image: cellimg, price: cellPrice, name_ar: celltitle_ar, name_en: celltitle_en)
                print(cellPrice, "EEE")
//                self.badgeView.isHidden = false
//                self.badgeView.setTitle(String(CartArr.count), for: .normal)
//                SharedHandler.DisAppearMessage(viewController: (UIApplication.shared.keyWindow?.rootViewController)!, message: LanguageHelper.getString(forKey: "itemadded"))
                print("تمت إضافة منتج جديد لعربة التسوق")
                
            case CartArrr?.count:
                for i in CartArr{
                    let idCheck = i.product_id!
                    checkID.append(idCheck)
                }
                if checkID.contains(productid){
                    for i in CartArr{
                        let count = Int(i.qty!)
                        if i.product_id == productid{
                            MyCartData.updateQTYCart(cart: i, qty: String(count! + 1))
                            print("تم إضافة هذا الإعلان من قبل")
                        }
                    }
                    
                }else{
                    MyCartData.addCartData(qty: "1", productID: productid, Image: cellimg, price: cellPrice, name_ar: celltitle_ar, name_en: celltitle_en)
                    
//                    SharedHandler.DisAppearMessage(viewController: (UIApplication.shared.keyWindow?.rootViewController)!, message: LanguageHelper.getString(forKey: "itemadded"))
                }
                
            default:
                return
            }
            print(MyCartData.getCartData().0.count , "DSfreger",MyCartData.getCartData().2)
            
            cartdelegete?.setCart(cartcount: String(describing:MyCartData.getCartData().0.count))
        }else{
            print("يوجد خطأ ما.")
        }
    }
    
    @objc func removeitem(){
       var checkID = [String]()
        if let CartArr = MyCartData.getCartData().0 as? [CartData]{
            for i in CartArr{
            let idCheck = i.product_id!
            checkID.append(idCheck)
            }
            if checkID.contains(productid){
                for i in CartArr{
                    let count = Int(i.qty!)
                    if i.product_id == productid{
                        if count! > 1{
                            MyCartData.updateQTYCart(cart: i, qty: String(count! - 1))
                            self.counterBtn.setTitle(String(count! - 1), for: .normal)
                        }else{
                            MyCartData.deleteObject(cart: i)
                            cartdelegete?.setCart(cartcount: String(describing:MyCartData.getCartData().0.count))
                            self.addnewItem.isHidden = false
                            UIView.animate(withDuration: 1, animations: {
                                self.greenviewheight.constant = 0
                                self.greenviewwidth.constant = 0
                                self.greenview.isHidden = true
                            })
                        }
                        print("تم إضافة هذا الإعلان من قبل")
                    }
                }
            }
        }
    }
    
}
