//
//  ConfirmDetailsCell.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 10/14/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import UIKit

class ConfirmDetailsCell: UITableViewCell {

    @IBOutlet weak var valuesLbl: UILabel!
    @IBOutlet weak var valuesCountLbl: UILabel!
    @IBOutlet weak var deliverTV: UITextViewX!
    @IBOutlet weak var totalLbl: UILabel!
    @IBOutlet weak var deliverLbl: UILabel!
    @IBOutlet weak var deliveryLabel: UILabel!
    @IBOutlet weak var deliverycost: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var totalCost: UILabel!
    @IBOutlet weak var paymentLbl: UILabel!
    @IBOutlet weak var cashBtn: UIButton!
    @IBOutlet weak var creditBtn: UIButton!
    @IBOutlet weak var confirmBtn: UIButtonX!
    @IBOutlet weak var couponLbl: UILabel!
    @IBOutlet weak var couponTF: UITextField!
//    @IBOutlet weak var couponvalueLbl: UILabel!
    @IBOutlet weak var activateBtn: UIButtonX!
    @IBOutlet weak var currency1Lbl: UILabel!
    @IBOutlet weak var curr2Lbl: UILabel!
    @IBOutlet weak var soonlbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
