//
//  CartCell.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 8/23/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import UIKit

var productID = "0"
var newcart = [[String:Any]]()
@available(iOS 13.0, *)
class CartCell: UITableViewCell {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var ttl2Lbl: UILabel!
    @IBOutlet weak var deletebtn: UIButtonX!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var addBtn: UIButtonX!
    @IBOutlet weak var minusBtn: UIButtonX!
    @IBOutlet weak var counterBtn: UIButtonX!
    @IBOutlet weak var qtyLbl: UILabel!
    @IBOutlet weak var qtyCounter: UILabel!
    @IBOutlet weak var currencyLbl: UILabel!
    @IBOutlet weak var priceView: UIViewX!
    
    let CartArrr = MyCartData.getCartData().0 as? [CartData]
    var cartarr = [[String:Any]]()
    var celltitle_ar = ""
    var celltitle_en = ""
    var cellimg = ""
    var cellBaseimg = ""
    var cellPrice = ""
    var productid = "0"
    var doubleqty = 0.0
    var doubleprice = 0.0
    var cart: CartData?
    var cartdelegete: CartDelegate?
    var pricedeleget: PriceDelegate?
    var priceint = 0.0
    var qtyint = 1.0
    var totalint = 0.0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        productID = productid
        calculatetotal()
    }
    
    func calculatetotal(){
        totalint = (priceint) * (qtyint)
        
        self.priceLbl.text = String(totalint)
    }

    @IBAction func addBtn(_ sender: Any) {
        
        updateItem()
    }
    @IBAction func minusbtn(_ sender: Any) {
        self.deleteItem()
    }
    @IBAction func deleteBtn(_ sender: Any) {
        self.removeItem()
    }
    
    func deleteItem(){
        var checkID = [String]()
        if let CartArr = MyCartData.getCartData().0 as? [CartData]{
            for i in CartArr{
            let idCheck = i.product_id!
            checkID.append(idCheck)
        }
            if checkID.contains(productid){
                for i in CartArr{
                    let count = Int(i.qty!)
                    print(count, "DASA")
                    if i.product_id == productid{
                    if count == 1{
                        MyCartData.deleteObject(cart: i)
                        cartdelegete?.setCart(cartcount: String(describing:MyCartData.getCartData().0.count))
                        print("تم إضافة هذا الإعلان من قبل")
                    }else{
                        MyCartData.updateQTYCart(cart: i, qty: String(count! - 1))
                        self.counterBtn.setTitle(String(count! - 1), for: .normal)
                        qtyint = Double(count! - 1)
                        calculatetotal()
                        pricedeleget?.setNewPrice(qty: String(count ?? 0
                                - 1))
                        self.ttl2Lbl.text = "الكمية:" + String(count! - 1)
                        }
                    }
                }
            }
        }
    }
    
    func removeItem(){
        var checkID = [String]()
        if let CartArr = MyCartData.getCartData().0 as? [CartData]{
            for i in CartArr{
            let idCheck = i.product_id!
            checkID.append(idCheck)
        }
            if checkID.contains(productid){
                for i in CartArr{
                    if i.product_id == productid{
                        MyCartData.deleteObject(cart: i)
                        cartdelegete?.setCart(cartcount: String(describing:MyCartData.getCartData().0.count))
                        print("تم إضافة هذا الإعلان من قبل")
                    }
                }
            }
        }
    }
    
    @objc func updateItem(){
        var checkID = [String]()
        if let CartArr = MyCartData.getCartData().0 as? [CartData]{
            for i in CartArr{
                let idCheck = i.product_id!
                checkID.append(idCheck)
            }
            if checkID.contains(productid){
                for i in CartArr{
                    let count = Int(i.qty!)
                    print(count, "DASA")
                    if i.product_id == productid{
                        MyCartData.updateQTYCart(cart: i, qty: String(count! + 1))
                        self.counterBtn.setTitle(String(count! + 1), for: .normal)
                        qtyint = Double(count! + 1)
                        calculatetotal()
                        pricedeleget?.setNewPrice(qty: String(count ?? 0
                            + 1))
                        self.ttl2Lbl.text = "الكمية:" + String(count! + 1)
                    }
                }
            }
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
