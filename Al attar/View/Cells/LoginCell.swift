//
//  LoginCell.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 7/24/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class LoginCell: UITableViewCell {

    @IBOutlet weak var loginLbl: UILabel!
    @IBOutlet weak var joinLbl: UILabel!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var joinBtn: UIButton!
    
    var v = ProfileVC()
    override func awakeFromNib() {
        super.awakeFromNib()
        setDefault()
    }

    @IBAction func loginBtn(_ sender: Any) {
        UIApplication.shared.keyWindow?.rootViewController?.pushVC(storyboard: "Main", viewController: "LoginVC")
    }
    @IBAction func joinBtn(_ sender: Any) {
        UIApplication.shared.keyWindow?.rootViewController?.pushVC(storyboard: "Main", viewController: "SignupVC")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setDefault(){
        loginLbl.setText(forKey: "login")
        joinLbl.setText(forKey: "join")
    }

}
