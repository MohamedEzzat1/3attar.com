//
//  FilterCell.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 9/4/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import UIKit

class FilterCell: UICollectionViewCell {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var checkview: UIViewX!
    var id: Int?
    
    func selectCell(){
        checkview.borderColor = UIColor(red: 127/255, green: 216/255, blue: 190/255, alpha: 1)
    }
    
    func unselectCell(){
        checkview.borderColor = UIColor(red: 229/255, green: 229/255, blue: 229/255, alpha: 1)
    }
}

