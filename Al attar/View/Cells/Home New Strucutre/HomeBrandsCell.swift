//
//  HomeBrandsCell.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 11/12/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import UIKit

class HomeBrandsCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {
    @IBOutlet weak var collection: UICollectionView!
    
    var brandsource: Brands_Base?
    var branddelegate: OpenBrandsDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return brandsource?.brands?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "homebrandCell", for: indexPath) as! homebrandCell
        let url = URL(string: (self.brandsource?.imgs_url)! + "/" + (self.brandsource?.brands?[indexPath.row].image)! )
        if LanguageHelper.getCurrentLanguage() == "ar"{
//            cell.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        }
        
        cell.img.kf.setImage(with: url)
        if cell.img.image == nil{
        cell.img.image = UIImage(named: "logo_splash_screen")
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        branddelegate?.openBrands(brandid: self.brandsource?.brands?[indexPath.row].id ?? 0, brandsource: brandsource!, cattitle: self.brandsource?.brands?[indexPath.row].title_ar ?? "", imageURL: (self.brandsource?.imgs_url)! + "/" + (self.brandsource?.brands?[indexPath.row].image)! )
    }

}
