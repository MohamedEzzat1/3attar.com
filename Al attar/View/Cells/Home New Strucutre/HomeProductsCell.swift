//
//  HomeProductsCell.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 11/12/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class HomeProductsCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var seeallBtn: UIButtonX!
    @IBOutlet weak var productsCollection: UICollectionView!
    
    var productdelegate: OpenProductBestSellingDelegate?
    var brandsdelegate: OpenBrandsDelegate?
    var catsource: Cat_Base?
    var productsourcev2: _Product_By_CatsV2?
    var productsourcedatav2: _Product_By_BrandsV2?
    var productsourcedataDL: _Product_By_BrandsDL?
    var productsarr: [article_Base]?
    var brandsource: Brands_Base?
    var cartdelegate: CartDelegate?
    var CartArr = MyCartData.getCartData().0 as! [CartData]
    var cartarr = [[String:Any]]()
    var nextpage = ""
    var producturi = ""
    var checkuri = false
    var lastContentOffset: CGFloat = 0.0
    var checktype = 0
    var productsource: HomeProductDetails?
    
    var cartdelegete: CartDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
//    func setCart(cartcount: String) {
//        print(cartcount, "dsfwef")
//        cartdelegate?.setCart(cartcount: cartcount)
//    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.productsource?.article?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductsCell", for: indexPath) as! ProductsCell
        
//        cell.cartdelegete = self
        cell.greenview.isHidden = true

        
        if let brands = self.brandsource?.brands{
            for i in brands{
                
                if self.productsarr?[indexPath.row].brand_id == i.id{
                    let base_url = self.brandsource?.imgs_url
                    let imgurl   = brands[brands.index(where: { $0.id == i.id }) ?? 0 ].image
                    
                    let url = base_url! + "/" + imgurl!
                    cell.brandimage = url
                    let u = URL(string: url)
                    cell.brandImg.cornerRadius = 22.5
                    cell.brandImg.kf.setImage(with: u)
                    if cell.brandImg.image == nil{
                        cell.brandImg.image = UIImage(named: "logo_splash_screen")
                    }
                }
            }
        }
        
        
        cell.cellbrandid = self.productsarr?[indexPath.row].brand_id ?? 0
        cell.brandBtn.addTarget(self, action: #selector(openbrandspage), for: .touchUpInside)
        
        
//        cell.productid = String(describing:(self.productsarr?[indexPath.row].id!)!)
        cell.cellPrice = self.productsarr?[indexPath.row].regular_price as! String
        cell.celltitle_ar = self.productsarr?[indexPath.row].title_ar as! String
        cell.celltitle_en = self.productsarr?[indexPath.row].title_en as! String

        if LanguageHelper.getCurrentLanguage() == "ar"{
            cell.addnewItem.roundCorners(corners: [.topRight, .bottomLeft], radius: 10)
            cell.prdouctName.text = self.productsarr?[indexPath.row].title_ar
            cell.desc.text = self.productsarr?[indexPath.row].description_ar
            cell.price.text = (self.productsarr?[indexPath.row].regular_price)! + " جنيه"
        }else{
            cell.addnewItem.roundCorners(corners: [.topLeft, .bottomRight], radius: 10)
            cell.prdouctName.text = self.productsarr?[indexPath.row].title_en
            cell.desc.text = self.productsarr?[indexPath.row].description_en
            cell.price.text = (self.productsarr?[indexPath.row].regular_price)! + " LE"
        }
////        cell.price.text = self.productsource?.categories?[indexPath.row].regular_price
        var  base_url =  self.productsource?.imgs_url ?? ""
       print(base_url, "dasewfW")
        let imgurl   = self.productsarr?[indexPath.row].image
        let url = base_url + "/" + imgurl!
//        cell.brandimage = url
        cell.cellimg = url
        let u = URL(string: url)
        cell.img.kf.setImage(with: u)
        if cell.img.image == nil{
            cell.img.image = UIImage(named: "logo_splash_screen")
        }
//        cell.addnewItem.addTarget(self, action: #selector(addcounter), for: .touchUpInside)
        
        return cell
    }
    
    @objc func openbrandspage(){
        //openProduct(productid: cellbrandtag)
        brandsdelegate?.openBrands(brandid: cellbrandtag, brandsource: brandsource!, cattitle: "", imageURL: pbrandimg)
        print(cellbrandtag, "dsfewfw")
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        productdelegate?.openBestProduct(productid: self.productsarr?[indexPath.row].id ?? 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 180, height: 228)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
