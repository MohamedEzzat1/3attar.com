//
//  NewBannerCell.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 11/12/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import UIKit

class NewBannerCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var bannersource: Banner_Base?
    
    @IBOutlet weak var slidercollection: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return self.bannersource?.banner?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SliderCell", for: indexPath) as! SliderCell
        let url = URL(string: (self.bannersource?.imgs_url)! + "/" + (self.bannersource?.banner?[indexPath.section].image)! )
        print((self.bannersource?.imgs_url)! + "/" + (self.bannersource?.banner?[indexPath.section].image)! , "Asffwefwe")
        cell.img.kf.setImage(with: url)
    if cell.img.image == nil{
    cell.img.image = UIImage(named: "logo_splash_screen")
    }
        return cell
    }

}
