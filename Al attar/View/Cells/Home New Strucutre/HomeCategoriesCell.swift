//
//  HomeCategoriesCell.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 11/12/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import UIKit
import Kingfisher

class HomeCategoriesCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var catsource: Cat_Base?
    var catdelegate: OpenSubCategoryDelegate?

    @IBOutlet weak var catcollection: UICollectionView!
    @IBOutlet weak var titleLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.catsource?.categories?[0].sub_category?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCatsCell", for: indexPath) as! HomeCatsCell
        
        var strURL = ""
        let word = (self.catsource?.imgs_url)!
        
        if let base = word.range(of: "\n")?.lowerBound {
            let substring = word[..<base]                 // "ora"
            let string = String(substring)
            strURL = string
            print(string)  // "ora"
        }
        
        var tailStr = (self.catsource?.categories?[0].sub_category?[indexPath.row].image)!
        tailStr = tailStr.replacingOccurrences(of: " ", with: "%20")
        
        
        let url = URL(string: word + "/" + tailStr )
        if LanguageHelper.getCurrentLanguage() == "ar"{
            cell.catLbl.text = self.catsource?.categories?[0].sub_category?[indexPath.row].title_ar
        }else{
            cell.catLbl.text = self.catsource?.categories?[0].sub_category?[indexPath.row].title_en
        }
        print(word + "/" + tailStr, "EEEE", word)
        
        cell.img.kf.setImage(with: url)
        if cell.img.image == nil{
            cell.img.image = UIImage(named: "logo_splash_screen")
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        catdelegate?.openCats(checktype: 1, catid: self.catsource?.categories?[0].sub_category?[indexPath.row].id ?? 0, catsource: catsource!, imageURL: (self.catsource?.imgs_url ?? "") + (self.catsource?.categories?[0].sub_category?[indexPath.row].image)!, cattitle: self.catsource?.categories?[0].sub_category?[indexPath.row].title_ar ?? "" )
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 109, height: 155)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
