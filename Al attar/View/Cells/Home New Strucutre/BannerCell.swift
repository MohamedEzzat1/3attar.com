//
//  BannerCell.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 11/12/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class BannerCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var gallerysource: Gallery_Base?
    var viewcont = NewHomeVC()
    var bannerdelegate: OpenBanner1Delegate?
    
    @IBOutlet weak var slidercollection: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1//self.gallerysource?.gallery?.count ?? 0
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
            return self.gallerysource?.gallery?.count ?? 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SliderCell", for: indexPath) as! SliderCell
       
            let url = URL(string: (self.gallerysource?.imgs_url)! + "/" + (self.gallerysource?.gallery?[indexPath.section].img)! )
            cell.img.kf.setImage(with: url)
        if cell.img.image == nil{
        cell.img.image = UIImage(named: "logo_splash_screen")
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 324, height: 180)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let url = self.gallerysource?.gallery?[indexPath.section].img_url ?? ""
        if let range = url.range(of: "attar:///catalog/") {
            let uriof = url[range.upperBound...].trimmingCharacters(in: .whitespaces)
            if uriof.contains("product/"){
                guard let urirange = uriof.range(of: "product/") else{return}
                let uri = uriof[urirange.upperBound...].trimmingCharacters(in: .whitespaces)
                print(uri)
                let nextStoryboard: UIStoryboard = UIStoryboard(name: "Order", bundle: nil)
                let vc = nextStoryboard.instantiateViewController(withIdentifier: "ProductDetailsVC") as! ProductDetailsVC
                vc.checkuri = true
                vc.producturi = uri
                bannerdelegate?.openBanner(checkuri: true, producturi: uri, checktype: 0, imageURL: "")
                viewcont.navigationController?.pushViewController(vc, animated: true)
            }else if uriof.contains("brands/"){
                guard let urirange = uriof.range(of: "brands/") else{return}
                let uri = uriof[urirange.upperBound...].trimmingCharacters(in: .whitespaces)
                print(uri)
                let nextStoryboard: UIStoryboard = UIStoryboard(name: "Order", bundle: nil)
                let vc = nextStoryboard.instantiateViewController(withIdentifier: "CategoryPageVC") as! CategoryPageVC
                vc.checkuri = true
                vc.producturi = uri
                vc.checktype = 2
                vc.imageURL = (self.gallerysource?.imgs_url)! + "/" + (self.gallerysource?.gallery?[indexPath.section].img)!
                bannerdelegate?.openBanner(checkuri: true, producturi: uri, checktype: 2, imageURL: vc.imageURL)
                UIApplication.shared.keyWindow?.rootViewController?.navigationController?.present(vc, animated: true, completion: nil)//(vc, animated: true)
            }else if uriof.contains("category/"){
                guard let urirange = uriof.range(of: "category/") else{return}
                let uri = uriof[urirange.upperBound...].trimmingCharacters(in: .whitespaces)
                print(uri)
                let nextStoryboard: UIStoryboard = UIStoryboard(name: "Order", bundle: nil)
                let vc = nextStoryboard.instantiateViewController(withIdentifier: "CategoryPageVC") as! CategoryPageVC
                vc.checkuri = true
                vc.producturi = uri
                vc.checktype = 1
                vc.imageURL = (self.gallerysource?.imgs_url)! + "/" + (self.gallerysource?.gallery?[indexPath.section].img)!
                bannerdelegate?.openBanner(checkuri: true, producturi: uri, checktype: 1, imageURL: vc.imageURL)
                print("erer")
                UIApplication.shared.keyWindow?.rootViewController?.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
