//
//  BasketCell.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 8/22/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import UIKit

class BasketCell: UITableViewCell {

    @IBOutlet weak var ordernum1: UILabel!
    @IBOutlet weak var ordernum2: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var priceView: UIViewX!
    @IBOutlet weak var currencyLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
