//
//  MenuProfileCell.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 7/24/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import UIKit

class MenuProfileCell: UITableViewCell {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var goLbl: UILabel!
    @IBOutlet weak var catsLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        goLbl.setText(forKey: "gotoprofile")
        catsLbl.setText(forKey: "maincats")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
