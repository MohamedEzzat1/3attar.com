//
//  HomeCatsCell.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 7/23/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import UIKit

class HomeCatsCell: UICollectionViewCell {
    
    @IBOutlet weak var catLbl: UILabel!
    @IBOutlet weak var img: UIImageViewX!
}
