//
//  extentions.swift
//  Green Arrow
//
//  Created by OSX on 7/4/18.
//  Copyright © 2018 2Grand. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController: UIPopoverPresentationControllerDelegate {
    
    
    // show alert
        @objc func showAlert(style: UIAlertController.Style, title: String, message: String, buttonTitle: String){
            let alert = UIAlertController(title: title, message: message, preferredStyle: style)
            let action = UIAlertAction(title: buttonTitle, style: .default, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
        }
        
    func changerate(button1: UIButton, button2: UIButton, button3: UIButton, button4: UIButton, button5: UIButton){
        button1.imageEdgeInsets.left = 8
        button1.imageEdgeInsets.right = 0
        button2.imageEdgeInsets.left = 8
        button2.imageEdgeInsets.right = 0
        button3.imageEdgeInsets.left = 8
        button3.imageEdgeInsets.right = 0
        button4.imageEdgeInsets.left = 8
        button4.imageEdgeInsets.right = 0
        button5.imageEdgeInsets.left = 8
        button5.imageEdgeInsets.right = 0
    }
        
        // push view controller
        @objc func pushVC(storyboard: String, viewController: String){
            let storyboard: UIStoryboard = UIStoryboard(name: storyboard, bundle: nil)
            let vc: UIViewController = storyboard.instantiateViewController(withIdentifier: viewController) as UIViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        
        // present view controller
        @objc func presentVC(storyboard: String, viewController: String, animated: Bool){
            let storyboard: UIStoryboard = UIStoryboard(name: storyboard, bundle: nil)
            let vc: UIViewController = storyboard.instantiateViewController(withIdentifier: viewController) as UIViewController
            self.present(vc, animated: animated, completion: nil)
        }
        
        
        // present view controller as popover
        @objc func presentPopover(popoverVC vc: UIViewController, sender: UIButton){
            vc.modalPresentationStyle = UIModalPresentationStyle.popover
            vc.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.up
            vc.popoverPresentationController?.delegate = self
            vc.popoverPresentationController?.sourceView = sender
            vc.popoverPresentationController?.sourceRect = sender.bounds
            self.present(vc, animated: true, completion: nil)
        }
        
        // set view controller semantic using semantic value
        @objc func setSemantic(view: UIView, semantic: UISemanticContentAttribute){
            self.navigationController?.navigationBar.semanticContentAttribute = semantic
            self.tabBarController?.tabBar.semanticContentAttribute = semantic
            view.semanticContentAttribute = semantic
            for case let textField as UITextField in view.subviews{
                if semantic == .forceLeftToRight{
                    textField.textAlignment = .left
                }
                else if semantic == .forceRightToLeft{
                    textField.textAlignment = .right
                }
            }
            
            for v in view.subviews{
                setSemantic(view: v, semantic: semantic)
            }
            
            
        }
        
        
        
        
        // add navigation title
        @objc func addNavigationTitle(stringKey text: String){
            
            let tView = UIView(frame: CGRect(x: 0, y: 0, width: 10000, height: 50))
            let tLbl = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 100, height: 50))
            tLbl.text = text
            tLbl.textColor = UIColor.white
            tView.addSubview(tLbl)
            if LanguageHelper.getCurrentLanguage() == "ar"{
                tLbl.textAlignment = .right
            }
            else{
                tLbl.textAlignment = .left
            }
            self.navigationItem.titleView = tView
        }
        
        
        // add navigation view
        @objc func addNavigationView(imageName name: String){
            
            let tView = UIView(frame: CGRect(x: 0, y: 0, width: 10000, height: 50))
            let image = UIImageView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 65, height: 50))
            image.setImage(forKey: name)
            self.navigationItem.titleView = tView
        }
        
        // Open From Splash Screen
        @objc func openFromSplash(vc:UIViewController, navVC1:String, navVC2:String, storyboard: String){
            let launchedBefore  = UserDefaults.standard.bool(forKey: "launchedBefore")
            let when = DispatchTime.now() + 1.5
            DispatchQueue.main.asyncAfter(deadline: when){
                if (launchedBefore) == true {
                    UserDefaults.standard.set(true, forKey: "launchedBefore")
                    vc.pushVC(storyboard: storyboard, viewController: navVC1)
                }else {
                    UserDefaults.standard.set(true, forKey: "launchedBefore")
                    vc.pushVC(storyboard: storyboard, viewController: navVC2)
                    //Walkthrough
                }
            }
        }
        
        //Change Language
        @objc func changeLanguage(vc:UIViewController, navVC:String, language: String) {
            var languagekey = LanguageHelper.getCurrentLanguage()
            if language == "ar"{
                LanguageHelper.setCurrentLanguage(langugaeCode: "ar")
                self.setSemantic(view: vc.view, languageKey: LanguageHelper.getCurrentLanguage())
                languagekey = "ar"
            }else{
                LanguageHelper.setCurrentLanguage(langugaeCode: "en")
                self.setSemantic(view: vc.view, languageKey: LanguageHelper.getCurrentLanguage())
                languagekey = "en"
            }
            UIApplication.shared.keyWindow?.rootViewController = vc.storyboard!.instantiateViewController(withIdentifier: navVC)
        }

  
//    func addRihgtBarBtn(){
//         var button1 = UIBarButtonItem()
//        if SharedHandler.getLanguage() == "en"{
//             button1 = UIBarButtonItem(image: UIImage(named: "backR"), style: .plain, target: self, action: #selector(self.go2Notification))
//        }else{
//            button1 = UIBarButtonItem(image: UIImage(named: "backL"), style: .plain, target: self, action: #selector(self.go2Notification))
//        }
//         // action:#selector(Class.MethodName) for swift 3
//        self.navigationItem.rightBarButtonItem  = button1
//    }


    //Hide Keyboard
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    //Remove ViewController
    func removeViewControllerWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.removeViewController))
        view.addGestureRecognizer(tap)
    }
    
    @objc func removeViewController() {
        popUpDismissVC()
    }
    
    
    // POPUP PRESENT
    func popUpPresent(_ uivewController: UIViewController) {
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = CATransitionType.fade
        transition.subtype = CATransitionSubtype.fromTop
        self.view.window?.layer.add(transition, forKey: kCATransition)
        present(uivewController, animated: false, completion: nil)
    }
    
    // CUSTOM PRESENT
    func CustomPresent(_ uivewController: UIViewController) {
        let transition = CATransition()
      transition.duration = 0.3
        transition.type = CATransitionType.fade
        transition.subtype = CATransitionSubtype.fromBottom
        self.view.window?.layer.add(transition, forKey: kCATransition)
        present(uivewController, animated: false, completion: nil)
    }
    
    func CustomPresentFromRight(_ uivewController: UIViewController) {
        let transition = CATransition()
        transition.duration = 0.3
        transition.type     = CATransitionType.push
        transition.subtype  = CATransitionSubtype.fromRight
        self.view.window?.layer.add(transition, forKey: kCATransition)
        present(uivewController, animated: false, completion: nil)
    }

    
    // POPUP DISMISS
    func popUpDismissVC() {
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = CATransitionType.fade
        transition.subtype = CATransitionSubtype.fromRight
        self.view.window?.layer.add(transition, forKey: kCATransition)
        dismiss(animated: false, completion: nil)
    }
    
    func popUpDismissFromLeft() {
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = CATransitionType.fade
        transition.subtype = CATransitionSubtype.fromLeft
        self.view.window?.layer.add(transition, forKey: kCATransition)
        dismiss(animated: false, completion: nil)
    }

    
    //View Controller Animation
    func showAnimate() {
        self.view.transform = CGAffineTransform(scaleX: 1.3 , y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25) {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }
    }
    
    func removeAnimation() {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3 , y: 1.3)
            self.view.alpha = 0.0;
        }) { (finished : Bool) in
            if finished {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    func noConnection(msgText:String, vc: UIViewController) {
        let shadowView = UIView()
        let noConnectionImg = UIImageView()
        let msgLbl = UILabel()
        let tryAgainBtn = UIButton()
        
        shadowView.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
        shadowView.frame = self.view.frame
        
        noConnectionImg.image = UIImage(named: "Vector Smart Object-6-min")
        noConnectionImg.frame.size = CGSize(width: 100, height: 100)
        noConnectionImg.center = shadowView.center
        
        msgLbl.frame.origin.x = 30
        msgLbl.textAlignment  = .center
        msgLbl.center.y   = shadowView.center.y + 100
        msgLbl.frame.size = CGSize(width: self.view.frame.width - 60, height: 25)
        msgLbl.text = msgText
        msgLbl.textColor = UIColor.white
        msgLbl.minimumScaleFactor = 0.5
        
        tryAgainBtn.frame.size = CGSize(width: 100, height: 40)
        tryAgainBtn.backgroundColor = UIColor(red: 63/255, green: 169/255, blue: 245/255, alpha: 1.0)
        tryAgainBtn.setTitleColor(UIColor.white, for: .normal)
        tryAgainBtn.setTitle("محاولة أخرى", for: .normal)
        tryAgainBtn.center.x = shadowView.center.x
        tryAgainBtn.center.y = shadowView.center.y + 200
        tryAgainBtn.layer.cornerRadius = 10
//        tryAgainBtn.addTarget(self, action: #selector(HomeVC.tryagain), for: .touchUpInside)
        
        shadowView.addSubview(msgLbl)
        shadowView.addSubview(noConnectionImg)
        shadowView.addSubview(tryAgainBtn)
        self.view.addSubview(shadowView)
    }
     
}

extension UIView {

    enum Visibility {
        case visible
        case invisible
        case gone
    }
    
    func addDashedBorder(view: UIView) {
    let color = UIColor(red: 188/255, green: 192/255, blue: 196/255, alpha: 1).cgColor

    let shapeLayer:CAShapeLayer = CAShapeLayer()
    let frameSize = self.frame.size
    let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)

    shapeLayer.bounds = shapeRect
    shapeLayer.position = CGPoint(x: view.center.x, y: frameSize.height/2)
    shapeLayer.fillColor = UIColor.clear.cgColor
    shapeLayer.strokeColor = color
    shapeLayer.lineWidth = 1
    shapeLayer.lineJoin = CAShapeLayerLineJoin.round
    shapeLayer.lineDashPattern = [6,3]
    shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 5).cgPath
        
    self.frame.origin.x = view.center.x
    self.layer.addSublayer(shapeLayer)
    }
    
    func roundCorners(corners:UIRectCorner, radius: CGFloat)
       {
           let maskLayer = CAShapeLayer()
           maskLayer.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius)).cgPath
           self.layer.mask = maskLayer
       }

    var visibility: Visibility {
        get {
            let constraint = (self.constraints.filter{$0.firstAttribute == .height && $0.constant == 0}.first)
            if let constraint = constraint, constraint.isActive {
                return .gone
            } else {
                return self.isHidden ? .invisible : .visible
            }
        }
        set {
            if self.visibility != newValue {
                self.setVisibility(newValue)
            }
        }
    }

    private func setVisibility(_ visibility: Visibility) {
        let constraint = (self.constraints.filter{$0.firstAttribute == .height && $0.constant == 0}.first)

        switch visibility {
        case .visible:
            constraint?.isActive = false
            self.isHidden = false
            break
        case .invisible:
            constraint?.isActive = false
            self.isHidden = true
            break
        case .gone:
            if let constraint = constraint {
                constraint.isActive = true
            } else {
                let constraint = NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 0)
                self.addConstraint(constraint)
                constraint.isActive = true
            }
        }
    }
}

extension UILabel {
    @IBInspectable var localizedString: String {
        get {
            return ""
        }
        set(value) {
            self.text = NSLocalizedString(value, comment: "")
        }
    }
}


extension UITextField {
    
    @IBInspectable var localizedString: String {
        get {
            return ""
        }
        set(value) {
            self.text = NSLocalizedString(value, comment: "")
        }
    }
    
    @IBInspectable var PlaceholderLocalized: String {
        get {
            return ""
        }
        set(value) {
            self.placeholder = NSLocalizedString(value, comment: "")
        }
    }
    
    func emptyField(msg:String){
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.red.cgColor
        self.leftViewMode = UITextField.ViewMode.always
        let imageView = UIImageView(frame: CGRect(x: 16, y: 0, width: 20, height: 20))
        let image = UIImage(named: "x-circle")
        self.text = ""
        self.placeholder = msg
        imageView.image = image
        self.leftView = imageView
    }
    
    func filled(){
        self.layer.borderWidth = 0
        let imageView = UIImageView(frame: CGRect(x: 16, y: 0, width: 20, height: 20))
        let image = UIImage(named: "")
        
        imageView.image = image
        self.leftView = imageView
    }
}

extension UITextView {
    @IBInspectable var localizedString: String {
        get {
            return ""
        }
        set(value) {
            self.text = NSLocalizedString(value, comment: "")
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
          let fixedWidth = textView.frame.size.width
          textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
          let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
          var newFrame = textView.frame
          newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
          textView.frame = newFrame
    }
}
extension CALayer {
    
    func addBorder(edge: UIRectEdge, color: UIColor, thickness: CGFloat) {
        
        let border = CALayer()
        
        switch edge {
        case UIRectEdge.top:
            border.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: thickness)
            break
        case UIRectEdge.bottom:
            border.frame = CGRect(x: 0, y: self.frame.height - thickness, width: self.frame.width, height: thickness)
            break
        case UIRectEdge.left:
            border.frame = CGRect(x: 0, y: 0, width: thickness, height: self.frame.height)
            break
        case UIRectEdge.right:
            border.frame = CGRect(x: self.frame.width - thickness, y: 0, width: thickness, height: self.frame.height)
            break
        default:
            //For Center Line
            border.frame = CGRect(x: self.frame.width/2 - thickness, y: 0, width: thickness, height: self.frame.height)
            break
        }
        
        border.backgroundColor = color.cgColor;
        self.addSublayer(border)
    }
}

extension  UITextField {
    
    static func isValidEmail(testStr: UITextField) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr.text)
    }
    
    
    static func compare(testStr: String , confirm: String) -> Bool{
        if testStr == confirm {
            return true
        }
        return false
    }
    
    static func isValidPassword(testStr: UITextField) -> Bool {
        if (testStr.text?.count)! >= 6 {
            return true
        }
        return false
    }

    
}


extension String {
    public func isPhone() -> Bool {
        if self.isAllDigits() == true {
            let phoneRegex = "966[0-9]{9}"
            let predicate = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
            return  predicate.evaluate(with: self)
        } else {
            return false
        }
    }
    
    public func isNum() -> Bool {
        if self.isAllDigits() {
            return true
        } else {
            return false
        }
    }
    
    private func isAllDigits() -> Bool {
        let charcterSet  = NSCharacterSet(charactersIn: "+0123456789").inverted
        let inputString = self.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        return  self == filtered
    }
    
    func encodeUrl() -> String
    {
        return self.addingPercentEncoding( withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
    }
    
    var fixedArabicURL: String?  {
        return self.addingPercentEncoding(withAllowedCharacters: CharacterSet.alphanumerics
            .union(CharacterSet.urlPathAllowed)
            .union(CharacterSet.urlHostAllowed))
    }
    
}


extension Int {
    init(_ range: Range<Int> ) {
        let delta = range.lowerBound < 0 ? abs(range.lowerBound) : 0
        let min = UInt32(range.lowerBound + delta)
        let max = UInt32(range.upperBound   + delta)
        self.init(Int(min + arc4random_uniform(max - min)) - delta)
    }
}


//function that receive the number of digits requested, calculates the range of the random number
func random(_ digits:Int) -> Int {
    let min = Int(pow(Double(10), Double(digits-1))) - 1
    let max = Int(pow(Double(10), Double(digits))) - 1
    return Int(Range(uncheckedBounds: (min, max)))
}



//extension String {
//    func extractURLs() -> [NSURL] {
//        var urls : [NSURL] = []
//        do {
//            let detector = try NSDataDetector(types: NSTextCheckingType.Link.rawValue)
//            detector.enumerateMatchesInString(self,
//                                              options: [],
//                                              range: NSMakeRange(0, text.characters.count),
//                                              usingBlock: { (result, _, _) in
//                                                if let match = result, url = match.URL {
//                                                    urls.append(url)
//                                                }
//            })
//        } catch let error as NSError {
//            print(error.localizedDescription)
//        }
//        return urls
//    }
//}

///----------------

//let input = "This is a test with the URL https://www.hackingwithswift.com to be detected."
//let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
//let matches = detector.matches(in: input, options: [], range: NSRange(location: 0, length: input.utf16.count))
//
//for match in matches {
//    guard let range = Range(match.range, in: input) else { continue }
//    let url = input[range]
//    print(url)
//}
///----------

//
//let encodedLink = requesturl.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
//let encodedURL = NSURL(string: encodedLink!)! as URL
