//
//  SharedHandler.swift
//  wuzfone
//
//  Created by Donna on 10/6/18.
//  Copyright © 2018 Madonna. All rights reserved.
//



import Foundation
import UIKit
import Lottie

class SharedHandler{
    
    static var sideButton : UIBarButtonItem!
    static var animationView: AnimationView?
    
    static func setSideImage()  {
        let image = UIImage(named: "Menu_black_button")
        
        sideButton = UIBarButtonItem(image: image,  style: .plain , target: self, action: Selector(("didTapEditButton:")))
        sideButton.image = UIImage(named:"Menu_black_button")?.withRenderingMode(.alwaysOriginal)
    }
    
    static func startSpinner (_ Spinner: UIActivityIndicatorView, _ View: UIView, _ StopView:  UIView){
//        Spinner.color = UIColor(red: 63/255, green: 169/255, blue: 245/255, alpha: 1.0)
        Spinner.startAnimating()
        Spinner.isHidden = false
        View.isHidden = true
        StopView.isUserInteractionEnabled = false
    }
    static func stopSpinner (_ Spinner: UIActivityIndicatorView,_ View: UIView, _ StopView:  UIView){
        Spinner.stopAnimating()
        Spinner.isHidden = true
         View.isHidden = false
        StopView.isUserInteractionEnabled = true
    }
    
    static func startloadingGIF(_ view: UIView){
         
        // 2. Start AnimationView with animation name (without extension)
          
        animationView = .init(name: "Loding")
                  
        animationView!.frame = view.bounds
          
          // 3. Set animation content mode
                  
        animationView!.contentMode = .center
          
          // 4. Set animation loop mode
          
        animationView!.loopMode = .loop
          
          // 5. Adjust animation speed
          
        animationView!.animationSpeed = 1.5
          
        view.addSubview(animationView!)
          
          // 6. Play animation
          
        animationView!.play()
    }
  
    static func stoploadingGIF(_ view: UIView){
         
        // 2. Start AnimationView with animation name (without extension)
          
//        animationView = .init(name: "Loding")
//                  
//        animationView!.frame = view.bounds
//          
//          // 3. Set animation content mode
//                  
//        animationView!.contentMode = .center
//          
//          // 4. Set animation loop mode
//          
//        animationView!.loopMode = .loop
//          
//          // 5. Adjust animation speed
//          
//        animationView!.animationSpeed = 1
//          
//        view.addSubview(animationView!)
          
          // 6. Stop animation
        animationView!.stop()
        animationView!.isHidden = true
    }
  
    static func sideMenus(ViewController control: UIViewController)
    {
        setSideImage()
        if control.revealViewController() != nil
        {
            
//            MOLHLanguage.isRTL()
            control.setSemantic(view: control.view, languageKey: LanguageHelper.getCurrentLanguage())
            
            if LanguageHelper.getCurrentLanguage() != "en" {

                // right setting
                sideButton.tintColor = UIColor.white
                sideButton.target = control.revealViewController()
                sideButton.action = #selector(SWRevealViewController.rightRevealToggle(_:))
                control.navigationItem.leftBarButtonItem = sideButton
//                control.view.addGestureRecognizer(control.revealViewController().panGestureRecognizer())
                control.revealViewController().rearViewRevealWidth = UIScreen.main.bounds.width - 40.0
            }
            else {
                // left setting
                sideButton.tintColor = UIColor.white
                sideButton.target = control.revealViewController()
                sideButton.action = #selector(SWRevealViewController.revealToggle(_:))
                control.navigationItem.leftBarButtonItem = sideButton
                control.revealViewController().rightViewRevealWidth = UIScreen.main.bounds.width - 40.0
               
            }
             control.revealViewController().frontViewController.view.addGestureRecognizer(control.revealViewController().tapGestureRecognizer())

        }
    }
  
    static func getUserID() -> Int{
        let y = UserDefaults.standard.object(forKey: _ID) as? Int ?? 0
        return y
    }
    static func getUserType() -> Int{
        // false = jobSeeker , true = employer
        let y = UserDefaults.standard.object(forKey: _Type) as? Int ?? 0
        return y
    }
    
  static func ByGrand(flag: Bool) {
    let def = UserDefaults.standard
        def.setValue(flag,forKey: "Grand")
        def.synchronize()
    }
   
    
    static func createViewController( storyboardID : String ) -> UIViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: storyboardID )
        return controller
        
    }
    static func gotosw(_ sourceViewController: UIViewController){
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
        sourceViewController.present(vc, animated: true, completion: nil)
        
    }
    
    
    
    static  func roundCorners(_ lbl: UILabel,cornerRadius: Double) {
        let path = UIBezierPath(roundedRect: lbl.bounds, byRoundingCorners: [.topLeft, .bottomLeft], cornerRadii: CGSize(width: cornerRadius, height: cornerRadius))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = lbl.bounds
        maskLayer.path = path.cgPath
        lbl.layer.mask = maskLayer
    }
    static func getLanguageInt()->Int{
        if LanguageHelper.getCurrentLanguage() == "en"{
            return 0

        }else{
            return 1

        }
    }
    
    static func alertDialogawithSegue(_ sourceViewController: UIViewController,_ destinationViewController: UIViewController,_ title: String , _ message: String ,BtnTitle: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: BtnTitle, style: UIAlertAction.Style.default, handler: { (action) in
            
            sourceViewController.present(destinationViewController, animated: true, completion: nil)
        }))
        
        // show the alert
        sourceViewController.present(alert, animated: true, completion: nil)
        
    }
    
    
    static func DisAppearalertDialogawithSegue(_ sourceViewController: UIViewController,_ destinationViewController: UIViewController,_ title: String , _ message: String ,BtnTitle: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: BtnTitle, style: UIAlertAction.Style.default, handler: { (action) in
            
            sourceViewController.present(destinationViewController, animated: true, completion: nil)
        }))
        
        // show the alert
        sourceViewController.present(alert, animated: true, completion: nil)
        // change to desired number of seconds
        let when = DispatchTime.now() + 1
        DispatchQueue.main.asyncAfter(deadline: when){
            alert.dismiss(animated: true, completion: nil)
        }
        
    }
    
    static func alertDialogawithPopNavigationVC(_ sourceViewController: UIViewController,_ title: String , _ message: String ,BtnTitle: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: BtnTitle, style: UIAlertAction.Style.default, handler: { (action) in
            
            sourceViewController.navigationController?.popViewController(animated: true)
        }))
        
        // show the alert
        sourceViewController.present(alert, animated: true, completion: nil)
        
    }
    
//    static func swSegue2ViewController(_ storyboardName: String ,_ viewControllerId : String,_ viewController : UIViewController)  {
//        let dvc = UIStoryboard(name: storyboardName, bundle: nil).instantiateViewController(withIdentifier: viewControllerId)
//        let Rvc : SWRevealViewController = viewController.revealViewController() as SWRevealViewController
//        Rvc.pushFrontViewController(dvc, animated: true)
//    }
    static func redfield(_ textfield: UITextField){
        textfield.layer.borderColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
        textfield.layer.borderWidth = 1
    }

    static func responseError(ViewController controller: UIViewController,errorMsg: String,catchError:Bool){
        if  catchError{
            if SharedHandler.getLanguage() == "en"{
                SharedHandler.alertDialog(controller, "Alert", errorMsg, BtnTitle: "Dismiss")
            }else{
                        SharedHandler.alertDialog(controller, "تنبيه", "البيانات في صورة غير صحيحة", BtnTitle: "إلغاء")
                
            }
            
        }else{
        if SharedHandler.getLanguage() == "en"{
            SharedHandler.alertDialog(controller, "Alert", errorMsg, BtnTitle: "Dismiss")
        }else{
            SharedHandler.alertDialog(controller, "تنبيه", "لا يوجد انترنت", BtnTitle: "إلغاء")
            
        }
        }
    }
    
    static func DisAppearMessage(viewController: UIViewController,message: String,myHandler: (() -> Swift.Void)? = nil)
    {
        let alert = UIAlertController(title: "", message: message , preferredStyle: UIAlertController.Style.alert)
        
        viewController.present(alert, animated: true, completion: nil)
        
        // change to desired number of seconds
        let when = DispatchTime.now() + 2
        DispatchQueue.main.asyncAfter(deadline: when){
            alert.dismiss(animated: true, completion: nil)
        }
    }
    
    static func DisAppearMessageTrue(viewController: UIViewController,message: String,myHandler: (() -> Swift.Void)? = nil)
    {
        let alert = UIAlertController(title: "", message: message , preferredStyle: UIAlertController.Style.alert)
        
        viewController.present(alert, animated: true, completion: nil)
        
        // change to desired number of seconds
        let when = DispatchTime.now() + 2
        DispatchQueue.main.asyncAfter(deadline: when){
            alert.dismiss(animated: true, completion: nil)
            viewController.dismiss(animated: true, completion: nil)
        }
    }
    
    static func IsLogin() -> Bool{
       
        let y = UserDefaults.standard.object(forKey: _LoggedIn) as? Bool ?? false
        return y
    }
    static func getUserName() -> String{
        let y = UserDefaults.standard.object(forKey: _Name) as? String ?? ""
        return y
    }
    static func getUserDefault(of userDeafult : String) -> String {
        let y = UserDefaults.standard.object(forKey: userDeafult) as? String ?? ""
        return y
    }
    
    static func setUserDefault(_ Value : String, forKey : String)  {
        UserDefaults.standard.set(Value, forKey: forKey)
        UserDefaults.standard.synchronize()
        
    }
    static func getUserMail() -> String{
        let y = UserDefaults.standard.object(forKey: _Mail) as? String ?? ""
        return y
    }
    static func getUserID() -> String{
        let y = UserDefaults.standard.object(forKey: _ID) as? String ?? ""
        return y
    }
    static func getUserPhone() -> String{
        let y = UserDefaults.standard.object(forKey: _Phone) as? String ?? ""
        return y
    }
    static func getUserImage() -> String{
        let y = UserDefaults.standard.object(forKey: _IMAGE) as? String ?? ""
        return y
    }
    static func getUserGender(_ number: String) -> String{
        if number == "0"{
            return "Male"
            
        }else if number == "1" {
            return "Female"
            
        } else{
            return "Both"
        }
        
    }
    
    static func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
   
    static func isValidPassword() -> Bool {
        let passwordRegex = "(?:(?:(?=.*?[0-9])(?=.*?[-!@#$%&*ˆ+=_])|(?:(?=.*?[0-9])|(?=.*?[A-Z])|(?=.*?[-!@#$%&*ˆ+=_])))|(?=.*?[a-z])(?=.*?[0-9])(?=.*?[-!@#$%&*ˆ+=_]))[A-Za-z0-9-!@#$%&*ˆ+=_]{6,15}"
        return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: self)
    }
    
    static func logout(){
        
        

    UserDefaults.standard.set(nil, forKey: _ID)
    UserDefaults.standard.set(false, forKey: _LoggedIn)
        UserDefaults.standard.set("", forKey: _Jwt)
        UserDefaults.standard.set(nil, forKey: _CompanyId)

    }
    
    
  
    static func alertDialog(_ viewController: UIViewController,_ title: String , _ message: String ,BtnTitle: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: BtnTitle, style: UIAlertAction.Style.default, handler: nil))
        
        // show the alert
        viewController.present(alert, animated: true, completion: nil)
        
    }
    static func alertDialogChangeLang(_ viewController: UIViewController ,BtnTitle: String,BtnTitle2: String,msg: String){
        let alert = UIAlertController(title: "", message: msg, preferredStyle: UIAlertController.Style.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: BtnTitle, style: UIAlertAction.Style.default, handler: { (act) in
                        alert.dismiss(animated: true, completion: nil)
        }))
//        alert.addAction(UIAlertAction(title: BtnTitle2, style: UIAlertAction.Style.default, handler: { (act) in
//            MOLH.setLanguageTo(MOLHLanguage.currentAppleLanguage() == "en" ? "ar" : "en")
//            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
//            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "tab") as! UITabBarController
//            UIApplication.shared.keyWindow?.rootViewController = viewController
////            MOLH.reset()
//        }))

  
        
        // show the alert
        viewController.present(alert, animated: true, completion: nil)
        
    }
    static func alertDialog2lang(_ viewController: UIViewController,msgEg: String,msgAr: String){
        if LanguageHelper.getCurrentLanguage() == "en"{
            SharedHandler.alertDialog(viewController, "Alert", msgEg, BtnTitle: "Dismiss")
        }else{

            SharedHandler.alertDialog(viewController, "تنبيه", msgAr, BtnTitle: "إلغاء")
        }
        
        
    }
    
    static func htmlToText(encodedString:String) -> String?
    {
        let encodedData = encodedString.data(using: String.Encoding.utf8)!
        do
        {
            return try NSAttributedString(data: encodedData, options: [.documentType : NSAttributedString.DocumentType.html], documentAttributes: nil).string
        } catch let error as NSError {
            print(error.localizedDescription)
            return nil
        }
    }
    
   static func formattedDateFromString(dateString: String) -> String? {
    
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.locale = Locale.init(identifier: "en_GB")
        
        let dateObj = dateFormatter.date(from: dateString)
        
        dateFormatter.dateFormat = "MMM d, yyyy h:mm a"
        print("Dateobj: \(dateFormatter.string(from: dateObj!))")
        return dateFormatter.string(from: dateObj!)
    }
    
    static func alertDialogWithDismiss(_ viewController: UIViewController,_ title: String , _ message: String ,BtnTitle: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        // add an action (button) UIAlertAction(title: BtnTitle, style: UIAlertActionStyle.default, handler: nil)
        alert.addAction(UIAlertAction(title: BtnTitle, style: UIAlertAction.Style.default, handler: { (uialertaction) in
            print("inside alert closure")
            viewController.dismiss(animated: true, completion: nil)
        }))
        
        // show the alert
        viewController.present(alert, animated: true, completion: nil)
        
    }
    static func alertDialogWithPOpRootNavigation(_ viewController: UIViewController,_ title: String , _ message: String ,BtnTitle: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        // add an action (button) UIAlertAction(title: BtnTitle, style: UIAlertActionStyle.default, handler: nil)
        alert.addAction(UIAlertAction(title: BtnTitle, style: UIAlertAction.Style.default, handler: { (uialertaction) in
            print("inside alert closure")
            viewController.dismiss(animated: true, completion: nil)
        }))
        
        // show the alert
        viewController.navigationController?.popToRootViewController(animated: true)
        
    }
    
    static func createLabelNoResult(_ viewController : UIViewController){
        // CGRectMake has been deprecated - and should be let, not var
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: viewController.view.frame.width , height: 60 ))
        
        // you will probably want to set the font (remember to use Dynamic Type!)
        label.font = UIFont.preferredFont(forTextStyle: .headline)
        
        // and set the text color too - remember good contrast
        label.textColor = .black
        
        // may not be necessary (e.g., if the width & height match the superview)
        // if you do need to center, CGPointMake has been deprecated, so use this
        label.center = CGPoint(x: viewController.view.frame.width / 2.0, y: viewController.view.frame.height / 2.0)
        
        // this changed in Swift 3 (much better, no?)
        label.textAlignment = .center
        label.font = label.font.withSize(18)
        if getLanguage() == "en"{
            label.text = "No Data."
        }else{
        label.text = "لا يوجد بيانات ."
        }
        
        viewController.view.addSubview(label)
    }
    
    static func getLanguage() -> String{
        if LanguageHelper.getCurrentLanguage() == "en"{
            return "en"
        }else{
            return "ar"
        }
    }
    static func isValidAction(textFeilds: [UITextField]) -> Bool {
        
        for myTextFeild in textFeilds {
            if (myTextFeild.text!.isEmpty) {
                return false
            }
        }
        return true
    }
    class func topMostController() -> UIViewController {
        var topController: UIViewController? = UIApplication.shared.keyWindow?.rootViewController
        while ((topController?.presentedViewController) != nil) {
            topController = topController?.presentedViewController
        }
        return topController!
    }
    
    class func alert(message:String){
        let alert=UIAlertController(title: "AppName", message: message, preferredStyle: .alert);
        let cancelAction: UIAlertAction = UIAlertAction(title: "OK", style: .cancel) { action -> Void in
            
        }
        alert.addAction(cancelAction)
        SharedHandler.topMostController().present(alert, animated: true, completion: nil);
    }
}
