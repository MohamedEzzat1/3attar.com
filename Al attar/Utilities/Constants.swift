//
//  Constants.swift
//  Kadi
//
//  Created by apple on 12/4/17.
//  Copyright © 2017 apple. All rights reserved.
//

// Constant.swift
import UIKit

// User info
let _Token = "FireBaseToken"
let _Type = "type"
let _LoggedIn = "loggedIn"
let _Mail = "mail"
let _Phone = "phone"
let _ID = "id"
let _IMAGE = "image"
let _Verified = "verified"
let _DeviceID = "DeviceId"
let _Name = "name"
let _Address = "Address"
let _Gender = "gender"
let _Jwt = "jwt"
let _CompanyId = "companyid"
let _Userlat = "lat"
let _Userlng = "lng"
let _AboutPage = "aboutPage"
let _FirstLaunch = "firstLaunch"
let _SubID = "subID"
let _ServiceID = "serviceID"
let _TechID = "techID"


var _isnotification  = false
var _orderid = 0
var _type = ""

// Device info
let APPversion = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
let buildNumber = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String
let systemVersion = UIDevice.current.systemVersion
let DeviceModel = UIDevice.current.model
let DEVICE_ID = UIDevice.current.identifierForVendor!.uuidString
//typealias CompletionHandlerStatus = (_ success: Bool,_ status: String?) -> ()
//typealias CompletionHandler = (_ success: Bool,_ error: String? ,_ model: Any?) -> ()
////

//
//typealias CompletionHandlerBool = (_ success: Bool) -> ()
typealias CompletionHandler = (_ model: Any?) -> ()

let def = UserDefaults.standard
   


//   var arrcach = [SliderCell?](repeating: nil, count: 5)
