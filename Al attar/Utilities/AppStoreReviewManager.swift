//
//  AppStoreReviewManager.swift
//  Wabell
//
//  Created by Mohamed Ezzat on 8/15/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation
import StoreKit

enum AppStoreReviewManager {
  static func requestReviewIfAppropriate() {
    SKStoreReviewController.requestReview()
  }
}
