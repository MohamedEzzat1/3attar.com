//
//  CollectionViewTools.swift
//  tjara
//
//  Created by apple on 12/13/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit


class CollectionViewTools{
    
    class func formatingCollectionView(_ collectionView:UICollectionView ,topPadding:CGFloat,rightPadding:CGFloat,leftPadding:CGFloat,bottomPadding:CGFloat,width:CGFloat, height:CGFloat,minimumInteritemSpacing:CGFloat,minimumLineSpacing:CGFloat){
        let flow = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        flow.sectionInset = UIEdgeInsets(top: topPadding,left: leftPadding,bottom: bottomPadding,right: rightPadding )
        flow.itemSize = CGSize(width: (width) , height: (height) )
        flow.minimumInteritemSpacing = minimumInteritemSpacing
        flow.minimumLineSpacing = minimumLineSpacing
        
    }
    
}
