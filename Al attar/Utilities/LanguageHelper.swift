//
//  LanguageHelper.swift
//
//  Created by Abdalrahman on 3/2/17.
//  Copyright © 2017 imac. All rights reserved.
//

import UIKit


class LanguageHelper{
    
    private static var currentLanguage = UserDefaults.standard.value(forKey: "language") as? String != nil ? UserDefaults.standard.value(forKey: "language") as! String : NSLocale.current.languageCode!
    
    //private static var currentLanguage = "ar"
    
    static func setCurrentLanguage(langugaeCode: String) {
        currentLanguage = langugaeCode
        let defaults = UserDefaults.standard
        defaults.setValue(langugaeCode, forKey: "language")
        defaults.synchronize()
    }
    
    static func getCurrentLanguage() -> String {
        return currentLanguage
    }
    
    static func getString(forKey key: String) -> String{
        switch currentLanguage{
        case "en":
            print("12")
            return EnglishStrings.EnglishStringsDictionary[key]!
        case "ar":
            print("13")
            return ArabicStrings.ArabicStringsDictionary[key]!
        default:
            return "nil"
        }
    }
}


extension UILabel{
    
    // set label text from localized strings
    @objc func setText(forKey key: String){
        self.text = LanguageHelper.getString(forKey: key)
    }
}


extension UIButton{
    
    // set button text from localized strings
    @objc func setText(forKey key: String){
        self.setTitle(LanguageHelper.getString(forKey: key), for: .normal)
    }
}

extension UIImageView{
    
    // set image text from localized strings
    @objc func setImage(forKey key: String){
        self.image = UIImage(named: LanguageHelper.getString(forKey: key))
    }
}


extension UIBarButtonItem{
    
    // set button text from localized strings
    @objc func setImage(forKey key: String){
        self.image = UIImage(named: LanguageHelper.getString(forKey: key))
    }
}


extension UIViewController{
    
    // set view controller semantic using language key
    @objc func setSemantic(view: UIView, languageKey: String){
        for case let textField as UITextField in view.subviews{
            if languageKey == "ar"{
                textField.textAlignment = .right
            }
            else{
                textField.textAlignment = .left
            }
            
        }
        
        if languageKey == "ar"{
            view.semanticContentAttribute = .forceRightToLeft
            self.navigationController?.navigationBar.semanticContentAttribute = .forceRightToLeft
            self.tabBarController?.tabBar.semanticContentAttribute = .forceRightToLeft
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        }
        else{
            view.semanticContentAttribute = .forceLeftToRight
            self.navigationController?.navigationBar.semanticContentAttribute = .forceLeftToRight
            self.tabBarController?.tabBar.semanticContentAttribute = .forceLeftToRight
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        }
        
        for v in view.subviews{
            setSemantic(view: v, languageKey: languageKey)
        }
    }
    
    
    // show localized alert
    @objc func showLocalizedAlert(style: UIAlertController.Style, title: String, message: String, buttonTitle: String){
        let alert = UIAlertController(title: LanguageHelper.getString(forKey: title), message: LanguageHelper.getString(forKey: message), preferredStyle: style)
        let action = UIAlertAction(title: LanguageHelper.getString(forKey: buttonTitle), style: .default, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
}


extension UITextField{
    
    // set attributed text for text field from a language key
    @objc func setAttributedPlaceholder(forKey key: String, withColor color: UIColor){
        self.attributedPlaceholder = NSAttributedString(string:LanguageHelper.getString(forKey: key),attributes:[NSAttributedString.Key.foregroundColor: color])
    }
}
