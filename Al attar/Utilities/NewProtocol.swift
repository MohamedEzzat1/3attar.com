//
//  NewProtocol.swift
//  Al attar
//
//  Created by Mohamed Ezzat on 11/17/20.
//  Copyright © 2020 Mohamed Ezzat. All rights reserved.
//

import Foundation

protocol OpenBanner1Delegate{
    func openBanner(checkuri: Bool, producturi:String, checktype: Int, imageURL: String)
}

protocol OpenProductNewDelegate {
    func openProduct(productid: Int)
}

protocol OpenProductBestSellingDelegate {
    func openBestProduct(productid: Int)
}

protocol OpenBrandsDelegate {
    func openBrands(brandid: Int, brandsource: Brands_Base, cattitle: String, imageURL: String)
}

protocol OpenSubCategoryDelegate {
    func openCats(checktype: Int, catid: Int, catsource: Cat_Base, imageURL: String, cattitle: String)
}
