//
//  URLs.swift
//  wuzfone
//
//  Created by Donna on 10/6/18.
//  Copyright © 2018 Madonna. All rights reserved.
//

import Foundation

class URLs{
    
    static let urlsv1   = "https://app.3attar.com/api/v2"
    static let urlsv2   = "https://app.3attar.com/api/v2"
    static let authUrl   = "https://app.3attar.com/api"
    static let imageURL  = "https://app.3attar.com/public/uploads/images/"
    static let utlDev   = "https://dev.3attar.com/api/v2"
    
//    static let basic     = url   + "/api/user/"
    static let login     = authUrl + "/login"
    static let signup    = authUrl + "/signup"
    static let products  = urlsv1 + "/products/"
    static let gallery   = urlsv2 + "/gallery"
    static let allcats   = urlsv1 + "/allcategories"
    
    static let productCats = urlsv1 + "/productByCategory/"
    static let productCatsv2 = urlsv2 + "/productByCategory/"
    static let wishes    = urlsv2 + "/wishlist"
    static let product_details = urlsv1 + "/product_details/"
    static let user_address = urlsv1 + "/user_address"
    static let get_user_address = urlsv1 + "/get_user_address"
    static let Shippingzones = urlsv1 + "/shipping_zones"
    static let MyOrders = urlsv2 + "/my_orders"
    static let order_details = urlsv1 + "/order_details/"
    static let create_address = urlsv1 + "/create_address"
    static let update_address = urlsv1 + "/update_address"
    static let add_to_wishlist = urlsv1 + "/add_to_wishlist"
    static let placeOrder      = urlsv1 + "/make_order"
    static let brands          = urlsv1 + "/brands"
    static let selectbrand     = urlsv2 + "/productByBrand/"
    static let search          = urlsv1 + "/search"
    static let filter          = urlsv1 + "/filter"
    static let coupon          = urlsv1 + "/coupon/"
    static let brandDL         = urlsv2 + "/productByBrandDL/"
    static let productDL         = urlsv2 + "/product_details_dl/"
    static let categoryDL         = urlsv2 + "/productByCategoryDL/"
    
    static let bestselling     = utlDev + "/best_selling"
    static let newly_arrived     = utlDev + "/newly_arrived"
    static let subCategory    = utlDev + "/sub_category/"
    static let homeList        = utlDev + "/app_ui/"
    static let newbanners     = utlDev + "/bannerById/"
    static let submitprofile  = utlDev + "/update_user/"
}
